CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Extending
 * Roadmap
 * Maintainers


INTRODUCTION
------------

The `Entity Association` module provides the ability to connect entities
together into associations. An association has structural rules and shares
contexts.

Association rules are managed by `behavior plugins` which are responsible for
determining what entities are allowed to be added to the association, and
creating the content management administrative UI. Behaviors can for example
limit the types of entities, the number of entities and enforce ordering of
entities in an association.

Entities belonging to an association are given a shared context allowing them
to be aware of association they belong to. This makes it possible to
share blocks, banners, and menus.

Associations also can update entities that they maintain
(@see `Adding entity updaters` in the Extending section). Some examples would be
maintaining a pathauto alias, enforcing the association's active status, or
search indexing.


REQUIREMENTS
------------

This module requires the following modules:

 * [Token](https://www.drupal.org/project/token)
 * [Toolshed](https://www.drupal.org/project/toolshed)


RECOMMENDED MODULES
-------------------

This module works well and is enhanced by use of the following modules:

 * [Pathauto](https://www.drupal.org/project/pathauto)


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

This module adds a new `Entity Association` entity type which can be created
and managed from the admin menu at *Structure > Entity Association*.


EXTENDING
---------

Entity Association module strives to be extendable and customizable. The
following are plugins, tagged services, and events which allow additional
entity types, association rules and policies, and management of content to be
added.


**Adding behavior plugins**

Behavior plugins are the plugins which manage how entities are added to and
managed by associations. These plugins implement
`\Drupal\association\Plugin\BehaviorInterface` and are placed in the
`/plugin/Association/Behavior` folder.

 * @see _\Drupal\association\Plugin\Association\Behavior\EntityListBehavior_
 * @see _\Drupal\association\Plugin\Association\Behavior\EntityManifestBehavior_


**Adding entity adapters**

Entity types become available via entity adapter plugins. Adapters will
determine which entity bundles can be used, build entity operation forms and
association access controls.

Adapters are added by adding a "<module>.association.entity_adapter.yml" file
to your module with entity adapter definitions.

```
# Add support for node entity types to entity associations.
node:
  label: Content (node)
  entity_type: node
  class: \Drupal\association\Adapter\EntityAdapter
```

If class is ommitted the default `\Drupal\association\Adapter\EntityAdapter` is
used. The default *EntityAdapter* class should work for most classes.

 * @see _\Drupal\assocation\Adapter\EntityAdapterInterface_
 * @see _\Drupal\assocation\Adapter\EntityAdapter_


**Adding entity updaters**

Entity updaters are command objects which implement

\Drupal\association\EntityUpdater\EntityUpdaterInterface and are run on
associated content when entity associations are updated. This allows
associations to maintain properties and policies for all the content that
belongs to an association.

New entity updaters can be registered from an event subscriber for the
`\Drupal\association\Event\AssociationEvents::ENTITY_UPDATER_ALTER` event.

 * @see _\Drupal\association\Event\AssociationEvents_
 * @see _\Drupal\association\EntityUpdater\AssociatedEntityUpdaterAlterEvent_
 * @see _\Drupal\association\EntityUpdater\EntityUpdaterInterface_
 * @see _\Drupal\association\EntityUpdater\PathAliasUpdater_ for an example


**Adding association negotiators**

Association negotiators are a tagged service which implement the
`\Drupal\association\AssociationNegotiatorInterface` and register as a service
the tag "association_negotiator".

These negotiators are used to determine the active association for contexts,
routes and from associated content entities. Implement a negotiator when you
want to add custom logic for determining the current association.

 * @see _\Drupal\association\DefaultAssociatonNegotiator_ for an example.


ROADMAP
-------
  * EntityListManager using views and pagination
  * Association heirarchy and nesting support
  * Enhanced access control
    * Per association per user access
    * `Group` module support for `Entity Associations`
  * More options for association landing page handlers
  * Automated tests coverage for `entity access`, `entity adapters`, `forms`,
    `entity updaters`, and `behavior plugins`


MAINTAINERS
-----------

Current maintainers:
 * Liem Khuu (lemming) - https://www.drupal.org/u/lemming
