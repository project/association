<?php

/**
 * @file
 * Entity Association token definitions and hooks.
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info_alter().
 */
function association_token_info_alter(array &$info): void {
  $tokenMapper = \Drupal::service('token.entity_mapper');
  $entityTypeManager = \Drupal::entityTypeManager();
  $entityAdapterManager = \Drupal::service('strategy.manager.association.entity_adapter');

  foreach ($entityAdapterManager->getEntityTypes() as $entityTypeId) {
    $tokenType = $tokenMapper->getTokenTypeForEntityType($entityTypeId);

    if ($info['tokens'][$tokenType]) {
      $tParams = [
        '@association' => 'Entity Association',
        '@entity_label' => $entityTypeManager->getDefinition($entityTypeId)->getLabel(),
      ];

      $info['tokens'][$tokenType]['associations'] = [
        'name' => t('Referenced @association', $tParams),
        'description' => t('The referenced @association linked to this @entity_label', $tParams),
        'module' => 'association',
        'type' => 'association',
      ];
    }
  }
}

/**
 * Implements hook_token_info().
 */
function association_token_info(): array {
  // Entity Association type definition.
  $info['types']['association'] = [
    'name' => t('Entity Association'),
    'description' => t('Tokens for Entity Associations.'),
    'needs-data' => 'association',
  ];

  // Entity Association token definitions.
  $info['tokens']['association']['name'] = [
    'name' => t('Name'),
    'description' => t('The name of this Entity Association.'),
  ];

  // Ensure the pathauto module is installed and that associations are
  // configured to support path aliasing.
  if (\Drupal::moduleHandler()->moduleExists('pathauto')) {
    $fieldDefs = \Drupal::service('entity_field.manager')
      ->getBaseFieldDefinitions('association');

    if (!empty($fieldDefs['path']) && $fieldDefs['path']->getType() === 'path') {
      $info['tokens']['association']['path'] = [
        'name' => t('Path'),
        'description' => t('The base path of association content.'),
      ];
    }
  }
  return $info;
}

/**
 * Implements hook_tokens().
 */
function association_tokens(string $type, array $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata): array {
  $tokenMapper = \Drupal::service('token.entity_mapper');
  $tokenGenerator = \Drupal::token();
  $languageManager = \Drupal::languageManager();
  $entityAdapterManager = \Drupal::service('strategy.manager.association.entity_adapter');

  $replacements = [];
  $urlOpts = ['absolute' => TRUE];

  if (isset($options['langcode'])) {
    $langcode = $options['langcode'];
    $urlOpts['language'] = $languageManager
      ->getLanguage($langcode);
  }
  else {
    $langcode = $languageManager
      ->getCurrentLanguage()
      ->getId();
  }

  foreach ($entityAdapterManager->getEntityTypes() as $entityTypeId) {
    $tokenType = $tokenMapper->getTokenTypeForEntityType($entityTypeId);

    if ($type == $tokenType && !empty($data[$entityTypeId])) {
      $entity = $data[$entityTypeId];

      // Fetch the entity association if one is attached to this entity.
      $link = $entity->hasField('associations') ? $entity->associations->entity : NULL;
      $association = $link ? $link->getAssociation() : NULL;

      // Chained association tokens.
      if ($association && ($assocTokens = $tokenGenerator->findWithPrefix($tokens, 'associations'))) {
        // Add fragment to url options.
        $replacements += $tokenGenerator->generate(
          'association',
          $assocTokens,
          ['association' => $association],
          $options,
          $bubbleable_metadata
        );
      }
    }
  }

  // Association field tokens.
  if ($type == 'association' && !empty($data['association'])) {
    $association = $data['association'];

    foreach ($tokens as $name => $original) {
      if ($name == 'name') {
        $replacements[$original] = $association->label();
      }
      if ($name == 'path' && $association->hasField('path')) {
        $fieldDef = $association->path->getFieldDefinition();

        if ($fieldDef->getType() === 'path') {
          $replacements[$original] = $association->path->alias;
        }
      }
    }
  }

  return $replacements;
}
