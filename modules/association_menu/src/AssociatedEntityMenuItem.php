<?php

namespace Drupal\association_menu;

use Drupal\association\Entity\AssociatedEntityInterface;
use Drupal\Core\Url;

/**
 * An association menu item which links to an associated entity.
 *
 * Associated entities are either the association page, or an association link
 * entity.
 */
class AssociatedEntityMenuItem extends MenuItemBase implements MenuItemInterface {

  /**
   * String identifier for entity to be used with serialization of menu item.
   *
   * @var string
   *
   * @see \Drupal\association_menu\AssociatedEntityMenuItem::__sleep()
   */
  private string $strEntity;

  /**
   * The associated entity this menu link represents.
   *
   * @var \Drupal\association\Entity\AssociatedEntityInterface|null
   */
  public ?AssociatedEntityInterface $entity;

  /**
   * Creates a new instance of the AssociatedEntityMenuItem class.
   *
   * @param array $values
   *   The menu item values like the ID, enable status.
   * @param \Drupal\association\Entity\AssociatedEntityInterface $entity
   *   The associated entity this menu link should represent.
   */
  public function __construct(array $values, AssociatedEntityInterface $entity) {
    parent::__construct($values);

    $this->strEntity = $entity->getEntityTypeId() . ':' . $entity->id();
    $this->entity = $entity;
  }

  /**
   * Implements the __sleep method prepare data for serialization.
   *
   * Prevent the serialization of the referenced entity, and instead store
   * a entity identity in string form to be restored during __wakeup().
   */
  public function __sleep(): array {
    $this->strEntity = $this->entity->getEntityTypeId() . ':' . $this->entity->id();

    $properties = get_object_vars($this);
    unset($properties['entity']);
    unset($properties['access']);
    unset($properties['url']);
    return array_keys($properties);
  }

  /**
   * Implements the __wakeup method to restore data from serialization.
   */
  public function __wakeup(): void {
    // Tests in isolation potentially unserialize in the parent process.
    $phpunit_bootstrap = isset($GLOBALS['__PHPUNIT_BOOTSTRAP']);
    // @phpstan-ignore-next-line
    if ($phpunit_bootstrap && !\Drupal::hasContainer()) {
      return;
    }

    [$entity_type_id, $entity_id] = explode(':', $this->strEntity, 2);
    // @phpstan-ignore-next-line
    $this->entity = \Drupal::entityTypeManager()
      ->getStorage($entity_type_id)
      ->load($entity_id);
  }

  /**
   * Get the associated entity this menu link implements.
   *
   * @return \Drupal\association\Entity\AssociatedEntityInterface|null
   *   The associated entity this menu link represents.
   */
  public function getEntity(): ?AssociatedEntityInterface {
    return $this->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(): \Stringable|string {
    if (empty($this->title)) {
      // If no explicitly set value, use the entity label as the menu title.
      return $this->entity->label();
    }

    return $this->title;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\association_menu\Exception\InvalidMenuUrlException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\Exception\UndefinedLinkTemplateException
   */
  protected function buildUrl(): Url {
    return $this->entity->toUrl('canonical', $this->getOptions());
  }

}
