<?php

namespace Drupal\association_menu;

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Transforms association menu links into renderable menu tree.
 */
interface AssociationMenuBuilderInterface {

  const MAX_MENU_DEPTH = 9;

  /**
   * Build a renderable menu array.
   *
   * Renders the menu using the Drupal menu theme, and templates.
   *
   * @param array $menu
   *   The loaded menu item data.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match information to determine the menu active trail from.
   *
   * @return array
   *   The renderable array of the menu items, ready to render using the core
   *   menu theme template.
   *
   * @see menu.html.twig
   */
  public function buildMenu(array $menu, ?RouteMatchInterface $route_match = NULL): array;

  /**
   * Organize the menu items into nested tree, and renderable menu links.
   *
   * @param \Drupal\association_menu\MenuItemInterface[] $menu_items
   *   Loaded enabled menu items to build into renderable menu items.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match to determine the menu active trail from.
   *
   * @return array
   *   The built menu items in the array format expected by the menu theme.
   *
   * @see menu.html.twig
   */
  public function buildMenuItems(array $menu_items, ?RouteMatchInterface $route_match = NULL): array;

}
