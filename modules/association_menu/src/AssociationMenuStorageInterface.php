<?php

namespace Drupal\association_menu;

use Drupal\Core\Session\AccountInterface;
use Drupal\association\Entity\AssociatedEntityInterface;
use Drupal\association\Entity\AssociationInterface;

/**
 * Interface for managing association navigation link items.
 */
interface AssociationMenuStorageInterface {

  const TABLE_NAME = 'association_menu_items';
  const ITEM_ENABLED = 1;

  /**
   * Gets the menu cache tags for the provided entity association.
   *
   * @param \Drupal\association\Entity\AssociationInterface $association
   *   The entity association to fetch menu cache tags for.
   *
   * @return string[]
   *   An array of cache tags for the menu of the provided entity association.
   */
  public function getCacheTags(AssociationInterface $association): array;

  /**
   * Clear the internal navigation service cache so menus will be rediscovered.
   *
   * @param int|null $associationId
   *   The unique ID of the navigation's association. If NULL then, all
   *   navigation menus will be flushed.
   */
  public function clearCache(?int $associationId = NULL): void;

  /**
   * Get the loaded menu items to prepare for display.
   *
   * @param \Drupal\association\Entity\AssociationInterface $association
   *   The association to fetch navigation information for.
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   The user account to fetch the menu tree for. This should only the menu
   *   tree, with only items the user has access to.
   *
   * @return array
   *   The navigation menu tree loaded for the requested Association.
   */
  public function getMenu(AssociationInterface $association, ?AccountInterface $account = NULL): array;

  /**
   * Remove all menu items for an Entity Association.
   *
   * @param \Drupal\association\Entity\AssociationInterface $association
   *   The entity association to remove all menu items from.
   */
  public function deleteMenu(AssociationInterface $association): void;

  /**
   * Fetch all available menu item, for editing and management forms.
   *
   * This method loads the menu items, and prepares them for use with admin
   * forms, and editing.
   *
   * @param \Drupal\association\Entity\AssociationInterface $association
   *   The association to fetch navigation information for.
   * @param array $filters
   *   Additional conditions to apply when getting the menu items. The array
   *   keys are the condition fields and the array values are the field values
   *   to match.
   * @param bool $flatten
   *   Should the menu items be return flatten with all items at a single depth.
   *   The default FALSE value, will return the menu items built as a tree.
   *
   * @return \Drupal\association_menu\MenuItemInterface[]
   *   An array of menu items belonging to the association.
   */
  public function getMenuItems(AssociationInterface $association, array $filters = [], bool $flatten = FALSE): array;

  /**
   * Fetches the menu item data for a single item in a format for admin forms.
   *
   * @param \Drupal\association\Entity\AssociationInterface $association
   *   The association which owns the menu item being requested.
   * @param int $menu_item_id
   *   The ID of the menu item to fetch.
   *
   * @return \Drupal\association_menu\MenuItemInterface
   *   The loaded menu item data.
   *
   * @throws \InvalidArgumentException
   *   If the menu item doesn't belong to the association provided or if no
   *   menu item exists matching the ID provided.
   */
  public function getMenuItem(AssociationInterface $association, int $menu_item_id): MenuItemInterface;

  /**
   * Add or update menu items to an association menu.
   *
   * @param \Drupal\association\Entity\AssociationInterface $association
   *   The association to add this navigation item to.
   * @param array $values
   *   Menu item data, including the title, route, entity, parent, weight,
   *   enabled status and URL options.
   */
  public function saveMenuItem(AssociationInterface $association, array $values): void;

  /**
   * Removes a menu item from the navigation.
   *
   * This delete method will reject removal of menu items that are linked to
   * associated entity content. These should only be disabled, but cannot be
   * removed unless the entire association is being deleted.
   *
   * @param \Drupal\association\Entity\AssociationInterface $association
   *   The association which owns the menu item being deleted.
   * @param int $menu_item_id
   *   The ID of the menu item to delete.
   *
   * @return bool
   *   Returns TRUE if any menu items were deleted, or FALSE of no matching
   *   items were found. The failure to delete could be a result of the link
   *   having associated entity information.
   */
  public function deleteMenuItem(AssociationInterface $association, int $menu_item_id): bool;

  /**
   * Update tree related data for the association menu links.
   *
   * @param \Drupal\association\Entity\AssociationInterface $association
   *   The entity association which owns the menu items being updated.
   * @param array $menu_items
   *   An array of menu items to update. Menu link ID must be available as this
   *   method only updates existing menu items.
   */
  public function updateMenuTree(AssociationInterface $association, array $menu_items): void;

  /**
   * Adds an associated entity to the navigation menu.
   *
   * @param \Drupal\association\Entity\AssociatedEntityInterface $entity
   *   Add an associated entity to the navigation menu.
   * @param array $options
   *   Additional URL options if there are any.
   */
  public function addAssociated(AssociatedEntityInterface $entity, array $options = []): void;

  /**
   * Removes an associated entity from the navigation menu.
   *
   * @param \Drupal\association\Entity\AssociatedEntityInterface $entity
   *   The associated entity to remove from the navigation menu.
   */
  public function removeAssociated(AssociatedEntityInterface $entity): void;

}
