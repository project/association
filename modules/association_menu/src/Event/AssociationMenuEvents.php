<?php

namespace Drupal\association_menu\Event;

/**
 * Defines events for the entity association menu module.
 */
final class AssociationMenuEvents {

  /**
   * Name of the event fired when building association links for rendering.
   *
   * This event allows modules to insert, remove and alter menu links for
   * association menus as menu items are loaded. These changes will be cached
   * with the menu item storage.
   *
   * This can allow modules to insert programatic links into an association
   * menu which get cached with the link storage. Changes should only be
   * dependent on the entity association, and not rely on any contexts such as
   * user, route, etc...
   *
   * The event listener recieves a
   * \Drupal\association_menu\Event\MenuLinksLoadEvent instance.
   *
   * @Event
   *
   * @var string
   */
  const MENU_LINKS_LOAD = 'association_menu.menu_links_load';

  /**
   * Name of the event fired when building association links for rendering.
   *
   * This event allows modules to insert, remove and alter menu links for
   * association menus after menu items are loaded and processed but before
   * they are rendered.
   *
   * This can allow modules to insert dynamic links into an association
   * menu. The event listener receives a
   * \Drupal\association_menu\Event\MenuLinksAlterEvent instance.
   *
   * @Event
   *
   * @var string
   */
  const MENU_LINKS_ALTER = 'association_menu.menu_links_alter';

}
