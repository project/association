<?php

namespace Drupal\association_menu\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Session\AccountInterface;
use Drupal\association\Entity\AssociationInterface;

/**
 * Event class for altering association menu links.
 *
 * Event is dispatched after menu items are loaded, but before URL access
 * checks are evaluated on the menu items and before they are rendered.
 *
 * This is the event to use if the intention is to dynamically alter the
 * association menu. If the menu changes are always going to be applied and can
 * be cached (not dependent on anything except the association), listen to the
 * \Drupal\association_menu\Event\AssociationMenuEvents::MENU_LINKS_LOAD event
 * instead.
 */
class MenuLinksAlterEvent extends Event {

  /**
   * The user account context the menu is built for.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $account;

  /**
   * The entity association the menu is being built for.
   *
   * @var \Drupal\association\Entity\AssociationInterface
   */
  protected AssociationInterface $association;

  /**
   * The cache metadata for the association menu.
   *
   * @var \Drupal\Core\Cache\CacheableMetadata
   */
  protected CacheableMetadata $cache;

  /**
   * Association menu items to alter.
   *
   * @var \Drupal\association_menu\MenuItemInterface[]
   */
  protected $menuItems;

  /**
   * Creates a new instance of the MenuLinksAlterEvent class.
   *
   * @param \Drupal\association_menu\MenuItemInterface[] $menu_items
   *   The menu items to be altered by listeners of this event.
   * @param \Drupal\association\Entity\AssociationInterface $association
   *   The entity association the menu belongs to.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account context the menu is being built with.
   * @param \Drupal\Core\Cache\CacheableMetadata $cache
   *   The menu cache metadata.
   */
  public function __construct(array &$menu_items, AssociationInterface $association, AccountInterface $account, CacheableMetadata $cache) {
    $this->menuItems = &$menu_items;
    $this->account = $account;
    $this->association = $association;
    $this->cache = $cache;
  }

  /**
   * Gets the association menu cache metadata.
   *
   * Event listeners are expected to update the cache metadata when they
   * alter the menu links, if the menu changes are based on cacheable
   * dependencies or contexts.
   *
   * For instance, menu items are inserted for nodes from the current route. The
   * cache context "route.node" should be added.
   *
   * @return \Drupal\Core\Cache\CacheableMetadata
   *   The association menu cache metadata.
   */
  public function getCache(): CacheableMetadata {
    return $this->cache;
  }

  /**
   * The user account the menu access checks will be run against.
   *
   * @return \Drupal\Core\Session\AccountInterface
   *   The user account these menu items are being generated for and will be
   *   check for access against.
   */
  public function getAccount(): AccountInterface {
    return $this->account;
  }

  /**
   * Gets the entity association these menu items belong to.
   *
   * @return \Drupal\association\Entity\AssociationInterface
   *   The entity association the menu is being build for.
   */
  public function getAssociation(): AssociationInterface {
    return $this->association;
  }

  /**
   * Get a reference to the menu link items array to alter.
   *
   * @return \Drupal\association_menu\MenuItemInterface[]
   *   Reference to the array of menu items which are being altered. This
   *   allows the entire set of menu items to be manipulated before rendering.
   */
  public function &getMenuItems(): array {
    return $this->menuItems;
  }

}
