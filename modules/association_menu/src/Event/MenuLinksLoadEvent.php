<?php

namespace Drupal\association_menu\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\association\Entity\AssociationInterface;

/**
 * Event class for manipulate association menu links being loaded.
 *
 * Event is dispatched as menu items are loaded from the database but before
 * they get cached or have their metadata computed. To make dynamic menu
 * changes (changes that rely on context) use the
 * \Drupal\association_menu\Event\AssociationMenuEvents::MENU_LINKS_LOAD
 * event instead.
 */
class MenuLinksLoadEvent extends Event {

  /**
   * The entity association the menu is being built for.
   *
   * @var \Drupal\association\Entity\AssociationInterface
   */
  protected AssociationInterface $association;

  /**
   * Cache metadata for the menu items.
   *
   * @var \Drupal\Core\Cache\CacheableMetadata
   */
  protected CacheableMetadata $cache;

  /**
   * Reference to the menu items being loaded.
   *
   * @var \Drupal\association_menu\MenuItemInterface[]
   */
  protected array $menuItems;

  /**
   * Creates a new instance of the MenuLinksAlterEvent class.
   *
   * @param \Drupal\association_menu\MenuItemInterface[] $menu_items
   *   The menu items to be altered by listeners of this event.
   * @param \Drupal\association\Entity\AssociationInterface $association
   *   The entity association the menu belongs to.
   * @param \Drupal\Core\Cache\CacheableMetadata $cache
   *   The menu cache metadata.
   */
  public function __construct(array &$menu_items, AssociationInterface $association, CacheableMetadata $cache) {
    $this->menuItems = &$menu_items;
    $this->association = $association;
    $this->cache = $cache;
  }

  /**
   * Gets the association menu cache metadata.
   *
   * Event listeners are expected to update the cache metadata when they
   * alter the menu links, if the menu changes are based on cacheable
   * dependencies or contexts.
   *
   * For instance, menu items are inserted for nodes from the current route.
   * The cache context "route.node" should be added.
   *
   * @return \Drupal\Core\Cache\CacheableMetadata
   *   The association menu cache metadata.
   */
  public function getCache(): CacheableMetadata {
    return $this->cache;
  }

  /**
   * Gets the entity association these menu items belong to.
   *
   * @return \Drupal\association\Entity\AssociationInterface
   *   The entity association the menu is being build for.
   */
  public function getAssociation(): AssociationInterface {
    return $this->association;
  }

  /**
   * Get a reference to the menu link items array of the menu being loaded.
   *
   * @return \Drupal\association_menu\MenuItemInterface[]
   *   Reference to the array of menu items which are being loaded. This
   *   allows the entire set of menu items to be manipulated before being
   *   cached.
   */
  public function &getMenuItems(): array {
    return $this->menuItems;
  }

}
