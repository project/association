<?php

namespace Drupal\association_menu\Exception;

/**
 * Exception for menu item which cannot generate a valid URL for the menu link.
 *
 * Menu items are capable of generating a URL from a referenced entity,
 * a route definition or a simple URI string. If none of these are available
 * the menu item is invalid.
 *
 * Non-link menu items should use the "<nolink>" route so a valid URL instance
 * can still be created.
 */
class InvalidMenuUrlException extends \Exception {

  /**
   * Creates a new instance of the InvalidMenuUrlException.
   *
   * @param string $message
   *   The exception message.
   * @param int $code
   *   A user defined error code.
   * @param \Throwable|null $previous
   *   The previous exception if this exception was nested.
   */
  public function __construct(string $message = '', int $code = 0, ?\Throwable $previous = NULL) {
    if (empty($message)) {
      $message = 'Menu item was not able to generate a valid URL';
    }

    parent::__construct($message, $code, $previous);
  }

}
