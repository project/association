<?php

namespace Drupal\association_menu\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\association\Entity\AssociationInterface;
use Drupal\association_menu\AssociatedEntityMenuItem;
use Drupal\association_menu\AssociationMenuStorageInterface;
use Drupal\association_menu\MenuItemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Form for editing and adding items to an association menu.
 */
class MenuItemDeleteConfirm extends ConfirmFormBase {

  /**
   * The association the menu item belongs to.
   *
   * @var \Drupal\association\Entity\AssociationInterface|null
   */
  protected ?AssociationInterface $association = NULL;

  /**
   * The menu item being deleted.
   *
   * @var \Drupal\association_menu\MenuItemInterface|null
   */
  protected ?MenuItemInterface $menuItem = NULL;

  /**
   * Association menu item storage manager.
   *
   * @var \Drupal\association_menu\AssociationMenuStorageInterface
   */
  protected AssociationMenuStorageInterface $menuStorage;

  /**
   * Create a new instance of the association menu delete confirm form.
   *
   * @param \Drupal\association_menu\AssociationMenuStorageInterface $association_menu_storage
   *   Association navigation storage manager.
   */
  public function __construct(AssociationMenuStorageInterface $association_menu_storage) {
    $this->menuStorage = $association_menu_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('association_menu.storage')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'association_menu_delete_item_confirm';
  }

  /**
   * Builds the form elements for the association menu item delete confirm.
   *
   * @param array $form
   *   Current form structure and elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state, values and build information.
   * @param \Drupal\association\Entity\AssociationInterface|null $association
   *   The entity association which this menu item is being deleted.
   * @param int|null $menu_item_id
   *   The ID of the menu item to delete.
   *
   * @return array
   *   The confirmation form elements for confirming the deletion.
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?AssociationInterface $association = NULL, ?int $menu_item_id = NULL): array {
    try {
      if (!$association || !$menu_item_id) {
        throw new NotFoundHttpException();
      }

      $menuItem = $this->menuStorage->getMenuItem($association, $menu_item_id);

      // Menu items which come from associated entity content,
      // should not be allowed to be deleted here.
      if ($menuItem instanceof AssociatedEntityMenuItem) {
        throw new AccessDeniedHttpException();
      }
    }
    catch (\InvalidArgumentException $e) {
      throw new NotFoundHttpException();
    }

    // Attach the required menu link data to this form instance.
    $this->association = $association;
    $this->menuItem = $menuItem;

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): \Stringable|string {
    return $this->t('Are you sure you want to delete this menu item from @label?', [
      '@label' => $this->association->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): \Stringable|string {
    return $this->t('Menu item %title will be deleted. This action cannot be undone.', [
      '%title' => $this->menuItem->getTitle() ?: '<missing title>',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return Url::fromRoute('association_menu.entity.menu_form', [
      'association' => $this->association->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $assoc = $this->association;
    $itemId = $this->menuItem->id();

    if ($this->menuStorage->deleteMenuItem($assoc, $itemId)) {
      $this
        ->messenger()
        ->addStatus($this->t('Successfully removed the menu link %title.', [
          '%title' => $this->menuItem->getTitle(),
        ]));
    }
    else {
      $this
        ->messenger()
        ->addError($this->t('Unable to remove menu link.'));
    }

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
