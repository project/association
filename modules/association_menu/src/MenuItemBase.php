<?php

namespace Drupal\association_menu;

use Drupal\Core\Url;

/**
 * Base class for association menu items.
 */
abstract class MenuItemBase implements MenuItemInterface {

  /**
   * The menu item unique identifier.
   *
   * @var int|null
   */
  public ?int $id;

  /**
   * The item's parent menu ID (0 if item is at the root).
   *
   * @var int
   */
  public int $parent;

  /**
   * The link text to be used for the menu item.
   *
   * @var \Stringable|string
   */
  public \Stringable|string $title;

  /**
   * Is this menu item enabled?
   *
   * @var bool
   */
  public bool $enabled = TRUE;

  /**
   * Are the menu children configured to be expanded?
   *
   * @var bool
   */
  public bool $expanded = FALSE;

  /**
   * Menu item URL options as described by \Drupal\Core\Url::fromUri().
   *
   * @var array
   */
  public array $options;

  /**
   * The sorting weight of the menu item.
   *
   * @var int
   */
  public int $weight;

  /**
   * The menu depth of the menu item in the menu tree.
   *
   * @var int
   */
  public int $depth;

  /**
   * Children menu items if set.
   *
   * @var \Drupal\association_menu\MenuItemInterface[]
   */
  public array $children = [];

  /**
   * The menu item URL.
   *
   * @var \Drupal\Core\Url|null
   *
   * @see \Drupal\association_menu\MenuItemBase::buildUrl()
   */
  protected ?Url $url;

  /**
   * Is this menu item set to be visible.
   *
   * Access is computed externally from this menu item and based on the context
   * that the menu is being generated for. This value just helps to track that
   * computed state.
   *
   * @var bool
   */
  protected bool $access;

  /**
   * Create a new MenuItemBase instance.
   *
   * @param array $values
   *   The menu item values to set in the new menu item instance.
   */
  public function __construct(array $values) {
    $this->id = $values['id'] ?? NULL;
    $this->parent = $values['parent'] ?? 0;
    $this->title = $values['title'] ?? '';
    $this->enabled = $values['enabled'] ?? TRUE;
    $this->expanded = $values['expanded'] ?? TRUE;
    $this->depth = $values['depth'] ?? 0;
    $this->weight = $values['weight'] ?? 0;
    $this->options = $values['options'] ?? [];
  }

  /**
   * Prevent serialization of specific menu item properties.
   *
   * Menu items maybe serialized into the cache but there maybe certain
   * properties of the menu item which we don't want to get serialized or
   * stored into the cache.
   */
  public function __sleep(): array {
    $values = get_object_vars($this);
    unset($values['url']);
    unset($values['access']);

    return array_keys($values);
  }

  /**
   * {@inheritdoc}
   */
  public function id(): ?int {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function getParentId(): int {
    return $this->parent;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(): \Stringable|string {
    return $this->title;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(): bool {
    return $this->enabled;
  }

  /**
   * {@inheritdoc}
   */
  public function isExpanded(): bool {
    return $this->expanded;
  }

  /**
   * {@inheritdoc}
   */
  public function isCollapsed(): bool {
    return !$this->expanded;
  }

  /**
   * {@inheritdoc}
   */
  public function hasAccess(): bool {
    return $this->access;
  }

  /**
   * {@inheritdoc}
   */
  public function setAccess($access): void {
    $this->access = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight(): int {
    return $this->weight;
  }

  /**
   * {@inheritdoc}
   */
  public function getOptions(): array {
    return $this->options;
  }

  /**
   * {@inheritdoc}
   */
  public function &getChildren(): array {
    return $this->children;
  }

  /**
   * {@inheritdoc}
   */
  public function getUrl(): Url {
    if (!isset($this->url)) {
      $this->url = $this->buildUrl();
    }

    return $this->url;
  }

  /**
   * Generate the URL for this menu item.
   *
   * @return \Drupal\Core\Url
   *   A newly generated URL instance for this menu item based on the menu
   *   item subclass.
   */
  abstract protected function buildUrl(): Url;

}
