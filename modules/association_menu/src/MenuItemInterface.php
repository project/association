<?php

namespace Drupal\association_menu;

use Drupal\Core\Url;

/**
 * The menu item interface for association menus.
 */
interface MenuItemInterface {

  /**
   * Get the menu item identifier.
   *
   * @return int|null
   *   Returns the menu item identifier, NULL if menu item has not be saved
   *   yet and so does not have an identifier assigned yet.
   */
  public function id(): ?int;

  /**
   * Sets the current access state of this menu item.
   *
   * @param bool $access
   *   The access state to set for this menu item.
   */
  public function setAccess($access): void;

  /**
   * Get the current access status.
   *
   * Menu items do not compute access, and only store the access set from
   * external contexts.
   *
   * @return bool
   *   Is the menu item access set to allow access.
   */
  public function hasAccess(): bool;

  /**
   * Get the menu item title.
   *
   * @return \Stringable|string
   *   Get the value to use as the menu title.
   */
  public function getTitle(): \Stringable|string;

  /**
   * Gets the menu item of the parent menu item.
   *
   * @return int
   *   The menu item ID of the parent menu item (0 if root).
   */
  public function getParentId(): int;

  /**
   * Gets the enabled state of this menu item.
   *
   * @return bool
   *   Is this menu item enabled?
   */
  public function isEnabled(): bool;

  /**
   * Check if the menu item is expanded.
   *
   * @return bool
   *   Are the menu item children expanded?
   */
  public function isExpanded(): bool;

  /**
   * Checks if the menu item children are collapsed.
   *
   * @return bool
   *   Are the menu item children collapsed?
   */
  public function isCollapsed(): bool;

  /**
   * Gets the sorting weight of this menu item.
   *
   * @return int
   *   The sorting weight of this menu item relative to it's siblings.
   */
  public function getWeight(): int;

  /**
   * Get the menu link URL options.
   *
   * @return array
   *   The options to use when generating this menu link URL.
   */
  public function getOptions(): array;

  /**
   * Get a reference to the child menu items.
   *
   * @return \Drupal\association_menu\MenuItemInterface[]
   *   Reference to array of children menu item. Empty array is returned if
   *   this menu item has no children.
   */
  public function &getChildren(): array;

  /**
   * Get the URL for this menu item.
   *
   * @return \Drupal\Core\Url
   *   The URL for this menu item. A menu item without a link should be a URL
   *   instance with a "<nolink>" route.
   *
   * @throws \Drupal\association_menu\Exception\InvalidMenuUrlException
   */
  public function getUrl(): Url;

}
