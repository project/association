<?php

namespace Drupal\association_menu\Plugin\Block;

use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\Plugin\Context\EntityContextDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\association\AssociationNegotiatorInterface;
use Drupal\association\Plugin\Block\AssociationBlockBase;
use Drupal\association_menu\AssociationMenuBuilderInterface;
use Drupal\association_menu\AssociationMenuStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Creates a block plugin, which displays association navigation.
 */
#[Block(
  id: 'association_menu',
  admin_label: new TranslatableMarkup('Association menu'),
  category: new TranslatableMarkup('Entity association'),
  context_definitions: [
    'association' => new EntityContextDefinition('entity:association', new TranslatableMarkup('Association'), FALSE),
    'entity' => new ContextDefinition('entity', new TranslatableMarkup('Entity'), FALSE),
  ],
)]
class AssociationMenuBlock extends AssociationBlockBase implements ContainerFactoryPluginInterface {

  /**
   * The loaded association menu definition (menu link items and cache data).
   *
   * @var array|null
   */
  protected ?array $menuData = NULL;

  /**
   * Fetches and saves manager for association navigation menu items.
   *
   * @var \Drupal\association_menu\AssociationMenuStorageInterface
   */
  protected AssociationMenuStorageInterface $menuStorage;

  /**
   * Builds association navigation data into renderable menu.
   *
   * @var \Drupal\association_menu\AssociationMenuBuilderInterface
   */
  protected AssociationMenuBuilderInterface $menuBuilder;

  /**
   * Create a new instance of a AssociationMenuBlock block plugin.
   *
   * @param array $configuration
   *   The block configuration.
   * @param string $plugin_id
   *   The block plugin identifier.
   * @param mixed $plugin_definition
   *   The block plugin definition.
   * @param \Drupal\association\AssociationNegotiatorInterface $association_negotiator
   *   Service to resolve the association from the association context.
   * @param \Drupal\association_menu\AssociationMenuStorageInterface $association_menu_storage
   *   Fetches and saves manager for association menu items.
   * @param \Drupal\association_menu\AssociationMenuBuilderInterface $association_menu_builder
   *   Builds association menu data into renderable array.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AssociationNegotiatorInterface $association_negotiator, AssociationMenuStorageInterface $association_menu_storage, AssociationMenuBuilderInterface $association_menu_builder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $association_negotiator);

    $this->menuStorage = $association_menu_storage;
    $this->menuBuilder = $association_menu_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('association.negotiator'),
      $container->get('association_menu.storage'),
      $container->get('association_menu.builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getAssociationContextNames(): array {
    return [
      'layout_builder' => 'entity',
      'global' => 'association',
    ];
  }

  /**
   * Get the menu definition of the menu belonging to the block's association.
   *
   * @return array
   *   The menu definition of the association context of this block.
   */
  protected function getMenu(): array {
    if (!isset($this->menuData)) {
      $this->menuData = [];

      if ($assoc = $this->getAssociation()) {
        $this->menuData = $this->menuStorage->getMenu($assoc);
      }
    }

    return $this->menuData;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(): array {
    $cacheTags = parent::getCacheTags();

    $menu = $this->getMenu();
    if (!empty($menu['cache'])) {
      $cacheTags = Cache::mergeTags($cacheTags, $menu['cache']->getCacheTags());
    }

    return $cacheTags;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts(): array {
    $cacheContexts = parent::getCacheContexts();

    $menu = $this->getMenu();
    if (!empty($menu['cache'])) {
      $cacheContexts = Cache::mergeTags($cacheContexts, $menu['cache']->getCacheContexts());
    }

    // Cache by route because each route can change which menu items end up in
    // the active trail.
    $cacheContexts = Cache::mergeContexts($cacheContexts, ['route']);
    return $cacheContexts;
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build = [];
    $menu = $this->getMenu();

    if (!empty($menu['items'])) {
      $build['menu'] = $this->menuBuilder->buildMenu($menu);
    }

    return $build;
  }

}
