<?php

namespace Drupal\association_menu\Plugin\Toolshed\ThirdPartyConfig;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\toolshed\Attribute\ToolshedThirdPartyConfig;
use Drupal\toolshed\Plugin\ThirdPartyConfigBase;

/**
 * Creates third party configuration for entity association menus.
 *
 * @ToolshedThirdPartyConfig(
 *   id = "association_menu",
 *   label = @Translation("Menu settings"),
 *   entity_types = {
 *     "association_type",
 *   },
 * )
 */
#[ToolshedThirdPartyConfig(
  id: 'association_menu',
  label: new TranslatableMarkup('Menu settings'),
  entity_types: [
    'association_type',
  ]
)]
class AssociationMenuThirdPartyConfig extends ThirdPartyConfigBase {

  /**
   * {@inheritdoc}
   */
  public function getDefaultSettings(): array {
    return [
      'menu_nesting' => FALSE,
      'enable_tags' => [
        'mode' => 1,
        'tags' => [],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function massageValues(ConfigEntityInterface $entity, array $values): array {
    $values['enabled_tags']['tags'] = array_filter($values['enabled_tags']['tags'] ?? []);
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  protected function formElements(ConfigEntityInterface $entity, array $settings, FormStateInterface $form_state): array {
    /** @var \Drupal\association\Entity\AssociationTypeInterface $entity */
    $elements['menu_nesting'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow nesting for association menu items'),
      '#default_value' => $settings['menu_nesting'],
    ];

    $elements['enabled_tags'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Default enabled items'),
      '#description_display' => 'before',
      '#description' => $this->t('Entities list does not update until after settings are saved.'),
      '#tree' => TRUE,

      'mode' => [
        '#type' => 'radios',
        '#title' => $this->t("Which entity's menu items should be enabled by default"),
        '#options' => [
          0 => $this->t('Only those selected'),
          1 => $this->t('All except those selected'),
        ],
        '#default_value' => $settings['enable_tags']['mode'] ?? 1,
      ],
      'tags' => [
        '#type' => 'checkboxes',
        '#title' => $this->t('Entities'),
        '#options' => $entity->getBehavior()?->getTags() ?? [],
        '#default_value' => $settings['enable_tags']['tags'] ?? [],
      ],
    ];

    return $elements;
  }

}
