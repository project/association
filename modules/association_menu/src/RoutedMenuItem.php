<?php

namespace Drupal\association_menu;

use Drupal\Core\Url;

/**
 * Association menu items which are routed.
 */
class RoutedMenuItem extends MenuItemBase implements MenuItemInterface {

  /**
   * The route name.
   *
   * @var string
   */
  public string $routeName;

  /**
   * Route parameters for the menu item.
   *
   * @var array
   */
  public array $routeParams;

  /**
   * Creates a new instance of RoutedMenuItem class.
   *
   * @param array $values
   *   The base menu item values (such as id, parent, weight, etc...).
   * @param string $route_name
   *   The route name.
   * @param array $route_params
   *   The route parameters.
   */
  public function __construct(array $values, string $route_name, array $route_params = []) {
    parent::__construct($values);

    $this->routeName = $route_name;
    $this->routeParams = $route_params;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildUrl(): Url {
    return Url::fromRoute($this->routeName, $this->routeParams, $this->options);
  }

}
