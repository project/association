<?php

namespace Drupal\association_menu;

use Drupal\Core\Url;

/**
 * Class for association menu items from a URI string.
 */
class UriMenuItem extends MenuItemBase implements MenuItemInterface {

  /**
   * The URI string of an unrouted path or external link.
   *
   * @var string
   */
  public string $uri;

  /**
   * Create a new instance of the UriMenuItem class.
   *
   * @param array $values
   *   The base menu item values (such as id, parent, weight, etc...).
   * @param string $uri
   *   The URI string to use when constructing the URL::fromUri().
   */
  public function __construct(array $values, string $uri) {
    parent::__construct($values);

    $this->uri = $uri;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildUrl(): Url {
    return Url::fromUri($this->uri, $this->options);
  }

}
