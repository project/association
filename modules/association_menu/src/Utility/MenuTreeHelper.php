<?php

namespace Drupal\association_menu\Utility;

/**
 * Utitly methods for helping with common menu tree functionality.
 */
class MenuTreeHelper {

  /**
   * Recursively discovers the menu item depths.
   *
   * @param array $items
   *   The full set of menu item values. This allows finding the parent depths
   *   or updating the parent's depths as needed.
   * @param array $item
   *   A reference to the array of menu item values to find the menu
   *   depth for.
   */
  public static function setItemDepth(array &$items, array &$item): void {
    $parent = $item['parent'];

    if (empty($parent) || empty($items[$parent])) {
      $item['depth'] = 0;
    }
    else {
      if (!isset($items[$parent]['depth'])) {
        static::setItemDepth($items, $items['parent']);
      }

      $item['depth'] = $items[$parent]['depth'] + 1;
    }
  }

}
