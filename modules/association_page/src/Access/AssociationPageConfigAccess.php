<?php

namespace Drupal\association_page\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\association\Entity\AssociationInterface;
use Drupal\association\Entity\AssociationTypeInterface;
use Drupal\Core\Access\AccessResultInterface;
use Symfony\Component\Routing\Route;

/**
 * Determines if page configuration is enabled for association type admin.
 */
class AssociationPageConfigAccess implements AccessInterface {

  /**
   * The key used by the routing requirement.
   *
   * @var string
   */
  protected string $requirementsKey = '_association_page_config';

  /**
   * {@inheritdoc}
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account): AccessResultInterface {
    $paramName = $route->getRequirement($this->requirementsKey);
    $entity = $route_match->getParameter($paramName);

    if ($entity instanceof AssociationInterface) {
      $entity = $entity->getType();
    }
    elseif (!$entity instanceof AssociationTypeInterface) {
      return AccessResult::forbidden();
    }

    $plugin = $entity->getLandingPageHandler();
    $isAssocPage = $plugin && 'association_page' === $plugin->getPluginId();
    return AccessResult::allowedIf($isAssocPage)
      ->addCacheableDependency($entity);
  }

}
