<?php

namespace Drupal\association_page;

use Drupal\association\AssociationPermissions;
use Drupal\association\Entity\AssociationTypeInterface;

/**
 * Utility for building available bundle permissions for association entities.
 */
class AssociationPagePermissions extends AssociationPermissions {

  /**
   * Fetch association entity operations available for an association type.
   *
   * @param \Drupal\association\Entity\AssociationTypeInterface $type
   *   The association_type bundle entity.
   *
   * @return array<\Stringable|string>
   *   An array of operation labels, keyed by the string identifier for the
   *   operation. For the case of association entities, these are the access
   *   permission identifiers, and for association landing page entities, the
   *   operations are prefixed with "page.".
   */
  public static function getBundleOperations(AssociationTypeInterface $type): array {
    if ('association_page' === $type->getLandingPageHandler()->getPluginId()) {
      return [
        'page_update' => t('Edit landing page for'),
        'page_revisions' => t('Curate landing page revisions for'),
      ];
    }

    return [];
  }

}
