<?php

namespace Drupal\association_page\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Route controller for building association management pages and revisions.
 */
class AssociationPageController implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTYpeManagerInterface $entityTypeManager;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * Create a new instance of AssociationPageController class.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, DateFormatterInterface $date_formatter) {
    $this->entityTypeManager = $entity_type_manager;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('date.formatter')
    );
  }

  /**
   * Generates an overview table of older revisions of a association page.
   *
   * @param string $entity_type_id
   *   The entity type ID, which should also be the ID of the entity parameter.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview($entity_type_id, RouteMatchInterface $route_match): array {
    /** @var \Drupal\Core\Entity\ContentEntityInterface */
    $entity = $route_match->getParameter($entity_type_id);

    // If entity does not implementat the RevisionableInterface we should
    // consider thie entity as unrevisionable, and shouldn't be rendering a
    // revision overview page for it.
    if (!$entity instanceof RevisionableInterface) {
      throw new NotFoundHttpException();
    }

    /** @var \Drupal\association_page\Entity\Storage\AssociationPageStorage */
    $entityStorage = $this->entityTypeManager->getStorage($entity_type_id);
    $accessHandler = $this->entityTypeManager->getAccessControlHandler($entity_type_id);

    $language = $entity->language();
    $langcode = $language->getId();
    $canRevert = $accessHandler->access($entity, 'revision_revert');
    $canDelete = $accessHandler->access($entity, 'revision_delete');

    // Determine the revision page title based on the number of translations.
    if (count($entity->getTranslationLanguages()) > 1) {
      $build['#title'] = $this->t('@langname revisions for %title', [
        '@langname' => $language->getName(),
        '%title' => $entity->label(),
      ]);
    }
    else {
      $build['#title'] = $this->t('Revisions for %title', [
        '%title' => $entity->label(),
      ]);
    }

    $rows = [];
    foreach ($entityStorage->revisionIds($entity, $langcode) as $revId) {
      /** @var \Drupal\Core\Entity\RevisionableContentEntityBase */
      $revision = $entityStorage->loadRevision($revId);

      if ($revision && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');

        $row = [];
        $row['revision']['data'] = [
          '#type' => 'inline_template',
          '#template' => '{{ date }}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
          '#context' => [
            'date' => Link::fromTextAndUrl($date, $revision->toUrl('revision')),
            'message' => [
              '#markup' => $revision->getRevisionLogMessage(),
              '#allowed_tags' => Xss::getHtmlTagList(),
            ],
          ],
        ];

        $row['author']['data'] = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Show operations based on if this is the current revision or not.
        if ($entity->getRevisionId() == $revId) {
          $row['operations']['data'] = [
            '#prefix' => '<em>',
            '#markup' => $this->t('Current revision'),
            '#suffix' => '</em>',
          ];

          foreach ($row as &$element) {
            $element['class'][] = 'revision-current';
          }
        }
        else {
          $links = [];

          if ($canRevert) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $revision->toUrl('revision-revert'),
            ];
          }
          if ($canDelete) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => $revision->toUrl('revision-delete'),
            ];
          }

          $row['operations']['data'] = !$links ? [] : [
            '#type' => 'operations',
            '#links' => $links,
          ];
        }

        $rows[$revId] = $row;
      }
    }

    $build['entity_revisions_table'] = [
      '#theme' => 'table',
      '#header' => [
        $this->t('Revision'),
        $this->t('Author'),
        $this->t('Operations'),
      ],
      '#rows' => $rows,
    ];

    return $build;
  }

  /**
   * Page title callback for an entity revision.
   *
   * @param \Drupal\Core\Entity\RevisionableInterface $revision
   *   The association page loaded revision entity.
   *
   * @return \Stringable|string
   *   A title that can be used as the page title for this entity revision.
   */
  public function revisionPageTitle(RevisionableInterface $revision): \Stringable|string {
    if ($revision instanceof RevisionLogInterface) {
      return $this->t('Revision of %title from %date', [
        '%title' => $revision->label(),
        '%date' => $this->dateFormatter->format($revision->getRevisionCreationTime()),
      ]);
    }

    return '';
  }

  /**
   * Displays the revision of the entity of the requested type.
   *
   * @param \Drupal\Core\Entity\RevisionableInterface $revision
   *   The loaded entity revision.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow(RevisionableInterface $revision, RouteMatchInterface $route_match): array {
    $entityTypeId = $revision->getEntityTypeId();
    $entity = $route_match->getParameter($entityTypeId);

    // Ensure that the current revision matches the entity ID.
    if ($revision && (!isset($entity) || $revision->id() == $entity->id())) {
      $viewBuilder = $this->entityTypeManager->getViewBuilder($entityTypeId);

      return $viewBuilder->view($revision);
    }

    throw new NotFoundHttpException();
  }

}
