<?php

namespace Drupal\association_page\Entity\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\association_page\AssociationPagePermissions;
use Drupal\Core\Access\AccessResultInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Access controller association page entity operations.
 */
class PageAccessControlHandler extends EntityAccessControlHandler implements EntityHandlerInterface {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Create a new instance of the association page access handler.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   Entity type definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($entity_type);

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type): static {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL): AccessResultInterface {
    // This entity is autogenerated and should never be use created.
    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResultInterface {
    /** @var \Drupal\association_page\Entity\AssociationPage $entity */
    $association = $entity->getAssociation();

    switch ($operation) {
      case 'view':
        if ($association && $association->isActive()) {
          return AccessResult::allowed();
        }
        // Purposely fall-through, if not published check for edit access.
      case 'update':
        $permKey = 'page_update';
        break;

      case 'revision_view':
      case 'revision_revert':
      case 'revision_delete':
        $permKey = 'page_revisions';
        break;

      default:
        // Prevents "delete" on purpose. Deleting can only be done when
        // the association entity is deleted.
        return AccessResult::forbidden();
    }

    try {
      if ($account->hasPermission($this->entityType->getAdminPermission())) {
        return AccessResult::allowed()->cachePerPermissions();
      }

      $opPermission = AssociationPagePermissions::getBundlePermissionKey($entity->getType(), $permKey);
      return AccessResult::allowedIfHasPermission($account, $opPermission)
        ->addCacheableDependency($association);
    }
    catch (\InvalidArgumentException $e) {
      // Operation doesn't exist for this entity, always return forbidden.
      return AccessResult::forbidden();
    }
  }

}
