<?php

namespace Drupal\association_page\Entity;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\association\Entity\AssociatedEntityInterface;
use Drupal\association\Entity\AssociationInterface;
use Drupal\association\Entity\AssociationType;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\UserInterface;

/**
 * Association entity landing page entity.
 *
 * This entity is dependent on the base association entity, and uses the same
 * entity ID as the corresponding association entity.
 *
 * @ContentEntityType(
 *   id = "association_page",
 *   label = @Translation("Association Page"),
 *   label_plural = @Translation("Association Pages"),
 *   bundel_label = @Translation("Association type"),
 *   bundle_entity_type = "association_type",
 *   field_ui_base_route = "entity.association_page.field_config",
 *   base_table = "association_page",
 *   data_table = "association_page_field_data",
 *   revision_table = "association_page_revision",
 *   revision_data_table = "association_page_field_revision",
 *   admin_permission = "administer association configurations",
 *   permission_granularity = "bundle",
 *   show_revision_ui = TRUE,
 *   translatable = TRUE,
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "title",
 *     "status" = "status",
 *     "published" = "status",
 *     "uid" = "uid",
 *     "langcode" = "langcode",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log"
 *   },
 *   handlers = {
 *     "access" = "Drupal\association_page\Entity\Access\PageAccessControlHandler",
 *     "storage" = "Drupal\association_page\Entity\Storage\AssociationPageStorage",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "translation" = "Drupal\content_translation\ContentTranslationHandler",
 *     "form" = {
 *       "default" = "Drupal\association_page\Entity\Form\AssociationPageForm",
 *       "edit" = "Drupal\association_page\Entity\Form\AssociationPageForm",
 *       "revision_revert" = "Drupal\association_page\Entity\Form\AssociationPageRevisionRevertConfirm",
 *       "revision_delete" = "Drupal\association_page\Entity\Form\AssociationPageRevisionDeleteConfirm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\association_page\Entity\Routing\AssociationPageHtmlRouteProvider",
 *     },
 *   },
 *   links = {
 *     "canonical" = "/association/{association}",
 *     "edit-form" = "/association/{association_page}/page/edit",
 *     "revision-history" = "/association/{association_page}/page/revisions",
 *     "revision" = "/association/{association_page}/page/revisions/{revision}/view",
 *     "revision-revert" = "/association/{association_page}/page/revisions/{revision}/revert",
 *     "revision-delete" = "/association/{association_page}/page/revisions/{revision}/delete",
 *   },
 * )
 */
class AssociationPage extends RevisionableContentEntityBase implements AssociatedEntityInterface, EntityChangedInterface, EntityOwnerInterface, EntityPublishedInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * The association entity this page belongs to.
   *
   * @var \Drupal\association\Entity\AssociationInterface|null
   */
  protected ?AssociationInterface $association = NULL;

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(): array {
    $tags = ['association:' . $this->id()];
    $tags = Cache::mergeTags($tags, parent::getCacheTags());

    return $tags;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTagsToInvalidate(): array {
    // Ensure to inform cache tags that the associated page has been updated,
    // and content lists or blocks need a refresh.
    $tags[] = 'association:links:' . $this->id();
    $tags = Cache::mergeTags($tags, parent::getCacheTagsToInvalidate());

    return $tags;
  }

  /**
   * Default value callback for 'uid' base field definition.
   *
   * @return array
   *   An array containing the default author data.
   */
  public static function getCurrentUserId(): array {
    return [\Drupal::currentUser()->id()];
  }

  /**
   * Gets the association type instance for this landing page.
   *
   * @return \Drupal\association\Entity\AssociationType
   *   Get the bundle entity for this association page.
   */
  public function getType(): AssociationType {
    /** @var \Drupal\association\Entity\AssociationType $type */
    $type = $this->type->entity;
    return $type;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp): self {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner(): UserInterface {
    /** @var \Drupal\user\UserInterface $owner */
    $owner = $this->get('uid')->entity;
    return $owner;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId(): int {
    return (int) $this->get('uid')->target_id ?: 0;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid): self {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account): self {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAssociation(): ?AssociationInterface {
    if (!$this->association) {
      /** @var \Drupal\association\Entity\AssociationInterface|null $assoc */
      $assoc = \Drupal::entityTypeManager()
        ->getStorage('association')
        ->load($this->id());

      $this->association = $assoc;
    }

    return $this->association;
  }

  /**
   * {@inheritdoc}
   */
  public function getTarget(): EntityInterface {
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel): array {
    $routeParams = parent::urlRouteParameters($rel);
    $revisionLinks = [
      'revision',
      'revision-revert',
      'revision-delete',
      'translation-revert',
    ];

    if ('canonical' === $rel) {
      $routeParams['association'] = $this->id();
      unset($routeParams['association_page']);
    }
    elseif (in_array($rel, $revisionLinks)) {
      $routeParams['revision'] = $this->getRevisionId();

      if ($rel === 'translation-revert') {
        $routeParams['langcode'] = $this->language()->getId();
      }
    }

    return $routeParams;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);

    foreach ($this->getTranslationLanguages() as $langcode => $language) {
      /** @var \Drupal\association\Entity\AssociationInterface */
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, then make the page
    // owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preSaveRevision(EntityStorageInterface $storage, \stdClass $record): void {
    parent::preSaveRevision($storage, $record);

    if (!$this->isNewRevision() && isset($this->original) && (!isset($record->revision_log) || $record->revision_log === '')) {
      // Prevent overwrite revision logs when empty, and when not creating a
      // new revision.
      $record->revision_log = $this->original->get('revision_log')->value;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] */
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setTargetEntityTypeId($entity_type->id())
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user who was the author of this page.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(FALSE)
      ->setRequired(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback(static::class . '::getCurrentUserId')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ]);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setTargetEntityTypeId($entity_type->id())
      ->setLabel(t('Title'))
      ->setDescription(t('The title of this landing page.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue('')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ]);

    $fields['status']->setDisplayOptions('form', [
      'type' => 'boolean_checkbox',
      'settings' => [
        'display_label' => TRUE,
      ],
      'weight' => 90,
    ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setTargetEntityTypeId($entity_type->id())
      ->setLabel(t('Created'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDescription(t('The time that the page was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setTargetEntityTypeId($entity_type->id())
      ->setLabel(t('Changed'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the page was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setTargetEntityTypeId($entity_type->id())
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

}
