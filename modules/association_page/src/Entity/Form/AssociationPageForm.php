<?php

namespace Drupal\association_page\Entity\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form association landing page add and edit form.
 *
 * @ingroup association
 */
class AssociationPageForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    $form['advanced'] = [
      '#type' => 'vertical_tabs',
      '#default_tab' => 'revision',
      '#weight' => 50,
    ];

    $form['author'] = [
      '#type' => 'details',
      '#title' => $this->t('Authoring Information'),
      '#group' => 'advanced',
      '#optional' => TRUE,
      '#access' => $form['uid']['#access'] ?? NULL,
    ];

    $form['uid']['#group'] = 'author';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $status = parent::save($form, $form_state);
    $entity = $this->entity;

    $msgParams = ['%label' => $entity->label()];
    $msg = SAVED_NEW === $status
      ? $this->t('Created the %label association landing page.', $msgParams)
      : $this->t('Saved the %label association landing page.', $msgParams);

    $this->messenger()->addStatus($msg);

    // Redirect to the page view.
    $form_state->setRedirectUrl($entity->toUrl('canonical'));

    return $status;
  }

}
