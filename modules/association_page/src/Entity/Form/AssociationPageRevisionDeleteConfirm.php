<?php

namespace Drupal\association_page\Entity\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\toolshed\Entity\Form\RevisionConfirmBase;

/**
 * Provides a form for deleting an association landing page revision.
 */
class AssociationPageRevisionDeleteConfirm extends RevisionConfirmBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): \Stringable|string {
    /** @var \Drupal\Core\Entity\RevisionLogInterface */
    $entity = $this->getEntity();

    return $this->t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => $this->dateFormatter->format($entity->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): \Stringable|string {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    /** @var \Drupal\Core\Entity\RevisionLogInterface */
    $entity = $this->entity;
    /** @var \Drupal\Core\Entity\RevisionableStorageInterface $entityStorage */
    $entityStorage = $this->entityTypeManager
      ->getStorage($entity->getEntityTypeId());

    $tParams = [
      '%title' => $entity->label(),
      '%revision' => $entity->getRevisionId(),
      '%revision-date' => $this->dateFormatter->format($entity->getRevisionCreationTime()),
    ];

    $entityStorage->deleteRevision($entity->getRevisionId());

    $this
      ->getLogger('association')
      ->notice('Association page: deleted %title revision %revision.', $tParams);
    $this
      ->messenger()
      ->addMessage($this->t('Revision from %revision-date of Association page %title has been deleted.', $tParams));

    $form_state->setRedirectUrl($entity->toUrl('revision-history'));
  }

}
