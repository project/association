<?php

namespace Drupal\association_page\Entity\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\toolshed\Entity\Form\RevisionConfirmBase;

/**
 * Provides a form for reverting a association page revision.
 */
class AssociationPageRevisionRevertConfirm extends RevisionConfirmBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): \Stringable|string {
    /** @var \Drupal\Core\Entity\RevisionLogInterface */
    $entity = $this->getEntity();

    return $this->t('Are you sure you want to revert to the revision from %revision-date?', [
      '%revision-date' => $this->dateFormatter->format($entity->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): \Stringable|string {
    return $this->t('Revert');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    /** @var \Drupal\Core\Entity\RevisionLogInterface */
    $entity = $this->entity;

    $tParams = [
      '%title' => $entity->label() ?? '',
      '%revision' => $entity->getRevisionId() ?? 0,
      '%revision-date' => $this->dateFormatter->format($entity->getRevisionCreationTime()),
    ];

    $entity->setNewRevision();
    $entity->isDefaultRevision(TRUE);

    $entity
      ->setRevisionCreationTime($this->time->getRequestTime())
      ->setRevisionLogMessage($this->t('Copy of the revision from %revision-date.', $tParams))
      ->save();

    $this
      ->getLogger('association')
      ->notice('Association page: reverted %title revision %revision.', $tParams);
    $this
      ->messenger()
      ->addMessage($this->t('Association page %title has been reverted to the revision from %revision-date.', $tParams));

    $form_state->setRedirectUrl($entity->toUrl('revision-history'));
  }

}
