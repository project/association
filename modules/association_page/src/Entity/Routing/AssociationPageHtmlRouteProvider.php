<?php

namespace Drupal\association_page\Entity\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Drupal\association_page\Controller\AssociationPageController;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * HTML route provider callback for the association landing page entities.
 */
class AssociationPageHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type): array|RouteCollection {
    $collection = parent::getRoutes($entity_type);
    $entityTypeId = $entity_type->id();

    if ($revisionHistoryRoute = $this->getRevisionHistoryRoute($entity_type)) {
      $collection->add("entity.{$entityTypeId}.revision_history", $revisionHistoryRoute);
    }

    if ($revisionViewRoute = $this->getRevisionViewRoute($entity_type)) {
      $collection->add("entity.{$entityTypeId}.revision", $revisionViewRoute);
    }

    if ($revert_route = $this->getRevisionRevertRoute($entity_type)) {
      $collection->add("entity.{$entityTypeId}.revision_revert", $revert_route);
    }

    if ($delete_route = $this->getRevisionDeleteRoute($entity_type)) {
      $collection->add("entity.{$entityTypeId}.revision_delete", $delete_route);
    }

    return $collection;
  }

  /**
   * Gets the canonical route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getCanonicalRoute(EntityTypeInterface $entity_type): ?Route {
    if ($entity_type->hasLinkTemplate('canonical') && $entity_type->hasViewBuilderClass()) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('canonical'));
      $route
        ->addDefaults([
          '_entity_view' => "{$entity_type_id}.full",
          '_title_callback' => '\Drupal\Core\Entity\Controller\EntityController::title',
        ])
        ->setRequirement('_entity_access', "association.view")
        ->setOption('parameters', [
          'association' => ['type' => 'entity:' . $entity_type_id],
        ]);

      // Entity types with serial IDs can specify this in their route
      // requirements, improving the matching process.
      if ($this->getEntityTypeIdKeyType($entity_type) === 'integer') {
        $route->setRequirement('association', '\d+');
      }

      return $route;
    }

    return NULL;
  }

  /**
   * Create a route for managing entity revisions.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   Entity type definition for the entity type to build the revision route
   *   history list page.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   If a link template definition is available a revision history route then
   *   returns the route for it. Otherwise returns NULL.
   */
  protected function getRevisionHistoryRoute(EntityTypeInterface $entity_type): ?Route {
    if ($entity_type->hasLinkTemplate('revision-history')) {
      $route = new Route($entity_type->getLinkTemplate('revision-history'));
      $label = $entity_type->getLabel();
      $entityTypeId = $entity_type->id();

      $route->addDefaults([
        '_title' => 'Revisions for: ' . $label->getUntranslatedString(),
        '_title_arguments' => $label->getArguments(),
        '_title_context' => $label->getOption('context'),
        '_controller' => AssociationPageController::class . '::revisionOverview',
        'entity_type_id' => $entityTypeId,
      ]);

      $route->setRequirement('_entity_access', $entityTypeId . '.revision_view');
      $route->setRequirement($entityTypeId, '\d+');

      $route->setOption('_admin_route', TRUE);
      $route->setOption('parameters', [
        $entityTypeId => [
          'type' => 'entity:' . $entityTypeId,
        ],
      ]);

      return $route;
    }

    return NULL;
  }

  /**
   * Create a route for viewing a revision of an association landing page.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   If a link template definition is available for the route than create a
   *   route object for it. Otherwise return NULL if no link template defined.
   */
  protected function getRevisionViewRoute(EntityTypeInterface $entity_type): ?Route {
    if ($entity_type->hasLinkTemplate('revision')) {
      $route = new Route($entity_type->getLinkTemplate('revision'));

      $route->setDefault('_controller', AssociationPageController::class . '::revisionShow');
      $route->setDefault('_title_callback', AssociationPageController::class . '::revisionPageTitle');

      $route->setRequirement('_entity_access', $entity_type->id() . '.revision_view');
      $route->setRequirement($entity_type->id(), '\d+');
      $route->setRequirement('revision', '\d+');

      $route->setOption('parameters', [
        $entity_type->id() => [
          'type' => 'entity:' . $entity_type->id(),
        ],
        'revision' => [
          'type' => 'entity_revision:' . $entity_type->id(),
        ],
      ]);

      return $route;
    }

    return NULL;
  }

  /**
   * Create a route for reverting a revision of an entity.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   If a link template definition is available for the route than create a
   *   route object for it. Otherwise return NULL if no link template defined.
   */
  protected function getRevisionRevertRoute(EntityTypeInterface $entity_type): ?Route {
    if ($entity_type->hasLinkTemplate('revision-revert')) {
      $route = new Route($entity_type->getLinkTemplate('revision-revert'));

      $route->setDefaults([
        '_title_callback' => AssociationPageController::class . '::revisionPageTitle',
        '_entity_form' => $entity_type->id() . '.revision_revert',
      ]);

      $route->setRequirement('_entity_access', $entity_type->id() . '.revision_revert');
      $route->setRequirement($entity_type->id(), '\d+');
      $route->setRequirement('revision', '\d+');

      $route->setOption('_admin_route', TRUE);
      $route->setOption('parameters', [
        $entity_type->id() => [
          'type' => 'entity:' . $entity_type->id(),
        ],
        'revision' => [
          'type' => 'entity_revision:' . $entity_type->id(),
        ],
      ]);

      return $route;
    }

    return NULL;
  }

  /**
   * Create a route for deleting a revision of an entity.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   If a link template definition is available for the route than create a
   *   route object for it. Otherwise return NULL if no link template defined.
   */
  protected function getRevisionDeleteRoute(EntityTypeInterface $entity_type): ?Route {
    if ($entity_type->hasLinkTemplate('revision-delete')) {
      $route = new Route($entity_type->getLinkTemplate('revision-delete'));
      $entityTypeId = $entity_type->id();

      $route->setDefaults([
        '_title_callback' => AssociationPageController::class . '::revisionPageTitle',
        '_entity_form' => $entity_type->id() . '.revision_delete',
      ]);

      $route->setRequirement('_entity_access', $entityTypeId . '.revision_delete');
      $route->setRequirement($entity_type->id(), '\d+');
      $route->setRequirement('revision', '\d+');

      $route->setOption('_admin_route', TRUE);
      $route->setOption('parameters', [
        $entity_type->id() => [
          'type' => 'entity:' . $entityTypeId,
        ],
        'revision' => [
          'type' => 'entity_revision:' . $entityTypeId,
        ],
      ]);

      return $route;
    }

    return NULL;
  }

}
