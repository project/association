<?php

namespace Drupal\association_page\Entity\Storage;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\RevisionableStorageInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the storage handler class for association page entities.
 *
 * This extends the base storage class, adding required special handling for
 * revisioning of association landing page entities.
 *
 * @ingroup association
 */
class AssociationPageStorage extends SqlContentEntityStorage implements RevisionableStorageInterface {

  /**
   * Get available revision IDs for a content entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to find revisions for.
   * @param string|null $langcode
   *   If provided, only return revision IDs that have this translation. If not
   *   provided, load all revisions regardless of language.
   *
   * @return string[]
   *   An array of revision IDs for the matching entity and language code.
   */
  public function revisionIds(EntityInterface $entity, ?string $langcode = NULL): array {
    $idKey = $this->entityType->getKey('id') ?: 'id';
    $revisionKey = $this->entityType->getKey('revision') ?: 'vid';

    $query = $this->database->select($this->getRevisionTable(), 'base')
      ->fields('base', [$revisionKey])
      ->condition('base.' . $idKey, $entity->id())
      ->orderBy($revisionKey, 'DESC');

    // Add a filter to only list revisions with translations only.
    if (!empty($langcode) && $this->entityType->hasKey('langcode')) {
      $query->condition('base.' . $this->entityType->getKey('langcode'), $langcode);
    }

    return $query->execute()->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account): array {
    $userKey = $this->entityType->getKey('uid') ?: 'uid';
    $revisionKey = $this->entityType->getKey('revision') ?: 'vid';

    return $this->database->select($this->getRevisionDataTable(), 'base')
      ->fields('base', [$revisionKey])
      ->condition('base.' . $userKey, $account->id())
      ->orderBy('base.' . $revisionKey, 'DESC')
      ->execute()
      ->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(EntityInterface $entity) {
    $idKey = $this->entityType->getKey('id') ?: 'id';

    $query = $this->database->select($this->getRevisionDataTable(), 'base')
      ->condition('base.' . $idKey, $entity->id())
      ->condition('base.default_langcode', 1);

    $query->addExpression('COUNT(*)');
    return $query->execute()->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language): ?int {
    return $this->database->update($this->getRevisionTable())
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
