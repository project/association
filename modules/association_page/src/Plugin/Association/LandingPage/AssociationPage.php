<?php

namespace Drupal\association_page\Plugin\Association\LandingPage;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\association\Attribute\AssociationLandingPage;
use Drupal\association\Entity\AssociationInterface;
use Drupal\association\Entity\AssociationTypeInterface;
use Drupal\association\Plugin\AssociationPluginFormInterface;
use Drupal\association\Plugin\LandingPagePluginBase;
use Drupal\association\Plugin\LandingPagePluginInterface;
use Drupal\association\Plugin\RevisionablePagePluginInterface;
use Drupal\association_page\Entity\AssociationPage as EntityAssociationPage;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Association landing page plugin to support using a association page entity.
 *
 * The association_page module defines a separate entity type for generating a
 * landing page for the associations. This plugin makes this entity available
 * for use as the landing page plugin handler for association types.
 *
 * This it recommended to use this as the association landing page because the
 * entity is tightly managed with the association. This means that for all
 * associations, this page is reliably available and managed with the
 * association contents.
 */
#[AssociationLandingPage(
  id: 'association_page',
  label: new TranslatableMarkup('Dedicated association page'),
  description: new TranslatableMarkup('Landing page is provided by a Association page entity.'),
 )]
class AssociationPage extends LandingPagePluginBase implements LandingPagePluginInterface, AssociationPluginFormInterface, RevisionablePagePluginInterface, ContainerFactoryPluginInterface {

  /**
   * The association page entity storage handler.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $pageStorage;

  /**
   * Constructs an AssociationPage landing page plugin instance.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin identifier.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $association_page_storage
   *   The entity storage handler for association_page entity types.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityStorageInterface $association_page_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->pageStorage = $association_page_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('association_page')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'new_revision' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function shouldCreateNewRevision(): bool {
    return $this->configuration['new_revision'] ?? FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getPage(AssociationInterface $association): ?EntityInterface {
    return $this->pageStorage->load($association->id());
  }

  /**
   * {@inheritdoc}
   */
  public function getPageUrl(AssociationInterface $association, ?EntityInterface $page): Url {
    if ($page instanceof EntityAssociationPage) {
      return $page->toUrl('canonical');
    }

    return Url::fromRoute('<nolink>');
  }

  /**
   * {@inheritdoc}
   */
  public function onCreate(AssociationInterface $association): void {
    $page = $this->pageStorage->create([
      'id' => $association->id(),
      'title' => $association->label(),
      'type' => $association->bundle(),
    ]);

    $page->save();
  }

  /**
   * {@inheritdoc}
   */
  public function onPreDelete(array $associations): void {
    $ids = array_keys($associations);
    $pages = $this->pageStorage->loadMultiple($ids);

    if ($pages) {
      $this->pageStorage->delete($pages);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, ?AssociationTypeInterface $association_type = NULL): array {
    $form['new_revision'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create new page revisions by default'),
      '#default_value' => $this->configuration['new_revision'] ?? FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['new_revision'] = (bool) $form_state->getValue('new_revision');
  }

}
