<?php

namespace Drupal\association_page\Routing;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Alters Field UI and layout builder routes for association page entities.
 */
class AssociationPageRouteSubscriber extends RouteSubscriberBase {

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Constructs an AssociationRouteSubscriber object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    // Ensure that this happens alter event triggers after field_ui (-100)
    // and layout_builder (-110) adds their routes.
    $events[RoutingEvents::ALTER][] = ['onAlterRoutes', -200];

    return $events;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    if (!$this->moduleHandler->moduleExists('field_ui')) {
      return;
    }

    // Alter the field_ui and layout_builder routes related to association page
    // routes.
    //
    // Association page field_ui and layout_builder routes need additional
    // access checks, and renaming.
    $entityTypeId = 'association_page';
    $fieldUiRoutes = [
      // Field UI routes.
      "entity.field_config.{$entityTypeId}_field_edit_form",
      "entity.field_config.{$entityTypeId}_storage_edit_form",
      "entity.field_config.{$entityTypeId}_field_delete_form",
      "field_ui.field_storage_config_add_{$entityTypeId}",
      "entity.{$entityTypeId}.field_ui_fields",
      "entity.entity_form_display.{$entityTypeId}.default",
      "entity.entity_form_display.{$entityTypeId}.form_mode",
      "entity.entity_view_display.{$entityTypeId}.default",
      "entity.entity_view_display.{$entityTypeId}.view_mode",
      // Layout builder routes.
      "layout_builder.defaults.{$entityTypeId}.view",
      "layout_builder.defaults.{$entityTypeId}.discard_changes",
      "layout_builder.defaults.{$entityTypeId}.disable",
    ];

    foreach ($fieldUiRoutes as $routeName) {
      if (!($route = $collection->get($routeName))) {
        continue;
      }

      // Routes are only available for pages when the association type has the
      // pages option enabled. Add the access check to the route requirements.
      $route->addRequirements([
        '_association_page_config' => 'association_type',
      ]);

      // Rename these to avoid confusion. Tabs are renamed in the
      // association_local_tasks_alter() hook in association.module file.
      switch ($routeName) {
        case "entity.{$entityTypeId}.field_ui_fields":
          $route->setDefault('_title', 'Page fields');
          break;

        case "entity.entity_form_display.{$entityTypeId}.default":
        case "entity.entity_form_display.{$entityTypeId}.form_mode":
          $route->setDefault('_title', 'Page form display');
          break;

        case "entity.entity_view_display.{$entityTypeId}.default":
        case "entity.entity_view_display.{$entityTypeId}.view_mode":
          $route->setDefault('_title', 'Page display');
          break;
      }
    }
  }

}
