<?php

namespace Drupal\association\Access;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\association\Entity\AssociationInterface;
use Drupal\Core\Access\AccessResultInterface;
use Symfony\Component\Routing\Route;

/**
 * Determines access to entity creation route for Entity Association links.
 */
class AssociationLinkCreateAccess implements AccessInterface {

  /**
   * The key used by the routing requirement.
   *
   * @var string
   */
  protected string $requirementsKey = '_create_association_link';

  /**
   * {@inheritdoc}
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account, $tag, $entity_type, $bundle): AccessResultInterface {
    $paramName = $route->getRequirement($this->requirementsKey);
    $entity = $route_match->getParameter($paramName);

    if ($entity instanceof AssociationInterface) {
      try {
        $behavior = $entity->getBehavior();
        return $behavior->createAccess($entity, $tag, $entity_type, $bundle, $account);
      }
      catch (PluginException $e) {
        // Will return access forbidden if the behavior plugin is missing.
      }
    }

    return AccessResult::forbidden();
  }

}
