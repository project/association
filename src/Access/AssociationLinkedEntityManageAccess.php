<?php

namespace Drupal\association\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\association\AssociationNegotiatorInterface;
use Drupal\Core\Access\AccessResultInterface;
use Symfony\Component\Routing\Route;

/**
 * Determines if page configuration is enabled for association type admin.
 */
class AssociationLinkedEntityManageAccess implements AccessInterface {

  /**
   * The key used by the routing requirement.
   *
   * @var string
   */
  protected string $requirementsKey = '_association_linked_entity_manage_access';

  /**
   * The association negotiator which determines the active association.
   *
   * @var \Drupal\association\AssociationNegotiatorInterface
   */
  protected AssociationNegotiatorInterface $associationNegotiator;

  /**
   * Creates a new instance of the AssociationLinkedEntityManageAccess class.
   *
   * @param \Drupal\association\AssociationNegotiatorInterface $association_negotiator
   *   The association negotiator which determines the active association from
   *   the current route match.
   */
  public function __construct(AssociationNegotiatorInterface $association_negotiator) {
    $this->associationNegotiator = $association_negotiator;
  }

  /**
   * {@inheritdoc}
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account): AccessResultInterface {
    $paramName = $route->getRequirement($this->requirementsKey);

    if ($entity = $route_match->getParameter($paramName)) {
      if ($assoc = $this->associationNegotiator->byEntity($entity)) {
        /** @var \Drupal\Core\Access\AccessResult $access */
        $access = $assoc->access('manage', $account, TRUE);
        return $access->addCacheableDependency($entity);
      }
    }

    return AccessResult::forbidden();
  }

}
