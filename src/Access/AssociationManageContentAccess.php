<?php

namespace Drupal\association\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\association\Entity\AssociationInterface;
use Drupal\Core\Access\AccessResultInterface;
use Symfony\Component\Routing\Route;

/**
 * Determines if user has access to the manage association content overview.
 */
class AssociationManageContentAccess implements AccessInterface {

  /**
   * The key used by the routing requirement.
   *
   * @var string
   */
  protected string $requirementsKey = '_association_manage_content_access';

  /**
   * {@inheritdoc}
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account): AccessResultInterface {
    $paramName = $route->getRequirement($this->requirementsKey);
    $entity = $route_match->getParameter($paramName);

    $access = AccessResult::neutral();
    if ($entity instanceof AssociationInterface) {
      foreach (['manage', 'create', 'update'] as $op) {
        $access = $access->orIf($entity->access($op, $account, TRUE));

        if ($access->isAllowed()) {
          break;
        }
      }
    }

    return $access;
  }

}
