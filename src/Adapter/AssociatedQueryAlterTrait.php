<?php

namespace Drupal\association\Adapter;

use Drupal\Core\Database\Query\ConditionInterface;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\association\AssociationPermissions;

/**
 * Trait which assists with altering queries of associated entity types.
 *
 * Primarily designed to be applied to EntityAdapters which should apply query
 * constraints to associated entity type access queries.
 */
trait AssociatedQueryAlterTrait {

  /**
   * Cached allowed association types, keyed by user and operation.
   *
   * @var array
   */
  private array $cachedAllowedTypes = [];

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|null
   */
  protected ?EntityTypeManagerInterface $entityTypeManager = NULL;

  /**
   * Get the entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager.
   */
  protected function getEntityTypeManager(): EntityTypeManagerInterface {
    if (!isset($this->entityTypeManager)) {
      $this->entityTypeManager = \Drupal::entityTypeManager();
    }

    return $this->entityTypeManager;
  }

  /**
   * Get the entity type ID of the entity type this query alter works with.
   *
   * @return string
   *   Get the name of the entity type ID this query alter works with.
   */
  abstract protected function getEntityTypeId(): string;

  /**
   * Get association types which this user has permission to use this operation.
   *
   * @param string $op
   *   The entity operation taking place.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account to test association type access for.
   *
   * @return string[]|true
   *   Returns an array of association type IDs which this operation is allowed
   *   for the user. Will return an empty array if no user doesn't have
   *   permission for any association types, and boolean TRUE if all allowed.
   */
  protected function getAllowedTypesByOperation($op, AccountInterface $account): array|true {
    $opMap = [
      'view' => 'manage',
      'edit' => 'manage',
      'update' => 'manage',
      'delete' => 'delete_content',
    ];

    if ($assocOp = $opMap[$op] ?? NULL) {
      // Queries are run multiple times in a single request (i.e. count queries)
      // and often it will be for the same user and operations. Avoid
      // recalculating these account permissions everytime.
      //
      // @todo Is it worth looking at permission hash instead of user? Or is
      // the overhead of generating the hash everytime not worth it?
      if (!isset($this->cachedAllowedTypes[$account->id()][$op])) {
        /** @var \Drupal\association\Entity\AssociationTypeInterface[] $types */
        $types = $this
          ->getEntityTypeManager()
          ->getStorage('association_type')
          ->loadMultiple();

        $isAll = TRUE;
        $allowed = [];
        foreach ($types as $typeId => $typeDef) {
          $permKey = AssociationPermissions::getBundlePermissionKey($typeDef, $assocOp);

          if ($account->hasPermission($permKey)) {
            $allowed[$typeId] = $typeId;
          }
          else {
            $isAll = FALSE;
          }
        }

        $this->cachedAllowedTypes[$account->id()][$op] = $isAll ?: $allowed;
      }

      return $this->cachedAllowedTypes[$account->id()][$op];
    }

    // Not an entity operation association module tries to control.
    //
    // @todo Revisit? Should this instead be return '[]' and only allow
    // content which isn't associated?
    return TRUE;
  }

  /**
   * Ensure that the neccessary association tables are available.
   *
   * @param \Drupal\Core\Database\Query\SelectInterface $query
   *   The query to be altered.
   *
   * @return string[]
   *   The aliases for the tables which are the "base" table for the associated
   *   entity, the "association" data table and the "association_link" base
   *   table, which have been ensured to be included into the query.
   */
  protected function ensureTables(SelectInterface $query): array {
    $entityTypeManager = $this->getEntityTypeManager();

    // Transform the query table data into something easier to look
    // up by actual table name.
    $lookup = [];
    foreach ($query->getTables() as $alias => $info) {
      $lookup[$info['table']] = $alias;
    }

    $baseTable = $query->getMetaData('base_table');
    $entityType = $entityTypeManager->getDefinition($this->getEntityTypeId());

    if (empty($baseTable)) {
      // Prefer the revision table if it is available.
      if ($entityType->isRevisionable()) {
        $baseTable = $lookup[$entityType->getRevisionTable()] ?? NULL;
      }

      if (empty($baseTable)) {
        $baseTable = $lookup[$entityType->getDataTable()] ?? NULL;

        if (empty($baseTable)) {
          // Unable to continue without a valid base table.
          throw new \InvalidArgumentException('No base table available for entity type: ' . $this->getEntityTypeId());
        }
      }
    }

    // Ensure the association link table is available.
    $assocLinkTable = $entityTypeManager
      ->getDefinition('association_link')
      ->getBaseTable();

    if (empty($lookup[$assocLinkTable])) {
      $alias = 'association_link_' . $baseTable;
      $lookup[$assocLinkTable] = $query->leftJoin(
        $assocLinkTable,
        $alias,
        $baseTable . '.' . $entityType->getKey('id') . " = {$alias}.target AND {$alias}.entity_type = :base_entity_type_id",
        [':base_entity_type_id' => $this->getEntityTypeId()]
      );
    }

    // Finally add the association field data table if it hasn't already
    // been included.
    $assocType = $entityTypeManager->getDefinition('association');
    $assocTable = $assocType->getDataTable();

    if (empty($lookup[$assocTable])) {
      $alias = 'association_data_' . $baseTable;
      $lookup[$assocTable] = $query->leftJoin(
        $assocTable,
        $alias,
        $lookup[$assocLinkTable] . ".association = {$alias}." . $assocType->getKey('id')
      );
    }

    return [
      'base' => $baseTable,
      'association' => $lookup[$assocTable],
      'association_link' => $lookup[$assocLinkTable],
    ];
  }

  /**
   * Build conditions to apply to a query to enforce association access.
   *
   * @param \Drupal\Core\Database\Query\SelectInterface $query
   *   The query being altered.
   * @param array $aliases
   *   The entity table aliases as would be returned by static::ensureTable().
   * @param string $op
   *   The entity operation being performed.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account to check the operation access for.
   *
   * @return \Drupal\Core\Database\Query\ConditionInterface|null
   *   The access criteria to apply the query in order to apply entity
   *   association constraints based on access. NULL is return if no conditions
   *   should be applied.
   */
  protected function buildAccessCondition(SelectInterface $query, array $aliases, $op, AccountInterface $account): ?ConditionInterface {
    $alias = $aliases['association'];
    $assocKeys = $this
      ->getEntityTypeManager()
      ->getDefinition('association')
      ->getKeys();

    $cond = $query->OrConditionGroup();

    // Get the association types this user has access to for
    // the requested operation.
    $allowedTypes = $this->getAllowedTypesByOperation($op, $account);

    // All association types allowed, no additional filter needed.
    if ($allowedTypes === TRUE) {
      return NULL;
    }
    elseif ($allowedTypes) {
      // OR the entity belongs to an association type user has access to.
      if (count($allowedTypes) === 1) {
        $cond->condition("{$alias}.{$assocKeys['bundle']}", reset($allowedTypes));
      }
      else {
        $cond->condition("{$alias}.{$assocKeys['bundle']}", $allowedTypes, 'IN');
      }
    }

    // OR viewing content from an active association.
    if ($op === 'view') {
      $cond->condition("{$alias}.{$assocKeys['status']}", 1);
    }

    // Is not part of an entity association should be allowed as far as
    // the association access rules are concerned.
    $cond->isNull("{$alias}.{$assocKeys['id']}");
    return $cond;
  }

}
