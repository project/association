<?php

namespace Drupal\association\Adapter;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\association\Event\AssociationEvents;
use Drupal\association\Event\QueryAccessAlterEvent;
use Drupal\toolshed\Strategy\ContainerInjectionStrategyInterface;
use Drupal\toolshed\Strategy\Exception\StrategyException;
use Drupal\toolshed\Strategy\StrategyBase;
use Drupal\toolshed\Strategy\StrategyDefinitionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * The default EntityAdapter implementation.
 *
 * This adapter employs the common entity storage creation and form operation
 * fetching, and will work for most standard entity types.
 *
 * This adapter also has plugin definition settings for limiting allowed bundles
 * and some different handling of entity administrative permissions.
 *
 * Potential reasons to create custom adapters:
 *   - Entity requires additional values during creation
 *   - Entity form requires additional settings, or initialization
 *   - Entity has non-standard operations or special access logic
 *
 * Definitions can be overridden or altered by implementing the
 * hook_association_entity_adapter_info_alter() alter hook.
 */
class EntityAdapter extends StrategyBase implements EntityAdapterInterface, ContainerInjectionStrategyInterface {

  use AssociatedQueryAlterTrait;
  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|null
   */
  protected ?EntityTypeManagerInterface $entityTypeManager = NULL;

  /**
   * The entity type bundle info manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected EntityTypeBundleInfoInterface $entityBundleInfo;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected FormBuilderInterface $formBuilder;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * List of entity bundles supported for association link target entities.
   *
   * @var array<string,\Stringable|string>|null
   */
  protected ?array $bundles = NULL;

  /**
   * Create a new instance of EntityAdapter class.
   *
   * @param string $id
   *   The unique strategy ID.
   * @param \Drupal\toolshed\Strategy\StrategyDefinitionInterface $definition
   *   Plugin definitions from discovery.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info manager.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(string $id, StrategyDefinitionInterface $definition, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, FormBuilderInterface $form_builder, EventDispatcherInterface $event_dispatcher) {
    $this->id = $id;
    $this->definition = $definition;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityBundleInfo = $entity_type_bundle_info;
    $this->formBuilder = $form_builder;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, string $id, StrategyDefinitionInterface $definition): static {
    return new static(
      $id,
      $definition,
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('form_builder'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeId(): string {
    if (!empty($this->definition->entity_type)) {
      return $this->definition->entity_type;
    }

    $err = sprintf('Entity adapters are required to have an "entity_type" defined, but Adapter with ID: %s does not specify an entity type.', $this->id());
    throw new StrategyException($err);
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel(): \Stringable|string {
    $label = $this->definition->label ?? NULL;

    if ($label) {
      return $label;
    }

    $entityType = $this->entityTypeManager
      ->getDefinition($this->getEntityTypeId(), FALSE);

    return $entityType ? $entityType->getLabel() : $this->t('Unknown');
  }

  /**
   * {@inheritdoc}
   */
  public function getBundles(): array {
    if (!isset($this->bundles)) {
      $entityTypeId = $this->getEntityTypeId();
      $bundleInfo = $this->entityBundleInfo
        ->getBundleInfo($entityTypeId);

      $this->bundles = [];
      foreach ($bundleInfo as $name => $info) {
        $this->bundles[$name] = $info['label'];
      }
    }

    return $this->bundles;
  }

  /**
   * {@inheritdoc}
   */
  public function getOperations(ContentEntityInterface $entity): array {
    $entityTypeId = $this->getEntityTypeId();

    if ($entityTypeId !== $entity->getEntityTypeId()) {
      $msg = sprintf('Entity is of wrong type, expected: %s but got: %s', $entityTypeId, $entity->getEntityTypeId());
      throw new \InvalidArgumentException($msg);
    }

    return $this->entityTypeManager
      ->getListBuilder($entityTypeId)
      ->getOperations($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function checkAccess(ContentEntityInterface $entity, $op, AccountInterface $account): AccessResultInterface {
    if ($entity->getEntityTypeId() != $this->getEntityTypeId()) {
      throw new \InvalidArgumentException(sprintf('Entity adapter expected %s but got %s.', $this->getEntityTypeId(), $entity->getEntityTypeId()));
    }

    if ($entity->hasField('associations')) {
      /** @var \Drupal\association\Entity\AssociatedEntityInterface */
      $assocLink = $entity->get('associations')->entity;

      if ($assocLink) {
        $assoc = $assocLink->getAssociation();

        switch ($op) {
          case 'view':
            $viewAccess = $assoc->access('view', $account, TRUE);
            if ($viewAccess->isAllowed()) {
              // Access will be up to the entity's own visibility rules from
              // here. View access is only blocked here if the association is
              // inactive, but otherwise the entity is responsible for further
              // access or publish state checking.
              return AccessResult::neutral()->addCacheableDependency($viewAccess);
            }

            // Purposely fall-through and allow view if user has access to edit
            // or manage this content directly.
          case 'edit':
          case 'update':
            $access = $assoc->access('manage', $account, TRUE);
            break;

          case 'delete':
            $access = $assoc->access('delete_content', $account, TRUE);
            break;

          default:
            // Don't exert any control over any other operations.
            return AccessResult::neutral()->addCacheableDependency($assoc);
        }

        // If edit, update or delete were not explicitly granted, we want to
        // deny access to these operations. It is possible to alter access
        // grants at the association access layer.
        if (!$access->isAllowed()) {
          // Ensure that access is forbidden but make sure not to lose the
          // caching metadata accumulated up to this point.
          $access = AccessResult::forbidden()->addCacheableDependency($access);
        }

        /** @var \Drupal\Core\Access\AccessResult $access */
        return $access->addCacheableDependency($assoc);
      }
    }

    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  public function accessQueryAlter(SelectInterface $query, $op, AccountInterface $account): void {
    $aliases = $this->ensureTables($query);
    $accessCond = $this->buildAccessCondition($query, $aliases, $op, $account);

    // Give other modules a chance to alter the query or the access conditions.
    $event = new QueryAccessAlterEvent($op, $query, $accessCond, $aliases, $account);
    // Per discussion https://github.com/phpstan/phpstan-symfony/issue/59
    // @phpstan-ignore-next-line
    $this->eventDispatcher->dispatch($event, AssociationEvents::QUERY_ACCESS_ALTER);

    if ($accessCond) {
      // Top level conjunction is always "AND" so it can just be added to
      // the query as just another conditional. If this ends up being an "OR"
      // query, this assumption will need to be revisited.
      $query->condition($accessCond);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createEntity($bundle, $values = []): ContentEntityInterface {
    $entityTypeId = $this->getEntityTypeId();
    $entityType = $this->entityTypeManager
      ->getDefinition($entityTypeId);

    if ($bundleKey = $entityType->getKey('bundle')) {
      $bundle = $bundle instanceof EntityInterface ? $bundle->id() : $bundle;
      $values[$bundleKey] = $bundle;

      // Make sure entities created are only from allowed bundles.
      if (!isset($this->getBundles()[$bundle])) {
        $error = sprintf('Bundle %s is not allowed for %s', $bundle, $entityTypeId);
        throw new \InvalidArgumentException($error);
      }
    }

    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $this->entityTypeManager
      ->getStorage($entityTypeId)
      ->create($values);

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityForm(ContentEntityInterface $entity, $op, FormStateInterface &$form_state): array {
    $entityType = $entity->getEntityType();

    if ($entityType->id() != $this->getEntityTypeId()) {
      throw new \InvalidArgumentException('This entity type adapter only supports entities of type ' . $this->getEntityTypeId());
    }

    // Allow the operation value to fallback to a "default" form if needed,
    // with the exception of the "delete" operation which should not fallback.
    $operation = ($op == 'delete' || $entityType->getFormClass($op)) ? $op : 'default';

    // Create the form object of the appropriate type, and build the form.
    // Using the form builder instead of the entity form builder, so we are able
    // to manage the form state which is used for the form building.
    $formObj = $this->entityTypeManager->getFormObject($entityType->id(), $operation);
    $formObj->setEntity($entity);
    $formObj->setOperation($op);

    return $this->formBuilder->buildForm($formObj, $form_state);
  }

}
