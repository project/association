<?php

namespace Drupal\association\Adapter;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\toolshed\Strategy\StrategyInterface;

/**
 * Adapter interface for supporting entity types to link tp Entity Associations.
 *
 * Adapters allow the developers to add entity types to be link to Entity
 * Associations. The adapter is responsible for:
 *
 *   - Creating new entity instances from bundle information
 *   - Creating entity forms for CRUD entity operations (create, update, delete)
 *   - Enumerate a list of bundles allowed for use with associations
 *   - Alter entity permissions to be aware of association permissions
 *   - Alter entity queries base on permissions
 *
 * EntityAdapter plugins are managed by the
 * "strategy.manager.association.entity_adapter" service. This service is a
 * plugin manager that finds definitions in "*.association.entity_adapter.yml".
 * These YAML files define what entity types are compatible with association
 * linking and what adapter classes are responsible for implementing the needed
 * functionality.
 *
 * Most entity types can be supported with the default EntityAdapter.
 *
 * @see \Drupal\association\EntityAdapterManagerInterface
 * @see \Drupal\association\EntityAdapterManager
 * @see \Drupal\association\Adapter\EntityAdapter
 */
interface EntityAdapterInterface extends StrategyInterface {

  /**
   * Get the machine ID of the entity type supported with this adapter instance.
   *
   * @return string
   *   The entity type ID of the type the adapter instance is for.
   */
  public function getEntityTypeId(): string;

  /**
   * Get the entity type display label.
   *
   * @return \Stringable|string
   *   Entity type label.
   */
  public function getLabel(): \Stringable|string;

  /**
   * Get a list of entity bundles available to be used with associations.
   *
   * Entity adapters are able to limit or allow association type behaviors from
   * using bundles of this entity, by choosing which bundles are returned here.
   *
   * @return \Stringable[]|string[]
   *   List of bundles of this entity type, this adapter allows to be created
   *   and linked to associations. The array key is the bundle machine ID for
   *   the bundle, and the value is the bundle label.
   */
  public function getBundles(): array;

  /**
   * Get entity operation links for the provided entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity instance to get the edit operations for.
   *
   * @return array
   *   An associative array of operation link data for this list, keyed by
   *   operation name, containing the following key-value pairs:
   *
   *   - title: The localized title of the operation.
   *   - url: An instance of \Drupal\Core\Url for the operation URL
   *   - weight: The weight of this operation.
   *
   * @see \Drupal\Core\Entity\EntityListBuilderInterface::getOperations()
   */
  public function getOperations(ContentEntityInterface $entity): array;

  /**
   * Check if user has access to an operation from association restrictions.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity being checked for access of an operation.
   * @param string $op
   *   The operation to be performed on the entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Account information of the user trying to perform the operation.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   A result of the access check.
   */
  public function checkAccess(ContentEntityInterface $entity, $op, AccountInterface $account): AccessResultInterface;

  /**
   * Alters database query to apply entity association access to content.
   *
   * @param \Drupal\Core\Database\Query\SelectInterface $query
   *   The query to apply the access checks for.
   * @param string $op
   *   The entity operation being performed.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account to apply the access permissions for.
   */
  public function accessQueryAlter(SelectInterface $query, $op, AccountInterface $account): void;

  /**
   * Creates and instance of the entity appropriate for this adapter and bundle.
   *
   * @param string $bundle
   *   The bundle to create the new entity instance of.
   * @param array $values
   *   Additional values to populate for the newly created entity.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   A newly created entity if possible. NULL is returned if creation fails.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \InvalidArgumentException
   */
  public function createEntity($bundle, array $values = []): ContentEntityInterface;

  /**
   * Build the entity operation form for the requested operation.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity the form is operating on.
   * @param string $op
   *   The entity operation to build the form for.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Optional form state, if one is already created. If no form state is
   *   provided as a parameter, a new form state will be created.
   *
   * @return array
   *   Form elements for the entity form build for the requested operation.
   */
  public function getEntityForm(ContentEntityInterface $entity, $op, FormStateInterface &$form_state): array;

}
