<?php

namespace Drupal\association\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Annotation for association_type (association bundle) behavior plugins.
 *
 * Plugin Namespace: Plugin\Association\Behavior.
 *
 * @see \Drupal\Component\Annotation\Plugin
 * @see \Drupal\association\Plugin\BehaviorInterface
 * @see \Drupal\association\Plugin\BehaviorPluginManagerInterface
 *
 * @ingroup association_behavior_plugins
 *
 * @Annotation
 */
class AssociationBehavior extends Plugin {

  /**
   * The ID of the plugin.
   *
   * @var string
   */
  public string $id;

  /**
   * The human friendly name for admin configuration forms.
   *
   * @var \Stringable|string
   *
   * @ingroup plugin_translatable
   */
  public \Stringable|string $label;

  /**
   * Class which builds the content management UI for association behavior.
   *
   * It's possible for the behavior plugin to implement this directly and so
   * this parameter is not provided if the builder is the plugin itself.
   *
   * @var string|null
   */
  public ?string $manager_builder;

  /**
   * List for form classes available for the behaviors.
   *
   * @var array[]
   */
  public array $forms;

}
