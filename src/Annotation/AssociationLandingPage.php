<?php

namespace Drupal\association\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Annotation for association_type (association bundle) landing plugins.
 *
 * Handles the landing page resolution and configurations for an association.
 *
 * Plugin Namespace: Plugin\Association\LandingPage.
 *
 * @see \Drupal\Component\Annotation\Plugin
 * @see \Drupal\association\Plugin\LandingPageInterface
 * @see \Drupal\association\Plugin\LandingPagePluginManager
 *
 * @ingroup association_landing_page_plugins
 *
 * @Annotation
 */
class AssociationLandingPage extends Plugin {

  /**
   * The unique plugin identifier.
   *
   * @var string
   */
  public string $id;

  /**
   * The human friendly name for admin label for admin form selection.
   *
   * @var \Stringable|string
   *
   * @ingroup plugin_translatable
   */
  public \Stringable|string $label;

  /**
   * The description of the landing page plugin.
   *
   * Provide a description for the landing page plugin and helpful tips for
   * aiding with the plugin selection.
   *
   * @var \Stringable|string|null
   *
   * @ingroup plugin_translatable
   */
  public \Stringable|string|null $description;

  /**
   * The plugin form class to build the configuration form elements.
   *
   * The plugin form class to use for the landing page plugin configuration
   * form elements. The class name should be fully namespaced and implement
   * the \Drupal\Component\Plugin\PluginFormInterface interface.
   *
   * @var class-string|null
   */
  public ?string $form_handler;

}
