<?php

namespace Drupal\association;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\association\Entity\AssociationInterface;

/**
 * Interface for services which resolve the active association from a context.
 */
interface AssociationNegotiatorInterface {

  /**
   * Determine which association is active for this route.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface|null $route_match
   *   The route data to fetch the active Entity Association from.
   *
   * @return \Drupal\association\Entity\AssociationInterface|null
   *   An Entity Association, if one could be found otherwise return NULL.
   */
  public function byRoute(?RouteMatchInterface $route_match = NULL): ?AssociationInterface;

  /**
   * Determine which Entity Association an entity belongs to.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to determine the Entity Association from.
   *
   * @return \Drupal\association\Entity\AssociationInterface|null
   *   An Entity Association, if one could be found that applies to this entity,
   *   otherwise return NULL.
   */
  public function byEntity(EntityInterface $entity): ?AssociationInterface;

}
