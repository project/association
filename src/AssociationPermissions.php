<?php

namespace Drupal\association;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\association\Entity\AssociationTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Utility for building available bundle permissions for association entities.
 */
class AssociationPermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity storage handler for association type entities.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface|null
   */
  protected ?EntityStorageInterface $assocTypeStorage;

  /**
   * Create a new instance of the AssociationPermissions utility class.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    try {
      $this->assocTypeStorage = $entity_type_manager->getStorage('association_type');
    }
    catch (PluginNotFoundException $e) {
      $this->assocTypeStorage = NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Fetch association entity operations available for an association type.
   *
   * @param \Drupal\association\Entity\AssociationTypeInterface $type
   *   The association_type bundle entity.
   *
   * @return array<\Stringable|string>
   *   An array of operation labels, keyed by the string identifier for the
   *   operation. For the case of association entities, these are the access
   *   permission identifiers, and for association landing page entities, the
   *   operations are prefixed with "page.".
   */
  public static function getBundleOperations(AssociationTypeInterface $type): array {
    $ops = [
      'publish' => t('Publish'),
      'create' => t('Create'),
      'update' => t('Edit'),
      'delete' => t('Delete'),
      'manage' => t('Manage content from'),
      'create_content' => t('Add content to'),
      'delete_content' => t('Remove content from'),
    ];

    return $ops;
  }

  /**
   * Get the permission key to use for an operation for an association type.
   *
   * @param \Drupal\association\Entity\AssociationTypeInterface|string $type
   *   The association bundle entity.
   * @param string $op
   *   String name of the operation to get the permission for.
   *
   * @return string
   *   The permission key for an operation on association type (bundle).
   */
  public static function getBundlePermissionKey($type, $op): string {
    if ($type instanceof AssociationTypeInterface) {
      $type_id = $type->id();
      $operations = static::getBundleOperations($type);

      if (!isset($operations[$op])) {
        throw new \InvalidArgumentException('Cannot build Entity Association permission with invalid operation: ' . $op);
      }
    }
    else {
      $type_id = $type;
    }

    return "{$op} association of type {$type_id}";
  }

  /**
   * Get a list of available permissions for association and page entities.
   *
   * @return array
   *   An array of permission definition arrays suitable to use with a
   *   hook.permissions.yml file.
   */
  public function getPermissions(): array {
    $permissions = [];

    $weight = 0;
    /** @var \Drupal\association\Entity\AssociationTypeInterface $assocType */
    foreach ($this->assocTypeStorage?->loadMultiple() ?? [] as $assocType) {
      // Add each of the generated bundle permissions, and add a weight to
      // ensure they are displayed in the desired order. Otherwise Drupal
      // tried to alpha sort them, which ruins the logical ordering.
      foreach ($this->getBundlePermissions($assocType) as $permId => $permInfo) {
        $permissions[$permId] = $permInfo + [
          'weight' => $weight++,
        ];
      }
    }

    return $permissions;
  }

  /**
   * Get permissions that are specific to this association type (bundle).
   *
   * @param \Drupal\association\Entity\AssociationTypeInterface $type
   *   The association type to build the permission(s) for.
   * @param string|null $op
   *   A specific operation to be performed or NULL to return all the
   *   available operations available for this association type.
   *
   * @return array
   *   An array of permissions for the association type and the requested
   *   operation. If $op is null, then get all available operations. If an
   *   $op is not empty and not a valid operation, an empty array is returned.
   */
  public function getBundlePermissions(AssociationTypeInterface $type, $op = NULL): array {
    $operations = static::getBundleOperations($type);

    if (!empty($op)) {
      $operations = isset($operations[$op]) ? [$op => $operations[$op]] : [];
    }

    $permissions = [];
    foreach ($operations as $operation => $label) {
      $permissions[static::getBundlePermissionKey($type, $operation)] = [
        'title' => $this->t('@operation Entity Associations of type: <strong>@label</strong>.', [
          '@label' => $type->label(),
          '@operation' => $label,
        ]),
      ];
    }

    return $permissions;
  }

}
