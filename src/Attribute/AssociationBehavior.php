<?php

namespace Drupal\association\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;

/**
 * Plugin attribute for association behavior plugins.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class AssociationBehavior extends Plugin {

  /**
   * Creates a new instance of the AssociationBehavior attribute.
   *
   * @param string $id
   *   The plugin machine name identifier.
   * @param \Stringable|string $label
   *   The plugin label.
   * @param string|null $manager_builder
   *   Class which builds the content management UI for association behavior.
   *   If not provided, the plugin behavior should implement
   *   \Drupal\assoication\Behavior\ManagerBuilderInterface and used as the
   *   content UI builder,.
   * @param class-string|null $deriver
   *   The plugin deriver class.
   * @param string[] $forms
   *   List of form classes for behavior administration forms.
   */
  public function __construct(
    public readonly string $id,
    public readonly \Stringable|string $label,
    public readonly ?string $manager_builder = NULL,
    public readonly ?string $deriver = NULL,
    public readonly array $forms = [],
  ) {}

}
