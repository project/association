<?php

namespace Drupal\association\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;

/**
 * Plugin attribute for association landing page handler plugins.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class AssociationLandingPage extends Plugin {

  /**
   * Creates a new instance of the AssociationBehavior attribute.
   *
   * @param string $id
   *   The plugin machine name identifier.
   * @param \Stringable|string $label
   *   The plugin label.
   * @param \Stringable|string|null $description
   *   Provide a description for the landing page plugin and helpful tips for
   *   aiding with the plugin selection.
   * @param class-string|null $deriver
   *   The plugin deriver fully namespaced class.
   * @param class-string|null $form_handler
   *   The plugin form class to build the configuration form elements.
   */
  public function __construct(
    public readonly string $id,
    public readonly \Stringable|string $label,
    public readonly \Stringable|string|null $description = NULL,
    public readonly ?string $deriver = NULL,
    public readonly ?string $form_handler = NULL,
  ) {}

}
