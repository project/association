<?php

namespace Drupal\association\Behavior\Manager;

use Drupal\Core\Url;
use Drupal\association\Behavior\ManagerBuilderBase;

/**
 * Build the management controls for a entity list behavior plugin.
 */
class EntityListBuilder extends ManagerBuilderBase {

  /**
   * {@inheritdoc}
   */
  public function buildManagerUi(): array {
    $behavior = $this->association->getBehavior();
    $entityTypes = $behavior->getConfiguration()['entity_types'] ?? [];
    $elements = [];

    // Only generate and display the create content links if user has access.
    if ($this->association->access('create_content')) {
      $createLinks = [];
      foreach ($entityTypes as $entityTypeId => $bundles) {
        if (!$this->adapterManager->isAssociableType($entityTypeId)) {
          continue;
        }

        $adapter = $this->adapterManager->getAdapterByEntityType($entityTypeId);
        $entityTypeLabel = $adapter->getLabel();

        foreach (array_intersect_key($adapter->getBundles(), $bundles) as $bundle => $bundleLabel) {
          $createLinks["$entityTypeId:$bundle"] = [
            'title' => $this->t('Add @label - @bundle', [
              '@label' => $entityTypeLabel,
              '@bundle' => $bundleLabel,
            ]),
            'url' => Url::fromRoute('entity.association_link.add_content', [
              'association' => $this->association->id(),
              'tag' => 'entity',
              'entity_type' => $entityTypeId,
              'bundle' => $bundle,
            ]),
          ];
        }
      }

      // Create the links to generate content.
      $elements['create_links'] = [
        '#type' => 'dropbutton',
        '#links' => $createLinks,
      ];
    }

    $elements['table'] = [
      '#type' => 'table',
      '#header' => [
        'title' => $this->t('Title'),
        'type' => $this->t('Type'),
        'status' => $this->t('Status'),
        'op' => $this->t('Operations'),
      ],
    ];

    // Load and organize all existing links belonging to this association.
    foreach ($this->getLinkedEntities() as $entity) {
      if ($target = $entity->getTarget()) {
        $elements['table'][$entity->id()] = [
          'label' => $this->getEntityLink($entity),
          'type' => [
            '#markup' => $behavior->getTagLabel($entity->getTag(), $target->getEntityTypeId(), $target->bundle()),
          ],
          'status' => [
            '#markup' => $entity->getStatus() ? $this->t('Published') : $this->t('Unpublished'),
          ],
          'op' => [
            '#type' => 'operations',
            '#links' => $this->getEntityOperations($entity),
          ],
        ];
      }
    }

    return $elements;
  }

}
