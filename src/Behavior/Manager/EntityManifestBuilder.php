<?php

namespace Drupal\association\Behavior\Manager;

use Drupal\Core\Url;
use Drupal\association\Behavior\ManagerBuilderBase;
use Drupal\association\Plugin\BehaviorInterface;

/**
 * Build the content management controls for an Entity Manifest behavior plugin.
 */
class EntityManifestBuilder extends ManagerBuilderBase {

  /**
   * {@inheritdoc}
   */
  public function buildManagerUi(): array {
    $plugin = $this->association->getBehavior();
    $manifest = $plugin->getConfiguration()['manifest'] ?? [];
    $canCreate = $this->association->access('create_content');

    $byTags = [];
    foreach ($this->getLinkedEntities() as $entity) {
      $byTags[$entity->getTag()][$entity->id()] = $entity;
    }

    $elements = [];

    // Iterate through the manifest types and create a section for each.
    foreach ($manifest as $tag => $def) {
      $elements[$tag] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['manifest-section'],
        ],

        'title' => [
          '#type' => 'html_tag',
          '#tag' => 'h3',
          '#attributes' => [
            'class' => ['manifest-section__title'],
          ],
          '#value' => $def['label'],
        ],
      ];

      if (!empty($byTags[$tag])) {
        $table = [
          '#type' => 'table',
          '#header' => [
            'title' => $this->t('Title'),
            'status' => $this->t('Status'),
            'op' => $this->t('Operations'),
          ],
        ];

        foreach ($byTags[$tag] as $entityId => $entity) {
          $table[$entityId] = [
            'title' => $this->getEntityLink($entity),
            'status' => [
              '#markup' => $entity->getStatus() ? $this->t('Published') : $this->t('Unpublished'),
            ],
            'op' => [
              '#type' => 'operations',
              '#links' => $this->getEntityOperations($entity),
            ],
          ];
        }

        $elements[$tag]['table'] = $table;
      }

      // User has permission to create content, and the limit for # of items
      // has not been reached.
      if ($canCreate && ($def['limit'] == BehaviorInterface::CARDINALITY_UNLIMITED || empty($byTags[$tag]) || count($byTags[$tag]) < $def['limit'])) {
        $createLinks = [];

        foreach ($def['entity_types'] as $entityTypeId => $bundles) {
          $adapter = $this->adapterManager->getAdapterByEntityType($entityTypeId);

          foreach (array_intersect_key($adapter->getBundles(), $bundles) as $bundle => $bundleLabel) {
            $createLinks["$entityTypeId:$bundle"] = [
              'title' => $this->t('Add @label - @bundle', [
                '@label' => $adapter->getLabel(),
                '@bundle' => $bundleLabel,
              ]),
              'url' => Url::fromRoute('entity.association_link.add_content', [
                'association' => $this->association->id(),
                'tag' => $tag,
                'entity_type' => $entityTypeId,
                'bundle' => $bundle,
              ]),
            ];
          }
        }

        $elements[$tag]['add_more'] = [
          '#type' => 'dropbutton',
          '#links' => $createLinks,
        ];
      }
    }

    return $elements;
  }

}
