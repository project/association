<?php

namespace Drupal\association\Behavior;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\PluginAwareInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Exception\UndefinedLinkTemplateException;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\association\Entity\AssociationInterface;
use Drupal\association\Entity\AssociationLink;
use Drupal\association\EntityAdapterManagerInterface;
use Drupal\association\Plugin\BehaviorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The base class to use for behavior UI management page builder.
 *
 * This class builds the UI page for the association content administrative
 * page and forms to manage linking of associated content.
 */
abstract class ManagerBuilderBase implements ManagerBuilderInterface, PluginAwareInterface, ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity adapter plugin manager.
   *
   * @var \Drupal\association\EntityAdapterManagerInterface
   */
  protected EntityAdapterManagerInterface $adapterManager;

  /**
   * The association entity to build the management pages with.
   *
   * @var \Drupal\association\Entity\AssociationInterface|null
   */
  protected ?AssociationInterface $association = NULL;

  /**
   * The association type behavior plugin.
   *
   * @var \Drupal\association\Plugin\BehaviorInterface|null
   */
  protected ?BehaviorInterface $plugin = NULL;

  /**
   * Base constructor for association type behavior content manager builders.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\association\EntityAdapterManagerInterface $entity_adapter_manager
   *   The entity adapter plugin manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityAdapterManagerInterface $entity_adapter_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->adapterManager = $entity_adapter_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('strategy.manager.association.entity_adapter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setAssociation(AssociationInterface $association): self {
    $this->association = $association;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setPlugin(PluginInspectionInterface $plugin): void {
    if (!$plugin instanceof BehaviorInterface) {
      throw new \InvalidArgumentException('Plugin is not a valid association type behavior plugin.');
    }

    $this->plugin = $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge(): int {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(): array {
    return [
      'association:' . $this->association->id(),
      'association:links:' . $this->association->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts(): array {
    return [
      'route.association',
    ];
  }

  /**
   * Fetches all content linked to the assocation.
   *
   * @return \Drupal\association\Entity\AssociationLink[]
   *   Loads link entities for the association entity this builder is set to.
   */
  protected function getLinkedEntities(): array {
    /** @var \Drupal\association\Entity\Storage\AssociationLinkStorageInterface */
    $linkStorage = $this->entityTypeManager->getStorage('association_link');
    return $linkStorage->loadByAssociation($this->association);
  }

  /**
   * Get a renderable of the canonical link for the linked target entity.
   *
   * @param \Drupal\association\Entity\AssociationLink $association_link
   *   The loaded association link entities.
   *
   * @return array
   *   Renderable array representing a link to the target entity.
   */
  public function getEntityLink(AssociationLink $association_link): array {
    try {
      $target = $association_link->getTarget();
      return [
        '#type' => 'link',
        '#title' => $target->label(),
        '#url' => $target->toUrl('canonical'),
        '#attributes' => [
          'class' => [
            'association__action',
            'association__action--view-content',
          ],
        ],
      ];
    }
    catch (UndefinedLinkTemplateException $e) {
      // Unable to generate a canonical link for this entity, bail and return
      // empty array to indicate no link is available.
    }

    return [];
  }

  /**
   * An array of link definitions for entity operations.
   *
   * @param \Drupal\association\Entity\AssociationLink $association_link
   *   The loaded association link entities.
   *
   * @return array
   *   An array of link items, compatible for use with theme link or dropbutton
   *   #links property.
   *
   * @see \Drupal\Core\Render\Element\Dropbutton
   * @see links.html.twig
   */
  public function getEntityOperations(AssociationLink $association_link): array {
    $operations = [];

    try {
      /** @var \Drupal\Core\Entity\ContentEntityInterface $target */
      $target = $association_link->getTarget();
      $operations = $this->adapterManager
        ->getAdapter($target)
        ->getOperations($target);

      unset($operations['delete_multiple']);

      // Always prefer the association wrapped delete form, as it gives more
      // control over how the content is deleted. Remove from association or
      // completely delete content from the site?
      if (!empty($operations['delete'])) {
        $operations['delete'] = [
          'title' => $this->t('Remove'),
          'url' => $association_link->toUrl('delete-content'),
        ];
      }
    }
    catch (\InvalidArgumentException | InvalidPluginDefinitionException | EntityMalformedException) {
      // Unable to generate links for this entity bail.
    }

    return $operations;
  }

}
