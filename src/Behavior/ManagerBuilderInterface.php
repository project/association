<?php

namespace Drupal\association\Behavior;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\association\Entity\AssociationInterface;

/**
 * Interface for the controls builder of the association type behaviors.
 *
 * Behaviors plugins can implement this interface directly or they can defer
 * the functionality to a separate class that implements this for them.
 */
interface ManagerBuilderInterface extends CacheableDependencyInterface {

  /**
   * Set the association that this builder will build management pages for.
   *
   * This association needs to be set before calling buildeManageUI() or the
   * cacheable dependency methods.
   *
   * @param \Drupal\association\Entity\AssociationInterface $assoc
   *   The association entity to build the content management UI for.
   *
   * @return self
   *   The manager builder instance for method chaining.
   */
  public function setAssociation(AssociationInterface $assoc): self;

  /**
   * Build an interface for managing a linking of association content.
   *
   * @return array
   *   Renderable array of UI pages or forms for managing linked entity
   *   content of the association being managed.
   */
  public function buildManagerUi(): array;

}
