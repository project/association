<?php

namespace Drupal\association\Cache\Context;

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Create a cache context for the active association type from a route.
 *
 * Cache context ID: 'route.association_type'
 */
class AssociationTypeRouteCacheContext extends AssociationRouteCacheContext {

  /**
   * {@inheritdoc}
   */
  public static function getLabel(): \Stringable|string {
    return new TranslatableMarkup('Association by route');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext(): string {
    $assoc = parent::getAssociation();
    return (string) ($assoc ? $assoc->getType()->id() : 0);
  }

}
