<?php

namespace Drupal\association\Controller;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\Error;
use Drupal\association\AssociationNegotiatorInterface;
use Drupal\association\Entity\AssociationInterface;
use Drupal\association\Entity\AssociationLink;
use Drupal\association\EntityAdapterManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Route controller building association_link entity forms and pages.
 */
class AssociationLinkController implements ContainerInjectionInterface {

  use StringTranslationTrait;
  use LoggerChannelTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The association negotiator for determining the active association.
   *
   * @var \Drupal\association\AssociationNegotiatorInterface
   */
  protected AssociationNegotiatorInterface $associationNegotiator;

  /**
   * The entity type adapter plugin manager.
   *
   * @var \Drupal\association\EntityAdapterManagerInterface
   */
  protected EntityAdapterManagerInterface $adapterManager;

  /**
   * Create a new instance of the AssociationController class.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\association\AssociationNegotiatorInterface $association_negotiator
   *   The association negotiator for determining the active association.
   * @param \Drupal\association\EntityAdapterManagerInterface $adapter_manager
   *   The entity type adapter manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AssociationNegotiatorInterface $association_negotiator, EntityAdapterManagerInterface $adapter_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->associationNegotiator = $association_negotiator;
    $this->adapterManager = $adapter_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static(
      $container->get('entity_type.manager'),
      $container->get('association.negotiator'),
      $container->get('strategy.manager.association.entity_adapter')
    );

    return $instance;
  }

  /**
   * Callback to generate the page title of the association link create form.
   *
   * @param \Drupal\association\Entity\AssociationInterface $association
   *   The association entity the content is being built for.
   * @param string $tag
   *   The tag to identify what type of association link to build.
   *
   * @return \Stringable|string
   *   A string or translation to use as the page title.
   */
  public function getAddTitle(AssociationInterface $association, string $tag): \Stringable|string {
    return $this->t('Creating content for @label', [
      '@label' => $association->label(),
    ]);
  }

  /**
   * Build the create association_link entity form.
   *
   * @param \Drupal\association\Entity\AssociationInterface $association
   *   The association entity whose content is being managed.
   * @param string $tag
   *   The tag to identify what type of association link to build.
   * @param string $entity_type
   *   The machine name of the entity type to create.
   * @param string $bundle
   *   The machine name of the bundle of entity to create.
   *
   * @return \Symfony\Component\HttpFoundation\Response|array
   *   The association_link entity form to create a new associated entity.
   */
  public function createLinkedContentForm(AssociationInterface $association, string $tag, string $entity_type, string $bundle): Response|array {
    if ($behavior = $association->getBehavior()) {
      try {
        $entity = $behavior->createEntity($association, $tag, $entity_type, $bundle);
        $adapter = $this->adapterManager->getAdapter($entity);

        // Create an association link entity to link the yet to be created
        // entity and the association for background tasks, path aliasing
        // and form alters to be aware that this entity is associated.
        //
        // This association link only gets saved if the entity is successfully
        // saved.
        $entity->get('associations')->setValue([]);
        $assocLink = $association->associateEntity($tag, $entity, FALSE);

        // Prepare a new form state, and ensure that redirect has been disabled.
        // We do not want the entity's default redirects to move us away from
        // the owning association entity management pages.
        $formState = new FormState();
        $formState->disableRedirect();
        $form = $adapter->getEntityForm($entity, 'add', $formState);

        // Was form submitted, and was the new entity appropriately saved?
        if ($formState->isExecuted() && !$formState->isRebuilding()) {
          /** @var \Drupal\Core\Entity\EntityFormInterface */
          $formObj = $formState->getFormObject();
          /** @var \Drupal\Core\Entity\ContentEntityInterface */
          $entity = $formObj->getEntity();

          // @todo Sees that an ID was assigned to this entity. This might not
          // be the best way to determine if the entity was successfully
          // created. Research other possible entity added indicators?
          if ($entity->id()) {
            // Update the link entity with the new entity's ID before saving.
            $assocLink->target->setValue([
              'target_id' => $entity->id(),
              'entity' => $entity,
            ]);
            $behavior->alterLink($assocLink);
            $assocLink->save();

            $entity->get('associations')->setValue([
              'target_id' => $assocLink->id(),
              'entity' => $assocLink,
            ]);
          }

          // Redirect back to the association management form.
          $returnUrl = $association->toUrl('manage')->toString();
          return new RedirectResponse($returnUrl);
        }

        return $form;
      }
      catch (ServiceNotFoundException | PluginException | \TypeError $e) {
        // Missing entity type or unable to initialize adapter. Can't continue.
      }
      catch (\InvalidArgumentException $e) {
        Error::logException($this->getLogger('association'), $e);
      }
    }

    throw new NotFoundHttpException();
  }

  /**
   * Callback to generate the page title of the association link edit form.
   *
   * @param \Drupal\association\Entity\AssociationLink $association_link
   *   The association link entity the page content is being built for.
   *
   * @return \Stringable|string
   *   A string or translation to use as the page title.
   */
  public function getEditTitle(AssociationLink $association_link): \Stringable|string {
    return $this->t('Edit @label', [
      '@label' => $association_link->getTarget()->label(),
    ]);
  }

  /**
   * Build the edit association link entity form.
   *
   * @param \Drupal\association\Entity\AssociationLink $association_link
   *   The association_link that links the association to the target entity.
   *
   * @return \Symfony\Component\HttpFoundation\Response|array
   *   The association_link form to edit the associated entity content.
   */
  public function editLinkedContentForm(AssociationLink $association_link): Response|array {
    try {
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      $entity = $association_link->getTarget();
      $adapter = $this->adapterManager->getAdapter($entity);

      // Prepare a new form state, and ensure that redirect has been disabled.
      // We do not want the entity's default redirects to move us away from
      // the owning association management pages.
      $formState = new FormState();
      $formState->disableRedirect();
      $form = $adapter->getEntityForm($entity, 'edit', $formState);

      // Was form submitted, and was the new entity appropriately saved?
      if ($formState->isExecuted() && !$formState->isRebuilding()) {
        // Redirect back to the association management form.
        $association = $association_link->getAssociation();
        $returnUrl = $association->toUrl('manage')->toString();
        return new RedirectResponse($returnUrl);
      }

      return $form;
    }
    catch (ServiceNotFoundException | PluginException $e) {
      // Missing entity type. Can't continue.
    }

    throw new NotFoundHttpException();
  }

  /**
   * Callback to generate the page title of the association entity delete form.
   *
   * @param \Drupal\association\Entity\AssociationLink $association_link
   *   The association entity to delete.
   *
   * @return \Stringable|string
   *   A string or translation to use as the page title.
   */
  public function getDeleteTitle(AssociationLink $association_link): \Stringable|string {
    return $this->t('Delete @label', [
      '@label' => $association_link->getTarget()->label(),
    ]);
  }

  /**
   * Build the delete associated entity content form.
   *
   * @param \Drupal\association\Entity\AssociationLink $association_link
   *   The association link that is being deleted.
   *
   * @return \Symfony\Component\HttpFoundation\Response|array
   *   The association_link form to delete the link between the association
   *   and the content entity.
   */
  public function deleteLinkedContentForm(AssociationLink $association_link): Response|array {
    try {
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      $entity = $association_link->getTarget();
      $adapter = $this->adapterManager->getAdapter($entity);

      // Prepare a new form state, and ensure that redirect has been disabled.
      // We do not want the entity's default redirects to move us away from
      // the owning association entity management pages.
      $formState = new FormState();
      $formState->disableRedirect();

      if ($form = $adapter->getEntityForm($entity, 'delete', $formState)) {
        // Was form submitted, and was the new entity appropriately deleted?
        if ($formState->isExecuted() && !$formState->isRebuilding()) {
          // Redirect back to the association entity management UI.
          $association = $association_link->getAssociation();
          $returnUrl = $association->toUrl('manage')->toString();
          return new RedirectResponse($returnUrl);
        }

        return $form;
      }
    }
    catch (ServiceNotFoundException | PluginException $e) {
      // Missing entity type. Can't continue.
    }

    throw new NotFoundHttpException();
  }

}
