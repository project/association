<?php

namespace Drupal\association\Controller;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\PluginAwareInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\association\AssociationNegotiatorInterface;
use Drupal\association\Behavior\ManagerBuilderInterface;
use Drupal\association\Entity\AssociationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Route controller for building entity management UI pages.
 */
class AssociationManagementController implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The association negotiator which determines the active association.
   *
   * @var \Drupal\association\AssociationNegotiatorInterface
   */
  protected AssociationNegotiatorInterface $associationNegotiator;

  /**
   * Helper for instaniating class instances from full namespaced classes.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected ClassResolverInterface $classResolver;

  /**
   * Create a new instance of the AssociationManagementController class.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\association\AssociationNegotiatorInterface $association_negotiator
   *   The association negotiator which determines the active association for
   *   other entities and routes.
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   Helper for instaniating class instances from full namespaced classes.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AssociationNegotiatorInterface $association_negotiator, ClassResolverInterface $class_resolver) {
    $this->entityTypeManager = $entity_type_manager;
    $this->associationNegotiator = $association_negotiator;
    $this->classResolver = $class_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('association.negotiator'),
      $container->get('class_resolver')
    );
  }

  /**
   * Callback to generate the page title of the association management page.
   *
   * @param \Drupal\association\Entity\AssociationInterface $association
   *   The association entity whose entity content is being managed.
   *
   * @return \Stringable|string
   *   A string or markup to use as the page title.
   */
  public function manageContentTitle(AssociationInterface $association): \Stringable|string {
    return new FormattableMarkup('@association_type: @label', [
      '@association_type' => $association->getType()->label(),
      '@label' => $association->label(),
    ]);
  }

  /**
   * Build the association entity content management page.
   *
   * @param \Drupal\association\Entity\AssociationInterface $association
   *   The association entity whose content is being managed.
   *
   * @return array
   *   Renderable array of the entity content managment page.
   */
  public function manageContent(AssociationInterface $association): array {
    $behavior = $association->getBehavior();

    if ($behavior instanceof ManagerBuilderInterface) {
      $builder = $behavior;
    }
    else {
      $builderClass = $behavior->getManagerBuilderClass();
      $builder = $this->classResolver->getInstanceFromDefinition($builderClass);

      if ($builder instanceof PluginAwareInterface) {
        $builder->setPlugin($behavior);
      }
    }

    $builder->setAssociation($association);
    $render = $builder->buildManagerUi();

    // Ensure that caching data has been appropriately set for this page.
    $cache = new BubbleableMetadata();
    $cache->addCacheableDependency($builder);
    $cache->applyTo($render);

    return $render;
  }

  /**
   * Gets the page title to use for the linked entity management page.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   Route match for the current request. Used to determine the association
   *   to build the management page for.
   *
   * @return \Stringable|string
   *   A string or markup to use as the page title.
   */
  public function entityManageTitle(RouteMatchInterface $route_match): \Stringable|string {
    $entityTypeId = $route_match->getParameter('association_entity_type');
    $entity = $route_match->getParameter($entityTypeId);

    if ($association = $this->associationNegotiator->byEntity($entity)) {
      return $this->manageContentTitle($association);
    }

    throw new NotFoundHttpException();
  }

  /**
   * Create the association management page from a linked entity.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   Route match for the current request. Used to determine the association
   *   to build the management page for.
   *
   * @return array
   *   Renderable array of the entity content managment page.
   *
   * @see \Drupal\Association\Routing\AssociationRouteSubscriber::alterRoutes()
   */
  public function entityManageContent(RouteMatchInterface $route_match): array {
    $entityTypeId = $route_match->getParameter('association_entity_type');
    $entity = $route_match->getParameter($entityTypeId);

    if ($association = $this->associationNegotiator->byEntity($entity)) {
      $render = [];

      try {
        $operations = $this->entityTypeManager
          ->getListBuilder('association')
          ->getOperations($association);

        if ($operations) {
          // Clear the operation links of destination query, which will
          // return back to this content page.
          foreach ($operations as &$op) {
            $query = $op['url']->getOption('query');

            if (isset($query['destination'])) {
              unset($query['destination']);
              $op['url']->setOption('query', $query);
            }
          }
          unset($op);

          $render['association_links'] = [
            '#type' => 'details',
            '#open' => TRUE,
            '#title' => $this->t('@bundle_label administrative quick links', [
              '@bundle_label' => $association->getType()->label(),
            ]),

            'links' => [
              '#type' => 'dropbutton',
              '#links' => $operations,
            ],
          ];
        }
      }
      catch (InvalidPluginDefinitionException $e) {
        // Could be thrown if the list builder handler for associations is not
        // avaiable. Should theoretically never happen.
      }

      $render['manage'] = $this->manageContent($association);
      return $render;
    }

    throw new NotFoundHttpException();
  }

}
