<?php

namespace Drupal\association;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\association\Entity\AssociatedEntityInterface;
use Drupal\association\Entity\AssociationInterface;

/**
 * Default Entity Association Negotiator.
 *
 * The default association resolution searches the route for compatible entities
 * to find linked associations from, or associations directly from the route
 * parameters.
 */
class DefaultAssociationNegotiator implements AssociationNegotiatorInterface {

  const ROUTE_REGEX = '/^(entity|layout_builder.overrides)\.(association(?:_page)?)\.[a-z_]+$/';

  /**
   * Manage entity types which are linkable to Entity Associations.
   *
   * @var \Drupal\association\EntityAdapterManagerInterface
   */
  protected EntityAdapterManagerInterface $adapterManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Construct a new instance of the DefaultAssociationNegotiator class.
   *
   * @param \Drupal\association\EntityAdapterManagerInterface $entity_adapter_manager
   *   Manage entity types which are linkable to Entity Associations.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityAdapterManagerInterface $entity_adapter_manager, EntityTypeManagerInterface $entity_type_manager) {
    $this->adapterManager = $entity_adapter_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function byRoute(?RouteMatchInterface $route_match = NULL): ?AssociationInterface {
    if (!$route_match) {
      return NULL;
    }

    $params = $route_match->getParameters();
    $routeName = $route_match->getRouteName();

    if (preg_match(self::ROUTE_REGEX, $routeName)) {
      if ($entity = $params->get('association')) {
        /** @var \Drupal\association\Entity\AssociationInterface|\Drupal\association\Entity\AssociatedEntityInterface $entity */
        return $entity instanceof AssociationInterface ? $entity : $entity->getAssociation();
      }
      elseif ($entity = $params->get('association_page')) {
        /** @var \Drupal\association\Entity\AssociatedEntityInterface $entity */
        return $entity->getAssociation();
      }

      return NULL;
    }

    // Check if this is an entity route, and determine which entity the route
    // is for. We look for routes that start with "entity." or have an
    // "entity_type_id" route parameter.
    $entityTypeId = preg_match('#^entity\.([\w_]+)\.#', $routeName, $matches)
      ? $matches[1] : $params->get('entity_type_id');

    // If we found the entity type for this route, and it is an associable type,
    // check if this entity is associated to any associations.
    if ($entityTypeId && $this->adapterManager->isAssociableType($entityTypeId)) {
      $entity = $params->get($entityTypeId);

      if ($entity instanceof ContentEntityInterface) {
        // Loaded content entities, will store in the computed associations
        // field any association_link entities joining them.
        /** @var \Drupal\association\Entity\AssociationLink */
        $assocLink = $entity->hasField('associations') ? $entity->get('associations')->entity : NULL;
      }
      elseif (is_numeric($entity)) {
        // If the entity is just the ID, then attempt look for a matching
        // association_link which matches this entity type and ID.
        /** @var \Drupal\association\Entity\Storage\AssociationLinkStorageInterface */
        $linkStorage = $this->entityTypeManager->getStorage('association_link');
        /** @var \Drupal\association\Entity\AssociationLink[] */
        $links = $linkStorage->loadByEntityInfo($entityTypeId, $entity);
        $assocLink = reset($links);
      }

      return !empty($assocLink) ? $assocLink->getAssociation() : NULL;
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function byEntity(EntityInterface $entity): ?AssociationInterface {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    if ($entity instanceof AssociatedEntityInterface) {
      return $entity->getAssociation();
    }
    elseif ($this->adapterManager->isAssociable($entity)) {
      /** @var \Drupal\association\Entity\AssociationLink */
      $association = $entity->get('associations')->entity;
      return $association ? $association->getAssociation() : NULL;
    }

    return NULL;
  }

}
