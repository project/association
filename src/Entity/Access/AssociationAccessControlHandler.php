<?php

namespace Drupal\association\Entity\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\association\AssociationPermissions;
use Drupal\association\Entity\AssociationTypeInterface;
use Drupal\Core\Access\AccessResultInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Access controller for association entity operations.
 */
class AssociationAccessControlHandler extends EntityAccessControlHandler implements EntityHandlerInterface {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Create a new instance of SubjecAccessControlHandler class.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   Entity type definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($entity_type);

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type): static {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL): AccessResultInterface {
    $bundle = $this->entityTypeManager
      ->getStorage('association_type')
      ->load($entity_bundle);

    // Bundle type doesn't exist, therefore this route should return that.
    if (!$bundle instanceof AssociationTypeInterface) {
      return AccessResult::forbidden();
    }

    return AccessResult::allowedIfHasPermissions($account, [
      $this->entityType->getAdminPermission(),
      AssociationPermissions::getBundlePermissionKey($bundle, 'create'),
    ], 'OR');
  }

  /**
   * {@inheritdoc}
   */
  public function access(EntityInterface $entity, $operation, ?AccountInterface $account = NULL, $return_as_object = FALSE): AccessResultInterface|bool {
    $account = $this->prepareUser($account);

    // Allow the admin permission to bypass other checks.
    if ($account->hasPermission($this->entityType->getAdminPermission())) {
      $result = AccessResult::allowed()
        ->cachePerPermissions();
    }
    else {
      /** @var \Drupal\Core\Access\AccessResult */
      $result = parent::access($entity, $operation, $account, TRUE);
      $result->cachePerPermissions();
    }

    return $return_as_object ? $result : $result->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResultInterface {
    /** @var \Drupal\association\Entity\AssociationInterface $entity */
    try {
      if ($operation === 'view') {
        if ($entity->isActive() && $account->hasPermission('access content')) {
          return AccessResult::allowed()
            ->cachePerPermissions();
        }

        $operation = 'update';
      }

      $opPermission = AssociationPermissions::getBundlePermissionKey($entity->getType(), $operation);
      return AccessResult::allowedIfHasPermission($account, $opPermission);
    }
    catch (\InvalidArgumentException $e) {
      // Operation doesn't exist for this entity, always return forbidden.
      return AccessResult::forbidden();
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function checkFieldAccess($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, ?FieldItemListInterface $items = NULL): AccessResultInterface {
    if ($account->hasPermission($this->entityType->getAdminPermission())) {
      return AccessResult::allowed()
        ->cachePerPermissions();
    }

    if ($operation === 'edit') {
      // Admin only update only fields.
      switch ($field_definition->getName()) {
        case 'uid':
        case 'created':
        case 'changed':
          return AccessResult::neutral()
            ->cachePerPermissions();

        case 'status':
          $publishPermission = AssociationPermissions::getBundlePermissionKey($field_definition->getTargetBundle(), 'publish');
          return AccessResult::allowedIfHasPermission($account, $publishPermission);
      }
    }

    return parent::checkFieldAccess($operation, $field_definition, $account, $items);
  }

}
