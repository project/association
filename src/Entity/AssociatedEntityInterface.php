<?php

namespace Drupal\association\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Interface for entities that are associated to entity associations.
 */
interface AssociatedEntityInterface extends ContentEntityInterface {

  /**
   * Get the association this entity is associated to.
   *
   * @return \Drupal\association\Entity\AssociationInterface|null
   *   The association this entity is associated to.
   */
  public function getAssociation(): ?AssociationInterface;

  /**
   * Fetch the entity targeted by this association.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Entity targeted by this association.
   */
  public function getTarget(): EntityInterface;

}
