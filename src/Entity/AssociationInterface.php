<?php

namespace Drupal\association\Entity;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\association\Plugin\BehaviorInterface;
use Drupal\association\Plugin\LandingPagePluginInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Base interface for the association entity.
 */
interface AssociationInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface, EntityPublishedInterface {

  /**
   * Gets the purging state of this entity association. True during deletion.
   *
   * @return bool
   *   The state of the association's purging state. TRUE if the association
   *   is in the process of deleting associated content, and other associated
   *   data.
   */
  public function isPurging(): bool;

  /**
   * Gets the association type instance for this association.
   *
   * @return \Drupal\association\Entity\AssociationTypeInterface
   *   Get the bundle entity for this association.
   */
  public function getType(): AssociationTypeInterface;

  /**
   * Get the association type's plugin handler that applies to this entity.
   *
   * The method is normally a convenience method for the association type's
   * \Drupal\association\Entity\AssociationTypeInterface::getPlugin()
   * implementation.
   *
   * @param string $plugin_type
   *   The plugin type identifier of the association type plugin to get.
   *
   * @return \Drupal\Component\Plugin\PluginInspectionInterface|null
   *   The association type's plugin of the requested type if available, or NULL
   *   if the plugin not available.
   *
   * @see \Drupal\association\Entity\AssociationTypeInterface::getPlugin()
   */
  public function getPlugin(string $plugin_type): ?PluginInspectionInterface;

  /**
   * Get the behavior plugin to use with this association entity.
   *
   * @return \Drupal\association\Plugin\BehaviorInterface|null
   *   Returns the association type behavior plugin that should be used
   *   with this association entity.
   */
  public function getBehavior(): ?BehaviorInterface;

  /**
   * Get the landing page handler plugin to use with this association.
   *
   * @return \Drupal\association\Plugin\LandingPagePluginInterface
   *   The association landing page handler plugin that should be used with
   *   this association entity. A fallback is provided if the plugin is not
   *   available (but still logged as an error).
   */
  public function getLandingPageHandler(): LandingPagePluginInterface;

  /**
   * Add an entity to the association.
   *
   * @param string $tag
   *   The association behavior tag.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to associate. The "associations" field will get updated
   *   with the association_link.
   * @param bool $save_link
   *   (optional) Should the association link be saved? Can only happen if
   *   $entity has been saved and has a valid entity ID.
   *
   * @return \Drupal\association\Entity\AssociationLink
   *   The association_link instance created to link the association and entity.
   *
   * @throws \Drupal\association\Entity\Exception\AlreadyAssociatedException
   */
  public function associateEntity(string $tag, ContentEntityInterface $entity, bool $save_link = TRUE): ?AssociationLink;

  /**
   * Indicate if the association is enabled.
   *
   * @return bool
   *   Indicate if the association is currently active.
   */
  public function isActive(): bool;

  /**
   * Get the companion landing page if one is available.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The companion association page if there is one, otherwise boolean FALSE.
   */
  public function getPage(): ?EntityInterface;

  /**
   * Get the landing page settings for this association.
   *
   * @return array
   *   The landing page settings for this association.
   */
  public function getPageSettings(): array;

}
