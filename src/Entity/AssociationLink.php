<?php

namespace Drupal\association\Entity;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Url;

/**
 * Entity representing an link between an association and target content entity.
 *
 * @ContentEntityType(
 *   id = "association_link",
 *   label = @Translation("Association link"),
 *   label_plural = @Translation("Association links"),
 *   base_table = "association_link",
 *   admin_permission = "administer association configurations",
 *   translatable = FALSE,
 *   fieldable = FALSE,
 *   internal = TRUE,
 *   static_cache = FALSE,
 *   list_cache_tags = {
 *     "association_list",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "entity_type",
 *   },
 *   handlers = {
 *     "storage_schema" = "Drupal\association\Entity\Storage\AssociationLinkStorageSchema",
 *     "storage" = "Drupal\association\Entity\Storage\AssociationLinkStorage",
 *     "views_data" = "Drupal\association\Entity\Views\AssociationLinkViewsData",
 *     "route_provider" = {
 *       "html" = "Drupal\association\Entity\Routing\AssociationLinkHtmlRouteProvider",
 *     },
 *   },
 *   links = {
 *     "add-content" = "/association/{association}/add-content/{tag}/{entity_type}/{bundle}",
 *     "edit-content" = "/association/{association}/edit-content/{association_link}",
 *     "delete-content" = "/association/{association}/delete-content/{association_link}",
 *   },
 * )
 */
class AssociationLink extends ContentEntityBase implements AssociatedEntityInterface {

  /**
   * Check the purging state of the owning entity association.
   *
   * Entity associations are purging while they are being deleted, and in the
   * process of clearing their associated content. This allows the links, and
   * other resource to know that they are being deleted.
   *
   * @return bool
   *   Returns if the owning association is purging its content, and data.
   */
  public function isAssociationPurging(): bool {
    $association = $this->getAssociation();
    return $association ? $association->isPurging() : TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTagsToInvalidate(): array {
    // Invalidate the associated content list for the parent association.
    $tags[] = 'association:links:' . $this->association->target_id;

    // When associations links change, inform the owning content to update.
    // This is likely to occur on association link creation and deletion.
    if ($entity = $this->target->entity) {
      $tags = Cache::mergeTags($tags, $entity->getCacheTagsToInvalidate());
    }

    return $tags;
  }

  /**
   * {@inheritdoc}
   */
  public function label(): \Stringable|string {
    return $this->getTarget()->label();

  }

  /**
   * {@inheritdoc}
   */
  public function toUrl($rel = 'canonical', array $options = []): Url {
    if ($rel === 'canonical') {
      // Association links don't have a canonical URL of their own, but will
      // direct to the canonical link of their current target, if there is one.
      return $this->getTarget()->toUrl('canonical', $options);
    }

    return parent::toUrl($rel, $options);
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel): array {
    $routeParams = parent::urlRouteParameters($rel);

    $routeParams['association'] = $this->association->target_id;
    return $routeParams;
  }

  /**
   * {@inheritdoc}
   */
  public function getAssociation(): ?AssociationInterface {
    /** @var \Drupal\association\Entity\AssociationInterface|null $assoc */
    $assoc = $this->association->entity;
    return $assoc;
  }

  /**
   * {@inheritdoc}
   */
  public function getTarget(): EntityInterface {
    // AssociationLinks should always have a target unless they are in the
    // process of being deleted. Therefore if this happens, this association
    // link should be considered malformed and incomplete.
    return $this->target->entity ?: new EntityMalformedException('Association link is missing a target entity.');
  }

  /**
   * Get the entity type machine name of the target entity.
   *
   * @return string
   *   The entity_type machine name of the target entity.
   */
  public function getTargetType(): string {
    return $this->entity_type->value;
  }

  /**
   * Get the bundle identifier of the target entity.
   *
   * @return string
   *   The bundle identifier of the target entity.
   */
  public function getTargetBundle(): string {
    return $this->bundle->value;
  }

  /**
   * Fetch the behavior tag identifier for this association link.
   *
   * @return string
   *   The behavior tag identifier.
   */
  public function getTag(): string {
    return $this->tag->value;
  }

  /**
   * Get the published or status of the target entity.
   *
   * @return bool
   *   The status of the target entity.
   */
  public function getStatus(): bool {
    $entity = $this->getTarget();
    if ($entity instanceof EntityPublishedInterface) {
      return $entity->isPublished();
    }

    $statusKey = $entity
      ->getEntityType()
      ->getKey('status');

    return $statusKey ? $entity->$statusKey->value : TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] */
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['association'] = BaseFieldDefinition::create('entity_reference')
      ->setTargetEntityTypeId($entity_type->id())
      ->setLabel(t('Association'))
      ->setDescription(t('The association.'))
      ->setRequired(TRUE)
      ->setTranslatable(FALSE)
      ->setSetting('target_type', 'association')
      ->setSetting('handler', 'default')
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'entity_reference_label',
        'settings' => [],
        'weight' => 0,
      ]);

    $fields['target'] = BaseFieldDefinition::create('entity_reference')
      ->setTargetEntityTypeId($entity_type->id())
      ->setLabel(t('Target'))
      ->setDescription(t('Entity linked to the association.'))
      ->setRequired(TRUE)
      ->setTranslatable(FALSE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'entity_reference_label',
        'weight' => 0,
      ]);

    $fields['tag'] = BaseFieldDefinition::create('string')
      ->setTargetEntityTypeId($entity_type->id())
      ->setLabel(t('Association tag'))
      ->setDescription(t('Association behavior tagging of this link.'))
      ->setRequired(TRUE)
      ->setTranslatable(FALSE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    // Ensure the bundle type from parent::baseFieldDefinitions generates a
    // column schema that is capable of being used in the keys and indexes of
    // the database table schema.
    $fields['entity_type']->setSettings([
      'is_ascii' => TRUE,
      'max_length' => 64,
      'text_processing' => 0,
    ]);

    $fields['bundle'] = BaseFieldDefinition::create('string')
      ->setTargetEntityTypeId($entity_type->id())
      ->setLabel(t('Target entity bundle'))
      ->setRequired(TRUE)
      ->setTranslatable(FALSE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions): array {
    $entityTypeManager = \Drupal::entityTypeManager();

    $fields = [];
    $original = $base_field_definitions['target'] ?? NULL;

    // Set the association to the appropriate type.
    // Same trick is being used for comment entities at the moment
    // Warning! May change in the future: https://www.drupal.org/node/2346347
    if ($original && $entityTypeManager->hasDefinition($bundle)) {
      $fields['target'] = BaseFieldDefinition::create('entity_reference')
        ->setLabel($original->getLabel())
        ->setDescription($original->getDescription())
        ->setConstraints($original->getConstraints())
        ->setTranslatable(FALSE)
        ->setRequired($original->isRequired())
        ->setDisplayConfigurable('view', $original->isDisplayConfigurable('view'))
        ->setDisplayConfigurable('form', $original->isDisplayConfigurable('form'))
        ->setDisplayOptions('view', $original->getDisplayOptions('view'))
        ->setDisplayOptions('form', $original->getDisplayOptions('form'))
        ->setSetting('handler', $original->getSetting('handler'))
        ->setSetting('target_type', $bundle);
    }

    return $fields;
  }

}
