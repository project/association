<?php

namespace Drupal\association\Entity;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\association\Plugin\BehaviorInterface;
use Drupal\association\Plugin\LandingPagePluginInterface;

/**
 * Interface for association entity type (bundle) classes.
 */
interface AssociationTypeInterface extends ConfigEntityInterface {

  /**
   * Check if this bundle has content data.
   *
   * New config entities cannot have any content created for them yet, so will
   * always return FALSE.
   *
   * @return bool
   *   TRUE if there is content using the type as a bundle, or FALSE is there
   *   is no data of this type.
   */
  public function hasData(): bool;

  /**
   * Should the association type label be used as the local task title?
   *
   * @return bool
   *   Returns TRUE if the local task title should match the type label.
   */
  public function useTaskLabel(): bool;

  /**
   * Indicates if content for associations of this type should be searchable.
   *
   * This method currently works with the search_api module indexes.
   *
   * @return bool
   *   TRUE if content should be included in search index, and FALSE otherwise.
   */
  public function isContentSearchable(): bool;

  /**
   * Get the title that should be used for the association settings local task.
   *
   * @return \Stringable|string
   *   The title that should be used for the local task of the association.
   */
  public function getTaskTitle(): \Stringable|string;

  /**
   * Gets the association type's plugin instance of the requested type.
   *
   * @param string $plugin_type
   *   The plugin type to fetch. Should be either "behavior" or "landing_page"
   *   to get that loaded plugin instance for this association bundle.
   * @param bool $force_rebuild
   *   Force the rebuilding of the plugin, even if it was already instantiated.
   *
   * @return \Drupal\Component\Plugin\PluginInspectionInterface|null
   *   If the requested plugin type is available, an instance fo that plugin
   *   type. NULL if the plugin type is invalid, not available.
   */
  public function getPlugin(string $plugin_type, bool $force_rebuild = FALSE): ?PluginInspectionInterface;

  /**
   * Get the plugin behavior for this association type.
   *
   * @param bool $force_rebuild
   *   Force the rebuilding of the behavior plugin, even if it was already
   *   instantiated.
   *
   * @return \Drupal\association\Plugin\BehaviorInterface|null
   *   Get the plugin to control the behavior of the associations of this type.
   *   Should always return a behavior, unless data has been corrupted.
   */
  public function getBehavior(bool $force_rebuild = FALSE): ?BehaviorInterface;

  /**
   * Get the landing page handler plugin.
   *
   * @param bool $force_rebuild
   *   Force the rebuilding of the landing page handler plugin, even if it was
   *   already instantiated.
   *
   * @return \Drupal\association\Plugin\LandingPagePluginInterface
   *   The landing page plugin for this association type. If this landing page
   *   plugin is not available a fallback handler is provided (error is logged).
   */
  public function getLandingPageHandler(bool $force_rebuild = FALSE): ?LandingPagePluginInterface;

  /**
   * Validation potential configuration changes to ensure safe updates.
   *
   * There are several settings that are important to an association and cannot
   * be safely updated if this association type already has data.
   *
   * @param array $updates
   *   The perspective configuration to validate as options that can update.
   *
   * @return string[]
   *   An array of reported configuration errors if there were any. Returns an
   *   empty array if there were no errors to report.
   */
  public function validateConfigChanges(array $updates);

}
