<?php

namespace Drupal\association\Entity\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of association entities.
 *
 * @ingroup association
 */
class AssociationListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['label'] = $this->t('Association');
    $header['type'] = $this->t('Type');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\association\Entity\AssociationInterface $entity */
    $row['label'] = Link::fromTextAndUrl($entity->label(), $entity->toUrl());
    $row['type'] = $entity->getType()->label();

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity): array {
    /** @var \Drupal\association\Entity\AssociationInterface $entity */
    $operations = parent::getDefaultOperations($entity);

    if (isset($operations['edit'])) {
      $operations['edit']['title'] = $this->t('Settings');
      $operations['edit']['weight'] = 30;
    }

    if ($entity->access('manage') && $entity->hasLinkTemplate('manage')) {
      $operations['manage'] = [
        'title' => $this->t('Manage content'),
        'weight' => 20,
        'url' => $entity->toUrl('manage'),
      ];
    }

    // Add page management links if the association type supports a page.
    if ($page = $entity->getPage()) {
      if ($page->access('view')) {
        $operations['view-page'] = [
          'title' => $this->t('View page'),
          'weight' => -5,
          'url' => $entity->toUrl('canonical'),
        ];
      }

      if ($entity->access('edit')) {
        $operations['edit-page'] = [
          'title' => $this->t('Edit page'),
          'weight' => 10,
          'url' => $this->ensureDestination($page->toUrl('edit-form')),
        ];
      }
    }

    return $operations;
  }

}
