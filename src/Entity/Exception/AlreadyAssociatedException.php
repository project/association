<?php

namespace Drupal\association\Entity\Exception;

/**
 * Exception when trying to associate an entity that is already associated.
 */
class AlreadyAssociatedException extends \Exception {

  /**
   * {@inheritdoc}
   */
  public function __construct(string $message = '', int $code = 0, ?\Throwable $previous = NULL) {
    if (empty($message)) {
      $message = 'Target entity is already associated to this association.';
    }

    parent::__construct($message, $code, $previous);
  }

}
