<?php

namespace Drupal\association\Entity\Form;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\association\Utility\DeleteAssociationBatchOp;
use Drupal\toolshed\Entity\Form\EntityDeleteConfirm;

/**
 * Confirmation form for deleting association entities and linked content.
 *
 * Confirmation form for deleting association entities and providing a choice
 * for the user to determine how to handle content linked to the association
 * while the association is being deleted.
 */
class AssociationDeleteConfirm extends EntityDeleteConfirm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);

    /** @var \Drupal\association\Entity\AssociationInterface */
    $entity = $this->getEntity();
    $associationType = $entity->getType();
    $tParams = [
      '@bundle_label' => $associationType->label(),
    ];

    // Allow user to decide how to handle the linked entities.
    $form['delete_linked_method'] = [
      '#type' => 'radios',
      '#title' => $this->t('When deleting the @bundle_label', $tParams),
      '#options' => [
        'entity_only' => $this->t('Delete the @bundle_label but keep the associated content', $tParams),
        'delete_all' => $this->t('Delete the @bundle_label and delete all the associated content', $tParams),
      ],
      '#default_value' => 'entity_only',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $method = $form_state->getValue('delete_linked_method');

    // Use the default entity delete method to just delete the entity.
    if ($method === 'entity_only') {
      parent::submitForm($form, $form_state);
    }
    elseif ($method === 'delete_all') {
      /** @var \Drupal\association\Entity\AssociationInterface */
      $entity = $this->getEntity();
      $behavior = $entity->getBehavior();
      $associations = [$entity->id()];
      $operations = [];

      foreach ($behavior->getEntityTypes() as $entityType) {
        $operations[] = [
          DeleteAssociationBatchOp::class . '::deleteLinkedEntitiesByType',
          [$associations, $entityType],
        ];
      }

      // Final clean-up to remove any dangle entities still left, after
      // cleaning by associations by entity type. Theoretically this should be
      // clear, but it is a good measure to avoid any dangling content.
      $operations[] = [
        DeleteAssociationBatchOp::class . '::deleteLinkedEntities',
        [$associations],
      ];

      batch_set([
        'title' => $this->t('Removing associated content'),
        'operations' => $operations,
        'finished' => [$this, 'deleteAssociationBatchFinish'],
      ]);

      // After batch completes redirect back to the association list page.
      $form_state->setRedirectUrl($this->getCancelUrl());
    }
  }

  /**
   * Report the results of the batch execution.
   *
   * @param bool $success
   *   Indicate of the batch completed successfully.
   * @param array $results
   *   Results information collected during the batch operations execution.
   * @param array $operations
   *   Array of batch operations which did not complete in the case of an error.
   */
  public function deleteAssociationBatchFinish(bool $success, array $results, array $operations): void {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
      $entity = $this->entity;
      $entityType = $entity->getEntityType();

      try {
        $entity->delete();

        $this->logDeletionMessage();
        $this->messenger()->addStatus($this->t('@entity_type of %label has been deleted.', [
          '@entity_type' => $entityType->getLabel(),
          '%label' => $entity->label(),
        ]));
      }
      catch (EntityStorageException $e) {
        $this->messenger()->addError($this->t('Unable to delete @entity_type %label : @message', [
          '@entity_type' => $entityType->getLabel(),
          '%label' => $entity->label(),
          '@message' => $e->getMessage(),
        ]));
      }
    }
    else {
      $this->messenger()->addError($this->t('Finished with an error.'));
    }
  }

}
