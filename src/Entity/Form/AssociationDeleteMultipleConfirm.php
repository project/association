<?php

namespace Drupal\association\Entity\Form;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\Form\DeleteMultipleForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\association\Utility\DeleteAssociationBatchOp;

/**
 * Form for deleting multiple association entities at once.
 */
class AssociationDeleteMultipleConfirm extends DeleteMultipleForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $entity_type_id = NULL): array {
    $form = parent::buildForm($form, $form_state, $entity_type_id);

    // Allow user to decide how to handle the linked entities.
    $form['delete_linked_method'] = [
      '#type' => 'radios',
      '#title' => $this->t('When deleting the associations'),
      '#options' => [
        'entity_only' => $this->t('Delete associations and keep associated content'),
        'delete_all' => $this->t('Delete associations and delete all associated content'),
      ],
      '#default_value' => 'entity_only',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $method = $form_state->getValue('delete_linked_method');

    // Use the default entity delete method to just delete the entity.
    if ($method === 'entity_only') {
      parent::submitForm($form, $form_state);
    }
    elseif ($method === 'delete_all') {
      $storage = $this->entityTypeManager->getStorage($this->entityTypeId);
      $associationIds = array_keys($this->selection);
      /** @var \Drupal\association\Entity\AssociationInterface[] $entities */
      $entities = $storage->loadMultiple($associationIds);

      $bundles = [];
      $linkedTypes = [];
      foreach ($entities as $entity) {
        // Find all entity types that are allowed to be linked to associations
        // from all the association types in this set of entities.
        if (!isset($bundles[$entity->bundle()])) {
          $bundles[$entity->bundle()] = $entity->bundle();
          $linkedTypes += $entity->getBehavior()->getEntityTypes();
        }
      }

      $operations = [];
      foreach ($linkedTypes as $entityType) {
        $operations[] = [
          DeleteAssociationBatchOp::class . '::deleteLinkedEntitiesByType',
          [$associationIds, $entityType],
        ];
      }

      // Final clean-up to remove any dangle entities still left, after
      // cleaning by entity type. Theoretically this should be clear, but it is
      // a good measure to avoid any dangling content.
      $operations[] = [
        DeleteAssociationBatchOp::class . '::deleteLinkedEntities',
        [$associationIds],
      ];

      batch_set([
        'title' => $this->t('Removing associated content'),
        'operations' => $operations,
        'finished' => [$this, 'deleteAssociationBatchFinish'],
      ]);

      // After batch completes redirect back to the associations listing page.
      $form_state->setRedirectUrl($this->getCancelUrl());
    }
  }

  /**
   * Report the results of the batch execution.
   *
   * @param bool $success
   *   Indicate of the batch completed successfully.
   * @param array $results
   *   Results information collected during the batch operations execution.
   * @param array $operations
   *   Array of batch operations which did not complete in the case of an error.
   */
  public function deleteAssociationBatchFinish(bool $success, array $results, array $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
      try {
        $storage = $this->entityTypeManager->getStorage($this->entityTypeId);
        $entities = $storage->loadMultiple(array_keys($this->selection));

        $storage->delete($entities);
        $this->tempStore->delete((string) $this->currentUser->id());

        $this->messenger()->addStatus($this->t('Selected @entity_type have been deleted.', [
          '@entity_type' => 'Entity Associations',
        ]));
      }
      catch (EntityStorageException $e) {
        $this->messenger()->addError($this->t('Unable to delete selected @entity_type : @message', [
          '@entity_type' => 'Entity Associations',
          '@message' => $e->getMessage(),
        ]));
      }
    }
    else {
      $this->messenger()->addError($this->t('Finished with an error.'));
    }
  }

}
