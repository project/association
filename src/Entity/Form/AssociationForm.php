<?php

namespace Drupal\association\Entity\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Entity form for creating an association entity.
 */
class AssociationForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    $form['advanced'] = [
      '#type' => 'vertical_tabs',
      '#weight' => 50,
    ];

    $form['landing_page'] = [
      '#type' => 'details',
      '#title' => $this->t('Landing Page'),
      '#group' => 'advanced',
      '#optional' => TRUE,
      '#access' => $form['page']['widget']['#access'] ?? $form['page']['#access'],
    ];

    $form['author'] = [
      '#type' => 'details',
      '#title' => $this->t('Authoring Information'),
      '#group' => 'advanced',
      '#optional' => TRUE,
      '#access' => $form['uid']['#access'] ?? NULL,
    ];

    $form['page']['#group'] = 'landing_page';
    $form['uid']['#group'] = 'author';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $status = parent::save($form, $form_state);

    /** @var \Drupal\association\Entity\AssociationInterface $entity */
    $entity = $this->getEntity();
    $msgParams = [
      '%label' => $entity->label(),
      ':bundle' => $entity->getType()->label(),
    ];

    $msg = SAVED_NEW === $status
      ? $this->t('Created the %label :bundle.', $msgParams)
      : $this->t('Saved the %label :bundle.', $msgParams);

    $this->messenger()->addStatus($msg);
    $form_state->setRedirectUrl($entity->toUrl('manage'));

    return $status;
  }

}
