<?php

namespace Drupal\association\Entity\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Drupal\association\Controller\AssociationManagementController;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * HTML route provider callback for the association entity routes.
 */
class AssociationHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type): array|RouteCollection {
    $collection = parent::getRoutes($entity_type);
    $entityTypeId = $entity_type->id();

    if ($contentManagementRoute = $this->getContentManagementRoute($entity_type)) {
      $collection->add("entity.{$entityTypeId}.manage", $contentManagementRoute);
    }

    return $collection;
  }

  /**
   * {@inheritdoc}
   */
  protected function getCollectionRoute(EntityTypeInterface $entity_type): ?Route {
    $route = parent::getCollectionRoute($entity_type);

    if ($route) {
      $route->setRequirement('_permission', 'access association entity overview page');
    }

    return $route;
  }

  /**
   * {@inheritdoc}
   */
  protected function getCanonicalRoute(EntityTypeInterface $entity_type): ?Route {
    // Allow for the canonical link template, but don't generate the route.
    // This allows link to entity functionality to work without needing a page.
    // The association entity ::toUrl() method conditionally switches the
    // default URL for the association.
    //
    // @see \Drupal\association\Entity\Association::toUrl()
    return NULL;
  }

  /**
   * Create the association content management route if it exists.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route content management if available.
   */
  protected function getContentManagementRoute(EntityTypeInterface $entity_type): ?Route {
    if ($entity_type->hasLinkTemplate('manage')) {
      $entityTypeId = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('manage'));

      $route->addDefaults([
        '_title_callback' => AssociationManagementController::class . '::manageContentTitle',
        '_controller' => AssociationManagementController::class . '::manageContent',
        'entity_type_id' => $entityTypeId,
      ]);

      // Ensure that user has required permissions to manage the
      // associations's linked content entities.
      $route
        ->setRequirement('_association_manage_content_access', $entityTypeId)
        ->setRequirement($entityTypeId, '\\d+')
        ->setOption('_admin_route', TRUE)
        ->setOption('parameters', [
          $entityTypeId => [
            'type' => 'entity:' . $entityTypeId,
          ],
        ]);

      return $route;
    }

    return NULL;
  }

}
