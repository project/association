<?php

namespace Drupal\association\Entity\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\EntityRouteProviderInterface;
use Drupal\association\Controller\AssociationLinkController;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * HTML route provider callback for the association link entities.
 */
class AssociationLinkHtmlRouteProvider implements EntityRouteProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type): array|RouteCollection {
    $collection = new RouteCollection();
    $entityTypeId = $entity_type->id();

    if ($addRoute = $this->getAddLinkedContentRoute($entity_type)) {
      $collection->add("entity.{$entityTypeId}.add_content", $addRoute);
    }

    if ($addRoute = $this->getEditLinkedContentRoute($entity_type)) {
      $collection->add("entity.{$entityTypeId}.edit_content", $addRoute);
    }

    if ($addRoute = $this->getDeleteLinkedContentRoute($entity_type)) {
      $collection->add("entity.{$entityTypeId}.delete_content", $addRoute);
    }

    return $collection;
  }

  /**
   * Create the route to add a new linked content entity.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route if an add-content link template is available.
   */
  public function getAddLinkedContentRoute(EntityTypeInterface $entity_type): ?Route {
    if ($entity_type->hasLinkTemplate('add-content')) {
      $entityTypeId = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('add-content'));

      $route->addDefaults([
        '_title_callback' => AssociationLinkController::class . '::getAddTitle',
        '_controller' => AssociationLinkController::class . '::createLinkedContentForm',
        'entity_type_id' => $entityTypeId,
      ]);

      $route
        ->setRequirement('_entity_access', 'association.create_content')
        ->setRequirement('association', '\\d+')
        ->setRequirement('_create_association_link', 'association')
        ->setOption('_admin_route', TRUE)
        ->setOption('parameters', [
          'association' => ['type' => 'entity:association'],
          'tag' => ['type' => 'string'],
          'entity_type' => ['type' => 'string'],
          'bundle' => ['type' => 'string'],
        ]);

      return $route;
    }

    return NULL;
  }

  /**
   * Create the route to edit the associated target entity.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route if an edit-content link template is available.
   */
  public function getEditLinkedContentRoute(EntityTypeInterface $entity_type): ?Route {
    if ($entity_type->hasLinkTemplate('edit-content')) {
      $entityTypeId = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('edit-content'));

      $route->addDefaults([
        '_title_callback' => AssociationLinkController::class . '::getEditTitle',
        '_controller' => AssociationLinkController::class . '::editLinkedContentForm',
        'entity_type_id' => $entityTypeId,
      ]);

      $route
        ->setRequirement('_entity_access', 'association.manage')
        ->setRequirement('association', '\\d+')
        ->setRequirement('association_link', '\\d+')
        ->setOption('_admin_route', TRUE)
        ->setOption('parameters', [
          'association' => [
            'type' => 'entity:association',
          ],
          $entityTypeId => [
            'type' => "entity:{$entityTypeId}",
          ],
        ]);

      return $route;
    }

    return NULL;
  }

  /**
   * Create the route to delete associated target entity.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route if an delete-content link template is available.
   */
  public function getDeleteLinkedContentRoute(EntityTypeInterface $entity_type): ?Route {
    if ($entity_type->hasLinkTemplate('delete-content')) {
      $entityTypeId = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('delete-content'));

      $route->addDefaults([
        '_title_callback' => AssociationLinkController::class . '::getDeleteTitle',
        '_controller' => AssociationLinkController::class . '::deleteLinkedContentForm',
        'entity_type_id' => $entityTypeId,
      ]);

      $route
        ->setRequirement('_entity_access', 'association.delete_content')
        ->setRequirement('association', '\\d+')
        ->setRequirement('association_link', '\\d+')
        ->setOption('_admin_route', TRUE)
        ->setOption('parameters', [
          'association' => [
            'type' => 'entity:association',
          ],
          $entityTypeId => [
            'type' => "entity:{$entityTypeId}",
          ],
        ]);

      return $route;
    }

    return NULL;
  }

}
