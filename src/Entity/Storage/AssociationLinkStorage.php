<?php

namespace Drupal\association\Entity\Storage;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Language\LanguageInterface;
use Drupal\association\Entity\AssociationInterface;

/**
 * Defines the storage handler class for association entities.
 *
 * This extends the base storage class, adding the ability to generate the
 * complementary association pages when the type dictates that it has pages.
 *
 * @ingroup association
 */
class AssociationLinkStorage extends SqlContentEntityStorage implements AssociationLinkStorageInterface {

  /**
   * {@inheritdoc}
   */
  protected function mapFromStorageRecords(array $records, $load_from_revision = FALSE): array {
    if (!$records) {
      return [];
    }

    // Get the names of the fields that are stored in the base table.
    $fieldNames = $this->tableMapping->getFieldNames($this->baseTable);

    $values = [];
    foreach ($records as $id => $record) {
      $values[$id] = [];

      // Skip the item delta and item value levels (if possible) but let the
      // field assign the value as suiting. This avoids unnecessary array
      // hierarchies and saves memory here.
      foreach ($fieldNames as $fieldName) {
        $tableCols = $this->tableMapping->getColumnNames($fieldName);
        $colDefs = $this->fieldStorageDefinitions[$fieldName]->getColumns();

        // Handle field types that store several properties.
        if (count($tableCols) > 1) {
          foreach ($tableCols as $property => $colName) {
            if (property_exists($record, $colName)) {
              $values[$id][$fieldName][LanguageInterface::LANGCODE_DEFAULT][$property] = !empty($colDefs[$property]['serialize'])
                ? unserialize($record->{$colName}, ['allowed_classes' => FALSE])
                : $record->{$colName};

              unset($record->{$colName});
            }
          }
        }
        else {
          $colName = reset($tableCols);
          if (property_exists($record, $colName)) {
            $colDef = reset($colDefs);
            $values[$id][$fieldName][LanguageInterface::LANGCODE_DEFAULT] = !empty($colDef['serialize'])
              ? unserialize($record->{$colName}, ['allowed_classes' => FALSE])
              : $record->{$colName};

            unset($record->{$colName});
          }
        }
      }

      // Handle additional entries that are not provided by an entity fields.
      foreach ($record as $name => $value) {
        $values[$id][$name][LanguageInterface::LANGCODE_DEFAULT] = $value;
      }
    }

    $entities = [];
    foreach ($values as $id => $entity_values) {
      $bundle = $this->bundleKey ? $entity_values[$this->bundleKey][LanguageInterface::LANGCODE_DEFAULT] : FALSE;

      // Turn the record into an entity class.
      $entityClass = $this->getEntityClass();
      $entities[$id] = new $entityClass($entity_values, $this->entityTypeId, $bundle);
    }
    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public function loadByTarget(EntityInterface $entity): array {
    return $this->loadByEntityInfo($entity->getEntityTypeId(), $entity->id());
  }

  /**
   * {@inheritdoc}
   */
  public function loadByEntityInfo($entity_type_id, $entity_id): array {
    $loaded = [];
    $rows = $this->database
      ->select($this->getBaseTable(), 'base')
      ->fields('base')
      ->condition('entity_type', $entity_type_id)
      ->condition('target', $entity_id)
      ->range(0, 1)
      ->execute()
      ->fetchAllAssoc($this->idKey);

    if ($rows) {
      $ids = array_keys($rows);
      $loaded = $this->getFromPersistentCache($ids);

      if ($ids && $remaining = array_intersect_key($rows, array_flip($ids))) {
        $stored = $this->mapFromStorageRecords($remaining);
        $this->invokeStorageLoadHook($stored);
        $this->setPersistentCache($stored);
        $loaded += $stored;
      }

      $this->postLoad($loaded);
    }

    return $loaded;
  }

  /**
   * {@inheritdoc}
   */
  public function loadByAssociation(AssociationInterface $association): array {
    $loaded = [];
    $rows = $this->database
      ->select($this->getBaseTable(), 'base')
      ->fields('base')
      ->condition('association', $association->id())
      ->execute()
      ->fetchAllAssoc($this->idKey);

    if ($rows) {
      $ids = array_keys($rows);
      $loaded = $this->getFromPersistentCache($ids);

      if ($ids && $remaining = array_intersect_key($rows, array_flip($ids))) {
        $stored = $this->mapFromStorageRecords($remaining);
        $this->invokeStorageLoadHook($stored);
        $this->setPersistentCache($stored);
        $loaded += $stored;
      }

      $this->postLoad($loaded);
    }

    return $loaded;
  }

}
