<?php

namespace Drupal\association\Entity\Storage;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\association\Entity\AssociationInterface;

/**
 * Defines the interface for storage handler class for association links.
 *
 * Associations are links are the join table for the association and its target
 * entity content. The table should in general be non-fieldable,
 * non-revisionable and non-translatable, make the some shortcuts to make the
 * entities faster to load and build.
 *
 * @ingroup association
 */
interface AssociationLinkStorageInterface extends ContentEntityStorageInterface {

  /**
   * Get the associations links that belong to the provided association.
   *
   * @param \Drupal\association\Entity\AssociationInterface $association
   *   Association to find the association links for.
   *
   * @return \Drupal\association\Entity\AssociationLink[]
   *   The loaded association links which belong to the association requested.
   */
  public function loadByAssociation(AssociationInterface $association): array;

  /**
   * Get all the association links that target the provided entity.
   *
   * These are all the association links that link/join the requested entity
   * to an entity association.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The association target content to find all the association links for.
   *
   * @return \Drupal\association\Entity\AssociationLink[]
   *   The loaded association links which target the provided content entity.
   */
  public function loadByTarget(EntityInterface $entity): array;

  /**
   * Get all the association links that target the provided entity.
   *
   * These are all the association links that link/join the requested entity
   * to an entity association.
   *
   * @param string $entity_type_id
   *   The entity type machine name of the target entity to match.
   * @param string|int $entity_id
   *   The ID of the target entity to find association links for.
   *
   * @return \Drupal\association\Entity\AssociationLink[]
   *   The loaded association links which target the provided content entity.
   */
  public function loadByEntityInfo($entity_type_id, $entity_id): array;

}
