<?php

namespace Drupal\association\Entity\Storage;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Entity handler for association link entity storage schemas.
 */
class AssociationLinkStorageSchema extends SqlContentEntityStorageSchema {

  /**
   * {@inheritdoc}
   */
  protected function initializeBaseTable(ContentEntityTypeInterface $entity_type): array {
    $schema = parent::initializeBaseTable($entity_type);

    $schema['indexes']['target_entity'] = [
      'entity_type',
      'target',
    ];

    // Ensure that an entity can only be linked to the same association,
    // only once. Any additional linking attempts should fail.
    $schema['unique keys']['association_link_target'] = [
      'association',
      'entity_type',
      'target',
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  protected function getSharedTableFieldSchema(FieldStorageDefinitionInterface $storage_definition, $table_name, array $column_mapping): array {
    $schema = parent::getSharedTableFieldSchema($storage_definition, $table_name, $column_mapping);
    $name = $storage_definition->getName();

    // The association link target doesn't get normal entity reference indexes.
    // This index will get added with the static::initializeBaseTable() as it
    // needs to be combined with the "entity_type" field.
    if ($name === 'target') {
      unset($schema['indexes']);
    }

    // Ensure required base definitions are not allowed to have NULL values.
    if ($storage_definition instanceof BaseFieldDefinition && $storage_definition->isRequired()) {
      $schema['fields'][$name]['not null'] = TRUE;
    }

    return $schema;
  }

}
