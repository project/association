<?php

namespace Drupal\association\Entity\Storage;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Defines the storage handler class for association entities.
 *
 * This extends the base storage class, adding the ability to generate the
 * complementary association pages when the type dictates that it has pages.
 *
 * @ingroup association
 */
class AssociationStorage extends SqlContentEntityStorage {

  /**
   * {@inheritdoc}
   */
  protected function doSave($id, EntityInterface $entity): bool|int {
    /** @var \Drupal\association\Entity\AssociationInterface $entity */

    // The "new" flag is removed after the save is completed.
    $isNew = $entity->isNew();
    $status = parent::doSave($id, $entity);

    // Update the landing page based on the association updates. Needs to be
    // called after the save to ensure association has an ID on creation.
    $pageHandler = $entity->getLandingPageHandler();
    $method = $isNew ? $pageHandler->onCreate(...) : $pageHandler->onUpdate(...);
    $method($entity);

    return $status;
  }

}
