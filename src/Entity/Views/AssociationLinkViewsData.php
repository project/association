<?php

namespace Drupal\association\Entity\Views;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\association\EntityAdapterManagerInterface;
use Drupal\views\EntityViewsData;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the views data for the association link entities.
 */
class AssociationLinkViewsData extends EntityViewsData {

  /**
   * The entity type adapter plugin manager.
   *
   * @var \Drupal\association\EntityAdapterManagerInterface|null
   */
  protected ?EntityAdapterManagerInterface $entityAdapterManager = NULL;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type): static {
    $instance = parent::createInstance($container, $entity_type);
    $instance->setEntityAdapterManager($container->get('strategy.manager.association.entity_adapter'));

    return $instance;
  }

  /**
   * Set the entity adapter manager for this instance.
   *
   * @param \Drupal\association\EntityAdapterManagerInterface $entity_adapter_manager
   *   Manager entity type adapters for associating to association entities.
   */
  public function setEntityAdapterManager(EntityAdapterManagerInterface $entity_adapter_manager): void {
    $this->entityAdapterManager = $entity_adapter_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    // The association link entity table to base fields and requests from.
    $baseTable = $this->entityType->getDataTable() ?: $this->entityType->getBaseTable();

    // Add a custom numeric argument for the parent association ID that allows
    // us to use replacement titles with the parent's label.
    $data[$baseTable]['association']['argument'] = [
      'id' => 'association_id',
      'numeric' => TRUE,
    ];

    // Unset the 'target' field relationship as this is dynamic based on the
    // bundle type of the association.
    unset($data[$baseTable]['target']['relationship']);

    foreach ($this->entityAdapterManager->getEntityTypes() as $entityType) {
      try {
        $entityDef = $this->entityTypeManager->getDefinition($entityType);
        $entityLabel = $entityDef->getLabel();
        $entityTable = $entityDef->getDataTable() ?: $entityDef->getBaseTable();

        $data[$entityTable]['association_link'] = [
          'title' => $this->entityType->getLabel(),
          'help' => $this->t('The @entity_type linked with a association entity.', [
            '@entity_type' => $entityLabel,
          ]),
          'relationship' => [
            'id' => 'standard',
            'group' => $this->entityType->getLabel(),
            'base' => $baseTable,
            'base field' => 'target',
            'relationship field' => $entityDef->getKey('id'),
            'extra' => [
              [
                'field' => 'entity_type',
                'value' => $entityDef->id(),
              ],
            ],
            'target_entity_type' => $this->entityType->id(),
          ],
        ];
      }
      catch (PluginNotFoundException $e) {
        // Entity type is missing, probably the module has been uninstalled and
        // the entity settings need to get updated. Skip the type without
        // breaking completely.
      }
    }

    return $data;
  }

}
