<?php

namespace Drupal\association\Entity\Views;

use Drupal\views\EntityViewsData;

/**
 * Entity views data handler for association entities.
 */
class AssociationViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    $baseTable = $this->entityType->getDataTable() ?: $this->entityType->getBaseTable();

    $data[$baseTable]['id']['argument'] = [
      'id' => 'association_id',
      'numeric' => TRUE,
    ];

    $data[$baseTable]['uid']['argument'] = [
      'id' => 'user_uid',
      'numeric' => TRUE,
    ];

    $data[$baseTable]['bulk_form'] = [
      'title' => $this->t('Entity Association operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple association entities.'),
      'field' => ['id' => 'bulk_form'],
    ];

    $data[$baseTable]['uid']['filter']['id'] = 'user_name';

    return $data;
  }

}
