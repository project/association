<?php

namespace Drupal\association;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\association\Adapter\EntityAdapter;
use Drupal\association\Adapter\EntityAdapterInterface;
use Drupal\toolshed\Strategy\Exception\StrategyNotFoundException;
use Drupal\toolshed\Strategy\StrategyManager;

/**
 * The entity type adapter manager for Entity Association linked content.
 */
class EntityAdapterManager extends StrategyManager implements EntityAdapterManagerInterface {

  /**
   * {@inheritdoc}
   */
  protected string $strategyInterface = EntityAdapterInterface::class;

  /**
   * Maps Entity type IDs (keys) to adapter plugin IDs (values).
   *
   * @var string[]
   */
  protected ?array $entityTypes = NULL;

  /**
   * Get the mapping from entity type ID to an adapter plugin ID.
   *
   * @return array
   *   Array that works as a lookup for entity types the appropriate adapter
   *   plugin to use. The array keys are the entity type IDs and the values are
   *   the adapter plugin IDs.
   */
  protected function getEntityTypeMap(): array {
    if (!isset($this->entityTypes)) {
      $this->entityTypes = [];

      foreach ($this->getDefinitions() as $pluginId => $definition) {
        $this->entityTypes[$definition->id()] = $pluginId;
      }
    }

    return $this->entityTypes;
  }

  /**
   * {@inheritdoc}
   */
  public function isAssociableType(string $entity_type_id): bool {
    $allowedTypes = $this->getEntityTypeMap();
    return isset($allowedTypes[$entity_type_id]);
  }

  /**
   * {@inheritdoc}
   */
  public function isAssociable(EntityInterface $entity): bool {
    $entityType = $entity->getEntityTypeId();

    if ($this->isAssociableType($entityType)) {
      return ($entity instanceof ContentEntityInterface && $entity->hasField('associations'))
        || $entity instanceof ConfigEntityInterface;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypes(): array {
    $typeIds = array_keys($this->getEntityTypeMap());
    return array_combine($typeIds, $typeIds);
  }

  /**
   * {@inheritdoc}
   */
  public function getAdapter(EntityInterface $entity): EntityAdapterInterface {
    return $this->getAdapterByEntityType($entity->getEntityTypeId());
  }

  /**
   * {@inheritdoc}
   */
  public function getAdapterByEntityType(string $entity_type_id): EntityAdapterInterface {
    $allowedTypes = $this->getEntityTypeMap();

    if (isset($allowedTypes[$entity_type_id])) {
      /** @var \Drupal\association\Adapter\EntityAdapterInterface $adapter */
      $adapter = $this->getInstance($allowedTypes[$entity_type_id]);
      return $adapter;
    }

    throw new StrategyNotFoundException($entity_type_id);
  }

  /**
   * {@inheritdoc}
   */
  protected function defaultStrategyClass(array $definition): string {
    return EntityAdapter::class;
  }

}
