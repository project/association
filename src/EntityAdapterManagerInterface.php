<?php

namespace Drupal\association;

use Drupal\Core\Entity\EntityInterface;
use Drupal\association\Adapter\EntityAdapterInterface;
use Drupal\toolshed\Strategy\StrategyManagerInterface;

/**
 * Interface for managing entity adapter to use with associations.
 *
 * Provides consistent and extensible way to fetch and manage entity type
 * adapters. These adapters support bridging entity permissions, enumberating
 * bundles and other entity type specific functionality linking to associations.
 */
interface EntityAdapterManagerInterface extends StrategyManagerInterface {

  /**
   * Inform the caller if an entity type is supported as an association target.
   *
   * @param string $entity_type_id
   *   The entity type ID of the entity type to check if it is supported to be
   *   used as an association link target.
   *
   * @return bool
   *   Returns TRUE if this entity type can be linked to an association.
   */
  public function isAssociableType(string $entity_type_id): bool;

  /**
   * Helper method to check if an entity can be the linked to an association.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity to check if it can be the target of an association.
   *
   * @return bool
   *   Returns TRUE if this entity is an associable entity type. This does not
   *   determine if the entity has an association, just that it can be a target
   *   of one.
   */
  public function isAssociable(EntityInterface $entity): bool;

  /**
   * Retrieve entity type IDs which can be used as association link targets.
   *
   * @return string[]
   *   The entity type identifiers for the entity types that are supported
   *   targets of association links.
   */
  public function getEntityTypes(): array;

  /**
   * Fetch the entity adapter plugin appropriate for the $entity parameter.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to fetch the adapter plugin for.
   *
   * @return \Drupal\association\Adapter\EntityAdapterInterface
   *   The entity adapter if an appropriate one could be found.
   *
   * @throws \Drupal\toolshed\Strategy\Exception\StrategyNotFoundException
   *   If entity type is unsuppported and does not have a valid entity type
   *   adapter available.
   */
  public function getAdapter(EntityInterface $entity): EntityAdapterInterface;

  /**
   * Fetch the entity adapter plugin by the entity type ID.
   *
   * @param string $entity_type_id
   *   Entity type machine name, to fetch the adapter plugin for.
   *
   * @return \Drupal\association\Adapter\EntityAdapterInterface
   *   The entity adapter if an appropriate one could be found.
   *
   * @throws \Drupal\toolshed\Strategy\Exception\StrategyNotFoundException
   *   If entity type is unsuppported and does not have a valid entity type
   *   adapter available.
   */
  public function getAdapterByEntityType(string $entity_type_id): EntityAdapterInterface;

}
