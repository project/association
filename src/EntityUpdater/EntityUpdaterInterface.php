<?php

namespace Drupal\association\EntityUpdater;

use Drupal\Core\Entity\EntityInterface;
use Drupal\association\Entity\AssociationInterface;
use Drupal\association\Entity\AssociationLink;

/**
 * The interface for entity updater classes.
 */
interface EntityUpdaterInterface {

  /**
   * Determine if this updater should be run based on this association state.
   *
   * This method is also expected to perform checks like module requirements
   * or other need dependencies.
   *
   * @param \Drupal\association\Entity\AssociationInterface $association
   *   The association who's content will be updated.
   *
   * @return bool
   *   Does this association require this update? TRUE if it does, and FALSE
   *   if it doesn't need this update.
   */
  public static function applies(AssociationInterface $association): bool;

  /**
   * Should the updater queue skip this updater if the entity will be saved.
   *
   * Some updaters perform updates on entities that will happen during an
   * entity update. If save will be called, this renders this update as no
   * longer needed.
   *
   * @return bool
   *   TRUE if the updater should be skipped if the content will be re-saved.
   */
  public function skipIfSave(): bool;

  /**
   * Does this updater need the entity to be saved.
   *
   * This method allows multiple updaters to modify the content individually
   * and have a single entity save call at the end.
   *
   * @return bool
   *   TRUE if this updater expects the entity to be saved, FALSE otherwise.
   */
  public function requiresSave(): bool;

  /**
   * Performs the updates to the content entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The content entity being update by the updater.
   * @param \Drupal\association\Entity\AssociationLink $link
   *   The association link which links the association and the content entity.
   */
  public function update(EntityInterface $entity, AssociationLink $link): void;

}
