<?php

namespace Drupal\association\EntityUpdater;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\association\Entity\AssociationInterface;
use Drupal\association\Entity\AssociationLink;
use Drupal\pathauto\PathautoGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Updates the entity path alias for associated content.
 */
class PathAliasUpdater implements EntityUpdaterInterface, ContainerInjectionInterface {

  /**
   * The service for generating pathauto aliases.
   *
   * @var \Drupal\pathauto\PathautoGeneratorInterface
   */
  protected PathautoGeneratorInterface $pathautoGenerator;

  /**
   * Creates an instance of a PathAliasUpdater for associated entity updates.
   *
   * @param \Drupal\pathauto\PathautoGeneratorInterface $pathauto_generator
   *   Pathauto generator to refresh path aliases when needed.
   */
  public function __construct(PathautoGeneratorInterface $pathauto_generator) {
    $this->pathautoGenerator = $pathauto_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('pathauto.generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function applies(AssociationInterface $association): bool {
    if ($association->hasField('path') && !empty($association->original)) {
      $original = $association->original;

      /* @phpstan-ignore-next-line */
      return $original->get('path')->alias !== $association->get('path')->alias
        && \Drupal::hasService('pathauto.generator');
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function skipIfSave(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function requiresSave(): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function update(EntityInterface $entity, AssociationLink $link): void {
    $this->pathautoGenerator->updateEntityAlias($entity, 'update');
  }

}
