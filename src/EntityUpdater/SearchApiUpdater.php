<?php

namespace Drupal\association\EntityUpdater;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Extension\Exception\UnknownExtensionException;
use Drupal\association\Entity\AssociationInterface;
use Drupal\association\Entity\AssociationLink;

/**
 * Entity updater for refreshing the content search in the search indexes.
 */
class SearchApiUpdater implements EntityUpdaterInterface {

  /**
   * Creates an instance of a SearchApiUpdater for associated entity updates.
   */
  public function __construct() {
    if (!function_exists('search_api_entity_update')) {
      throw new UnknownExtensionException('Missing Search API module, for updating search index information.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function applies(AssociationInterface $association): bool {
    return function_exists('search_api_entity_update')
      && $association->getType()->isContentSearchable();
  }

  /**
   * {@inheritdoc}
   */
  public function skipIfSave(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function requiresSave(): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function update(EntityInterface $entity, AssociationLink $link): void {
    search_api_entity_update($entity);
  }

}
