<?php

namespace Drupal\association\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\association\Entity\AssociationInterface;

/**
 * Event class for associated entity form operations.
 */
class AssociatedEntityFormEvent extends Event {

  /**
   * The event identifier for this current event.
   *
   * @var string
   */
  protected string $event;

  /**
   * The entity assocation which the form is operating on.
   *
   * @var \Drupal\association\Entity\AssociationInterface
   */
  protected AssociationInterface $association;

  /**
   * The entity being operated on the the entity operation form.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected ContentEntityInterface $entity;

  /**
   * Reference to the entity operation form elements.
   *
   * @var array
   */
  protected array $form;

  /**
   * The entity operation form state.
   *
   * @var \Drupal\Core\Form\FormStateInterface
   */
  protected FormStateInterface $formState;

  /**
   * Creates a new instance of the AssociationEntityFormEvent.
   *
   * @param string $op
   *   The entity form operation being performed.
   * @param array $form
   *   The reference to the form renderable form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form rebuild info, state and values.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity being edited by the form.
   * @param \Drupal\association\Entity\AssociationInterface $association
   *   The entity association to which this entity is a part of.
   */
  public function __construct($op, array &$form, FormStateInterface $form_state, ContentEntityInterface $entity, AssociationInterface $association) {
    $this->form = &$form;
    $this->formState = $form_state;
    $this->entity = $entity;
    $this->association = $association;

    $this->event = static::getEventNames()[$op] ?? NULL;
    if (!$this->event) {
      $error = sprintf('AssociatedEntityFormEvent does not support the %s entity form operation', $op);
      throw new \InvalidArgumentException($error);
    }
  }

  /**
   * The event names supported by this event.
   *
   * @return string[]
   *   Get event names this event supports keyed by the entity operations which
   *   map to the event name.
   */
  public static function getEventNames(): array {
    return [
      'add' => AssociationEvents::INSERT_ASSOCIATED_FORM_ALTER,
      'edit' => AssociationEvents::UPDATE_ASSOCIATED_FORM_ALTER,
      'delete' => AssociationEvents::DELETE_ASSOCIATED_FORM_ALTER,
    ];
  }

  /**
   * The name of the event that this event triggers.
   *
   * @return string
   *   Get the event name identifier.
   */
  public function getEventName(): string {
    return $this->event;
  }

  /**
   * Gets the entity the form operation is targeting.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   Get the content entity being operated on.
   */
  public function getEntity(): ContentEntityInterface {
    return $this->entity;
  }

  /**
   * Get the association this content entity belongs to.
   *
   * @return \Drupal\association\Entity\AssociationInterface
   *   The entity assocation owning this content.
   */
  public function getAssociation(): AssociationInterface {
    return $this->association;
  }

  /**
   * Get a reference to the renderable form elements.
   *
   * @return array
   *   A reference to the $form.
   */
  public function &getForm(): array {
    return $this->form;
  }

  /**
   * The form state.
   *
   * @return \Drupal\Core\Form\FormStateInterface
   *   The form state.
   */
  public function getFormState(): FormStateInterface {
    return $this->formState;
  }

}
