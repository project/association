<?php

namespace Drupal\association\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\association\Entity\AssociationInterface;

/**
 * Event for altering the entity updaters run during content updating.
 */
class AssociatedEntityUpdaterAlterEvent extends Event {

  /**
   * List of fully scoped EntityUpdater classes to apply to associated content.
   *
   * @var string[]
   */
  protected array $updaters;

  /**
   * Association entity being updated.
   *
   * @var \Drupal\association\Entity\AssociationInterface
   */
  protected AssociationInterface $association;

  /**
   * Creates a new instance of the AssociatedEntityUpdaterAlterEvent class.
   *
   * @param string[] $updaters
   *   Captures a reference to the updaters fully scoped class names.
   * @param \Drupal\association\Entity\AssociationInterface $association
   *   The association linked to the content entities being updated.
   */
  public function __construct(array &$updaters, AssociationInterface $association) {
    $this->updaters = &$updaters;
    $this->association = $association;
  }

  /**
   * Alterable reference to the list of updater fully scoped updater classes.
   *
   * @return string[]
   *   Reference to the list of EntityUpdater classes to use to update the
   *   linked entity.
   */
  public function &getUpdaters(): array {
    return $this->updaters;
  }

  /**
   * Get the association linked to the content that will be updated.
   *
   * @return \Drupal\association\Entity\AssociationInterface
   *   The association linked to the content to get updated.
   */
  public function getAssociation(): AssociationInterface {
    return $this->association;
  }

}
