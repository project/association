<?php

namespace Drupal\association\Event;

/**
 * Defines events for the entity association module.
 */
final class AssociationEvents {

  /**
   * Name of the event fired when building an associated entity create form.
   *
   * This event allows modules to alter the entity form whenever an associated
   * entity create form operation is being built. The event listener method
   * receives a \Drupal\association\Event\AssociatedEntityFormEvent instance.
   *
   * @Event
   *
   * @var string
   */
  const INSERT_ASSOCIATED_FORM_ALTER = 'association.insert_associated';

  /**
   * Name of the event fired when building an associated entity update form.
   *
   * This event allows modules to alter the entity form whenever an associated
   * entity update form operation is being built. The event listener method
   * receives a \Drupal\association\Event\AssociatedEntityFormEvent instance.
   *
   * @Event
   *
   * @var string
   */
  const UPDATE_ASSOCIATED_FORM_ALTER = 'association.update_associated';

  /**
   * Name of the event fired when building an associated entity delete form.
   *
   * This event allows modules to alter the entity form whenever an associated
   * entity form delete operation is being confirmed. The event listener method
   * receives a \Drupal\association\Event\AssociatedEntityFormEvent instance.
   *
   * @Event
   *
   * @var string
   */
  const DELETE_ASSOCIATED_FORM_ALTER = 'association.delete_associated';

  /**
   * Name of the event fired when finding entity updaters for content updates.
   *
   * This event allows modules to alter the list of entity updaters which are
   * run on associated content when the association is updated. These
   * ensure that the associate content stays updated. The event listener method
   * receives a \Drupal\association\Event\AssociatedEntityUpdaterAlterEvent
   * instance.
   *
   * @Event
   *
   * @see \Drupal\association\EntityUpdater\EntityUpdaterInterface
   *
   * @var string
   */
  const ENTITY_UPDATER_ALTER = 'association.entity_updater_alter';

  /**
   * Name of the event fired when altering associated entity access queries.
   *
   * This event allows modules to alter the query condition applied to during
   * query alter for entity access. Implementers are able to alter the query
   * of just target the condition that will be added to the query. The event
   * listener method receives a
   * \Drupal\association\Event\QueryAcessAlterEvent instance.
   *
   * @Event
   *
   * @see \Drupal\association\Adapter\EntityAdapter::accessQueryAlter()
   *
   * @var string
   */
  const QUERY_ACCESS_ALTER = 'association.query_access_alter';

}
