<?php

namespace Drupal\association\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Database\Query\ConditionInterface;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Event for altering query access criteria for associated entities.
 */
class QueryAccessAlterEvent extends Event {

  /**
   * The entity operation the query being built for.
   *
   * @var string
   */
  protected string $operation;

  /**
   * Reference to the query condition which apply association access criteria.
   *
   * This is a reference to the original variable being worked on and can be
   * used to replace and alter the condition.
   *
   * This is important if it is currently NULL and access conditions need to
   * be added. In this case you replace it with a new condition group. Keep in
   * mind that in normal cases the expectation is that this is an "OR" group.
   *
   * @var \Drupal\Core\Database\Query\ConditionInterface|null
   */
  protected ?ConditionInterface $accessCondition;

  /**
   * The query being altered.
   *
   * @var \Drupal\Core\Database\Query\SelectInterface
   */
  protected SelectInterface $query;

  /**
   * The table aliases of the base table, associaton and association link.
   *
   * @var string[]
   */
  protected array $tableAliases;

  /**
   * The account to use for access permission and access checks.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $account;

  /**
   * Create a new instance of the AccessConditionAlterEvent class.
   *
   * @param string $operation
   *   The entity operation being performed.
   * @param \Drupal\Core\Database\Query\SelectInterface $query
   *   The query being altered.
   * @param \Drupal\Core\Database\Query\ConditionInterface|null $condition
   *   A reference to the query condition with the association access filters.
   * @param string[] $table_aliases
   *   The table aliases from the query for the entity base, association,
   *   and association link tables.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account to use for access and permission checks.
   */
  public function __construct($operation, SelectInterface $query, ?ConditionInterface &$condition, array $table_aliases, AccountInterface $account) {
    $this->operation = $operation;
    $this->query = $query;
    $this->accessCondition = &$condition;
    $this->tableAliases = $table_aliases;
    $this->account = $account;
  }

  /**
   * Gets the a reference to the access condition variable.
   *
   * This returns a reference to the condition variable that will be applied
   * to the query. This means that it's possible to completely replace this
   * or just add to it.
   *
   * Condition can be NULL, if there are currently no constraints, so adding
   * additional query conditions requires creating a new condition group first,
   * and replacing the condition.
   *
   * Keep in mind that the expectation is that this is an "OR" group.
   *
   * @return \Drupal\Core\Database\Query\ConditionInterface|null
   *   Get the current access condition with the association based access
   *   constraints applied. This can be NULL if there are currently no
   *   query constraints needed.
   */
  public function &getCondition(): ?ConditionInterface {
    return $this->accessCondition;
  }

  /**
   * The associated entity query being altered.
   *
   * It is recommend in most cases to only alter the condition and not to
   * change the query directly.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface
   *   The query being altered.
   */
  public function getQuery(): SelectInterface {
    return $this->query;
  }

  /**
   * Gets the entity operation being performed.
   *
   * @return string
   *   The entity operation being performed.
   */
  public function getOperation(): string {
    return $this->operation;
  }

  /**
   * Gets the table aliases of the entity base and association tables.
   *
   * @return string[]
   *   The table aliaes keyed by "base" (base entity table), "association"
   *   (association field data table), and "association_link" (association
   *   link base table).
   *
   * @see \Drupal\association\Adapte\AssociatedQueryAlterTrait::ensureTable()
   */
  public function getTableAliases(): array {
    return $this->tableAliases;
  }

  /**
   * Get the account to use for permission and access checks.
   *
   * @return \Drupal\Core\Session\AccountInterface
   *   Gets the account to compare permission and access with.
   */
  public function getAccount(): AccountInterface {
    return $this->account;
  }

}
