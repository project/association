<?php

namespace Drupal\association\EventSubscriber;

use Drupal\Core\Config\ConfigImportValidateEventSubscriberBase;
use Drupal\Core\Config\ConfigImporterEvent;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Validate association behavior settings changes to ensure safe changes.
 */
class ConfigValidationEventSubscriber extends ConfigImportValidateEventSubscriberBase {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Create a new instance of the ConfigValidationEvent subscriber.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function onConfigImporterValidate(ConfigImporterEvent $event): void {
    $configImporter = $event->getConfigImporter();
    $storageComparer = $configImporter->getStorageComparer();

    // Only listen for changes. Entity validation takes care of the case where
    // the bundle has been deleted.
    foreach ($storageComparer->getChangelist('update') as $configName) {
      if (preg_match('/association.type.([a-z_]+)/', $configName, $matches)) {
        $changes = $storageComparer
          ->getSourceStorage()
          ->read($configName);

        /** @var \Drupal\association\Entity\AssociationTypeInterface */
        $associationType = $this->entityTypeManager
          ->getStorage('association_type')
          ->load($matches[1]);

        if ($associationType) {
          $errors = $associationType->validateConfigChanges($changes);

          foreach ($errors as $error) {
            $configImporter->logError($error);
          }
        }
      }
    }
  }

}
