<?php

namespace Drupal\association\Menu;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Menu\LocalTaskDefault;
use Drupal\Core\Menu\LocalTaskInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatch;
use Drupal\association\AssociationNegotiatorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a local task plugins for association routes.
 *
 * Association and association page entities will appear together in local
 * tasks, but because they are different entity types, they have different
 * route parameters, but can easily be translated between due to having the same
 * entity ID.
 *
 * This local task plugin manages that translation of route parameters so the
 * two entities can exist in the same local task scope (tabs).
 */
class AssociatedEntityLocalTask extends LocalTaskDefault implements LocalTaskInterface, ContainerFactoryPluginInterface {

  /**
   * Negotiator to determine the active association from the route.
   *
   * @var \Drupal\association\AssociationNegotiatorInterface
   */
  protected AssociationNegotiatorInterface $associationNegotiator;

  /**
   * Create a new instance of the AssociatedEntityLocalTask class.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin identifier string.
   * @param mixed $plugin_definition
   *   The plugin deifnition.
   * @param \Drupal\association\AssociationNegotiatorInterface $association_negotiator
   *   The association negotiator which determines which association is active
   *   based on the current route.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AssociationNegotiatorInterface $association_negotiator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->associationNegotiator = $association_negotiator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('association.negotiator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts(): array {
    return Cache::mergeContexts(parent::getCacheContexts(), [
      'route.association',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(?Request $request = NULL): string {
    $routeMatch = RouteMatch::createFromRequest($request);

    // Determine the association type, and get the label to use as task label.
    $paramName = $routeMatch->getParameter('association_entity_type');

    if ($paramName) {
      $entity = $routeMatch->getParameter($paramName);
      $assoc = $entity ? $this->associationNegotiator->byEntity($entity) : NULL;
    }
    else {
      $assoc = $this->associationNegotiator->byRoute($routeMatch);
    }

    return $assoc ? $assoc->getType()->getTaskTitle() : parent::getTitle($request);
  }

}
