<?php

namespace Drupal\association\Menu;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Menu\LocalTaskDefault;
use Drupal\Core\Menu\LocalTaskInterface;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a local task plugins for association routes.
 *
 * Association and association page entities will appear together in local
 * tasks, but because they are different entity types, they have different
 * route parameters, but can easily be translated between due to having the same
 * entity ID.
 *
 * This local task plugin manages that translation of route parameters so the
 * two entities can exist in the same local task scope (tabs).
 */
class AssociationLocalTask extends LocalTaskDefault implements LocalTaskInterface {

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts(): array {
    return Cache::mergeContexts(parent::getCacheContexts(), [
      'route.association_type',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(?Request $request = NULL): string {
    if ($this->getPluginId() === 'entity.association.manage') {
      $routeMatch = RouteMatch::createFromRequest($request);

      // Determine the association type, and get the label to use as task label.
      if ($assoc = $routeMatch->getParameter('association') ?: $routeMatch->getParameter('association_page')) {
        return $assoc->getType()->getTaskTitle();
      }
    }

    return parent::getTitle($request);
  }

  /**
   * {@inheritdoc}
   */
  public function getRouteParameters(RouteMatchInterface $route_match): array {
    $route_parameters = $this->pluginDefinition['route_parameters'] ?? [];

    $route = $this->routeProvider()->getRouteByName($this->getRouteName());
    $variables = $route->compile()->getVariables();

    $raw_parameters = $route_match->getRawParameters();
    $parameters = $route_match->getParameters();

    foreach ($variables as $name) {
      if (isset($route_parameters[$name])) {
        continue;
      }

      if ($raw_parameters->has($name)) {
        $route_parameters[$name] = $raw_parameters->get($name);
      }
      elseif ($parameters->has($name)) {
        $route_parameters[$name] = $parameters->get($name);
      }
      elseif ($name === 'association_page' && $raw_parameters->has('association')) {
        $route_parameters[$name] = $raw_parameters->get('association');
      }
      elseif ($name === 'association' && $raw_parameters->has('association_page')) {
        $route_parameters[$name] = $raw_parameters->get('association_page');
      }
    }

    return $route_parameters;
  }

}
