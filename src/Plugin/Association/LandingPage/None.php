<?php

namespace Drupal\association\Plugin\Association\LandingPage;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\association\Attribute\AssociationLandingPage;
use Drupal\association\Entity\AssociationInterface;
use Drupal\association\Plugin\LandingPagePluginBase;
use Drupal\association\Plugin\LandingPagePluginInterface;

/**
 * Plugin for associations which have no landing pages.
 */
#[AssociationLandingPage(
  id: 'none',
  label: new TranslatableMarkup('No landing page'),
  description: new TranslatableMarkup('Associations which have no landing page.'),
)]
class None extends LandingPagePluginBase implements LandingPagePluginInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getPage(AssociationInterface $association): ?EntityInterface {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getPageUrl(AssociationInterface $association, ?EntityInterface $page): Url {
    return Url::fromRoute('<nolink>');
  }

}
