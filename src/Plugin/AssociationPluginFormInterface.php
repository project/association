<?php

namespace Drupal\association\Plugin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\association\Entity\AssociationTypeInterface;

/**
 * Extends the PluginFormInterface to add a association_type parameter.
 *
 * Extends the PluginFormInterface to add an association_type parameter to the
 * PluginFormInterface::buildConfigurationForm() method. This allows better
 * type hinting, and clarifies that behavior configuration forms expect to have
 * an association type parameter.
 */
interface AssociationPluginFormInterface extends PluginFormInterface {

  /**
   * Builds the association behavior configuration form.
   *
   * @param array $form
   *   The form elements and structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state, values and build information.
   * @param \Drupal\association\Entity\AssociationTypeInterface|null $association_type
   *   The association type entity which this behavior is being configured for.
   *
   * @return array
   *   Form API elements to configure behavior plugin for this association type.
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, ?AssociationTypeInterface $association_type = NULL): array;

}
