<?php

namespace Drupal\association\Plugin;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\association\Entity\AssociationInterface;
use Drupal\association\Entity\AssociationLink;
use Drupal\association\EntityAdapterManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Default base class for association behavior plugins.
 *
 * @see \Drupal\assocation\Plugin\BehaviorBase
 */
abstract class BehaviorBase extends PluginBase implements BehaviorInterface, ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Manager entity type adapters for associating to association entities.
   *
   * @var \Drupal\association\EntityAdapterManagerInterface
   */
  protected EntityAdapterManagerInterface $adapterManager;

  /**
   * Create a new instance of the ContentManifestBehavior plugin.
   *
   * @param array $configuration
   *   Plugin configuration options.
   * @param string $plugin_id
   *   The plugin identifier.
   * @param mixed $plugin_definition
   *   The plugin definition (from plugin discovery).
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\association\EntityAdapterManagerInterface $entity_adapter_manager
   *   Manager entity type adapters for associating to association entities.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityAdapterManagerInterface $entity_adapter_manager) {
    $configuration += $this->defaultConfiguration();

    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->adapterManager = $entity_adapter_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('strategy.manager.association.entity_adapter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getManagerBuilderClass(): ?string {
    return $this->pluginDefinition['manager_builder'];
  }

  /**
   * {@inheritdoc}
   */
  public function createAccess(AssociationInterface $association, string $tag, string $entity_type_id, string $bundle, AccountInterface $account): AccessResultInterface {
    $accessResult = $this->isValidEntity($tag, $entity_type_id, $bundle)
      ? AccessResult::allowed()
      : AccessResult::forbidden();

    return $accessResult->addCacheableDependency($association->getType());
  }

  /**
   * {@inheritdoc}
   */
  public function createEntity(AssociationInterface $association, string $tag, string $entity_type_id, string $bundle, array $values = []): ContentEntityInterface {
    if (!$this->isValidEntity($tag, $entity_type_id, $bundle)) {
      $error = sprintf('The bundle %s of entity type %s is not valid for tag %s.', $entity_type_id, $bundle, $tag);
      throw new \InvalidArgumentException($error);
    }

    // Create a new instance of the requested entity type and bundle.
    return $this->adapterManager
      ->getAdapterByEntityType($entity_type_id)
      ->createEntity($bundle, $values);
  }

  /**
   * {@inheritdoc}
   */
  public function alterLink(AssociationLink $link): self {
    return $this;
  }

  /**
   * Add the dependencies from the entity type and bundles.
   *
   * This is usually performed during the ::calculateDependencies() call of
   * behavior plugins so they can let Drupal know what entity configurations
   * they depend on. This helps to safe guard from missing configurations or
   * deletion of thing the plugin will rely on.
   *
   * @param array $dependencies
   *   A reference to the dependency array to add the entity dependencies to.
   * @param string $entityTypeId
   *   The entity type identifier for the entity being updated.
   * @param array $bundles
   *   An array of bundle names to add dependencies for. An empty array means
   *   to include all available entity bundles.
   *
   * @return self
   *   The plugin itself for method chaining.
   */
  public function addEntityTypeDependencies(array &$dependencies, string $entityTypeId, array $bundles = []): self {
    if ($entityDef = $this->entityTypeManager->getDefinition($entityTypeId, FALSE)) {
      $adapter = $this->adapterManager->getAdapterByEntityType($entityTypeId);
      $dependencies['module'][] = $entityDef->getProvider();

      foreach ($bundles ?: $adapter->getBundles() as $bundle) {
        ['type' => $dependType, 'name' => $dependName] = $entityDef->getBundleConfigDependency($bundle);
        if ('module' === $dependType && ('core' === $dependName || $entityDef->getProvider() === $dependName)) {
          continue;
        }
        $dependencies[$dependType][] = $dependName;
      }
    }

    return $this;
  }

}
