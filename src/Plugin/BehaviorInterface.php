<?php

namespace Drupal\association\Plugin;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\association\Entity\AssociationInterface;
use Drupal\association\Entity\AssociationLink;
use Drupal\association\Entity\AssociationTypeInterface;

/**
 * Interface for association behavior plugins.
 *
 * Ensures that behavior plugins implement a minimal expected interface for
 * supporting content entities linked to associations.
 *
 * Behaviors control what entity types can be associated and the constraints
 * or limits on them. This behavior includes entity access, admin UI for
 * content maintenance and allowed actions.
 */
interface BehaviorInterface extends PluginInspectionInterface, ConfigurableInterface {

  /**
   * Cardinality constant for when there is no limit on entities can be related.
   *
   * This constant matches \Drupal\Core\Field\FieldStorageDefinitionInterface
   * cardinality constant for unlimited cardinality.
   *
   * @var int
   */
  const CARDINALITY_UNLIMITED = FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED;

  /**
   * Get a list of all entity types being used with this behavior plugin.
   *
   * @return string[]
   *   An array of all the entity types configured to be used with this
   *   behavior. This allows link operations to fetch all associated entities
   *   by entity type for batch operations.
   */
  public function getEntityTypes(): array;

  /**
   * Gets an array of entity tags that are available for this behavior.
   *
   * @return \Stringable[]|string[]
   *   Array of available entity tags for the behavior. The array keys are the
   *   machine name of the tags, and the values are the tab labels.
   */
  public function getTags(): array;

  /**
   * Get a display label for a assocation link tag.
   *
   * An association link tag identifies a behaviors handling of link entity
   * content. This information at a minimum includes the entity type and bundle
   * but some behaviors can include more rules like # of items allowed, etc...
   *
   * @param string $tag
   *   The association link tag to generate a title for.
   * @param string $entity_type_id
   *   The machine name of the entity type the label is for.
   * @param string $bundle
   *   The machine name of the entity bundle to generate the label for.
   *
   * @return \Stringable|string
   *   The display label to go with this tag.
   */
  public function getTagLabel(string $tag, string $entity_type_id, string $bundle): \Stringable|string;

  /**
   * Gets the fully namespace class to use to build the entity association UI.
   *
   * This returns the name of the class to use for building the associating
   * entities page for this association behavior plugin. This page is
   * responsible for creating the UI to manage linked content entities.
   *
   * This method has the potential to return the class of current the behavior
   * plugin, and indicates that the plugin itself should be used directly.
   *
   * @return string|null
   *   The class to use to build the entity association UI and and implements
   *   \Drupal\association\Behavior\ManagerBuilderInterface.
   */
  public function getManagerBuilderClass();

  /**
   * Check if the tag, entity type and bundle are valid for this behavior.
   *
   * @param string $tag
   *   The behavior tag for this target entity.
   * @param string $entity_type_id
   *   The machine name of the entity type of the target entity.
   * @param string $bundle
   *   The machine name of the bundle of the target entity.
   *
   * @return bool
   *   TRUE if the tag is valid and the entity type and bundle combination
   *   are allowed for the specified association behavior tag.
   */
  public function isValidEntity(string $tag, string $entity_type_id, string $bundle): bool;

  /**
   * Determine if a user has access to create an associated entity.
   *
   * @param \Drupal\association\Entity\AssociationInterface $association
   *   The association that the target entity will be created for.
   * @param string $tag
   *   Behavior identifier for the content entity to create. The meaning of this
   *   tag is behavior defined, but is typically a nominal identifier for the
   *   entities intented usage.
   * @param string $entity_type_id
   *   The machine name of the entity type being tested for access.
   * @param string $bundle
   *   The machine name of the entity bundle.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Account to check permissions for.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Access result for if the target entity can be created by this account
   *   and for this association.
   */
  public function createAccess(AssociationInterface $association, string $tag, string $entity_type_id, string $bundle, AccountInterface $account): AccessResultInterface;

  /**
   * Create a new entity appropriate for this association behavior tag.
   *
   * @param \Drupal\association\Entity\AssociationInterface $association
   *   The association that the target entity will be created for.
   * @param string $tag
   *   Identifier for the target entity to create for this behavior.
   * @param string $entity_type_id
   *   The entity type machine name for the entity to create.
   * @param string $bundle
   *   The entity bundle machine name.
   * @param mixed[] $values
   *   The values to apply to the new entity instance.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   A newly created entity appropriate for the behavior tag.
   *
   * @throws \InvalidArgumentException
   *   Thrown if the "tag" is does not exist for this association type, or if
   *   the entity type and bundle do not match the required "tag".
   */
  public function createEntity(AssociationInterface $association, string $tag, string $entity_type_id, string $bundle, array $values = []): ContentEntityInterface;

  /**
   * Allow behaviors to alter the association links before they are saved.
   *
   * @param \Drupal\association\Entity\AssociationLink $link
   *   The association link to alter.
   *
   * @return self
   *   The behavior itself for method chaining.
   */
  public function alterLink(AssociationLink $link): self;

  /**
   * If a association type has data, validate the config updates are safe.
   *
   * This validation will only get called if the association type already has
   * data and needs to make sure the changes are safe for this behavior's
   * current state.
   *
   * @param \Drupal\association\Entity\AssociationTypeInterface $association_type
   *   The association type to validate configurations for.
   * @param array $changes
   *   The proposed configuration changes.
   *
   * @return string[]
   *   A list of validation errors with the proposed configuration updates.
   *   Will return an empty array if there are no errors.
   */
  public function validateConfigUpdate(AssociationTypeInterface $association_type, array $changes): array;

}
