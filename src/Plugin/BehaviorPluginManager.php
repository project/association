<?php

namespace Drupal\association\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\association\Annotation\AssociationBehavior as AnnotationAssociationBehavior;
use Drupal\association\Attribute\AssociationBehavior;

/**
 * The default plugin manager for association behaviors.
 *
 * @see \Drupal\association\Plugin\BehaviorInterface
 */
class BehaviorPluginManager extends DefaultPluginManager implements BehaviorPluginManagerInterface {

  /**
   * Create a new instance of the plugin manager for association behaviors.
   *
   * @param \Traversable $namespaces
   *   Namespaces available for Drupal to crawl for plugin discovery.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend to cache definitions and maps.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Association/Behavior',
      $namespaces,
      $module_handler,
      BehaviorInterface::class,
      AssociationBehavior::class,
      AnnotationAssociationBehavior::class
    );

    $this->alterInfo('association_behavior_info');
    $this->setCacheBackend($cache_backend, 'association.behavior_plugin');
  }

}
