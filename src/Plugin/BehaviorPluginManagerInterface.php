<?php

namespace Drupal\association\Plugin;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Interface for the association behavior plugin manager.
 *
 * Behavior plugins are used to setup business logic for managing linked content
 * entities. Behavior plugins are set at the association type (bundle).
 */
interface BehaviorPluginManagerInterface extends PluginManagerInterface {
}
