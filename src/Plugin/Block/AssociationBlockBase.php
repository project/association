<?php

namespace Drupal\association\Plugin\Block;

use Drupal\Component\Plugin\Exception\ContextException;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\Context\ContextInterface;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\association\AssociationNegotiatorInterface;
use Drupal\association\Entity\AssociationInterface;
use Drupal\Core\Access\AccessResultInterface;

/**
 * Provides a block, based on the active association context.
 *
 * Recommended context definitions for derivative blocks:
 * @code
 * context_definitions = {
 *   "association" = EntityContextDefinition("entity:association", required = FALSE),
 *   "entity" = ContextDefinition("entity", required = FALSE),
 * }
 * @endcode
 *
 * With the expectation that the "entity" context will be for use with layout
 * builder or another system that provides an entity context.
 */
abstract class AssociationBlockBase extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The target association for this block.
   *
   * Property is NULL when not initialized, and will be FALSE, when initialized
   * but no valid association was available.
   *
   * @var \Drupal\association\Entity\AssociationInterface|false|null
   */
  protected AssociationInterface|false|null $association = NULL;

  /**
   * The plugin context to use as the association context.
   *
   * Context is NULL if it hasn't been initialized yet, and FALSE if a valid
   * context for this block is not available.
   *
   * @var \Drupal\Core\Plugin\Context\ContextInterface|false|null
   */
  protected ContextInterface|false|null $associationContext = NULL;

  /**
   * The negotiator to determine the active association context of the block.
   *
   * @var \Drupal\association\AssociationNegotiatorInterface
   */
  protected AssociationNegotiatorInterface $associationNegotiator;

  /**
   * Create an instance of the AssociationBlock plugin.
   *
   * @param array $configuration
   *   The block configuration.
   * @param string $plugin_id
   *   The unique identifier for this plugin.
   * @param mixed $plugin_definition
   *   The plugin definition from discovery or a deriver.
   * @param \Drupal\association\AssociationNegotiatorInterface $association_negotiator
   *   The negotiator to determine the active association context of the block.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AssociationNegotiatorInterface $association_negotiator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->associationNegotiator = $association_negotiator;
  }

  /**
   * Get the names of plugin contexts that provides the association.
   *
   * To support layout builder and block layout (global) usage of association
   * based blocks, we may need to define multiple plugin context and prefer
   * them in different situations. These should be context names defined
   * in the order of priority of use (first one available gets used).
   *
   * The value could be provided indirectly, through a entity that the
   * association negotiator can use to resolve the target association.
   *
   * @return string[]
   *   Names of plugin contexts that can provided the association value.
   */
  abstract protected function getAssociationContextNames(): array;

  /**
   * Get the plugin context that should be used to find the target association.
   *
   * @return \Drupal\Core\Plugin\Context\ContextInterface|null
   *   The context that should be used to find the target association. NULL if
   *   no valid context is available.
   */
  protected function getAssociationContext(): ?ContextInterface {
    if (!isset($this->associationContext)) {
      $this->associationContext = FALSE;

      foreach ($this->getAssociationContextNames() as $name) {
        try {
          $context = $this->getContext($name);
          if ($context->hasContextValue()) {
            $this->associationContext = $context;
            break;
          }
        }
        catch (ContextException $e) {
          // Skip this context as it was not defined by the plugin.
        }
      }
    }

    return $this->associationContext ?: NULL;
  }

  /**
   * Get the target association to use for this block plugin.
   *
   * @return \Drupal\association\Entity\AssociationInterface|null
   *   The target association if one can be determined, NULL otherwise.
   */
  protected function getAssociation(): ?AssociationInterface {
    if (!isset($this->association)) {
      $this->association = FALSE;
      $context = $this->getAssociationContext();

      if ($context && ($entity = $context->getContextValue())) {
        try {
          if ($entity instanceof AssociationInterface) {
            $this->association = $entity;
          }
          else {
            $this->association = $this->associationNegotiator->byEntity($entity) ?: FALSE;
          }
        }
        catch (\TypeError $e) {
          // Context value is not \Drupal\Core\Entity\EntityInterface type.
        }
      }
    }

    return $this->association ?: NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function addContextAssignmentElement(ContextAwarePluginInterface $plugin, array $contexts): array {
    $element = parent::addContextAssignmentElement($plugin, $contexts);
    $assocContexts = $this->getAssociationContextNames();

    // Ensure that when the layout builder context is available that it is
    // being used for the designated context. Otherwise the context mapping
    // fields can be confusing and provide too many invalid options.
    if (!empty($assocContexts['layout_builder'])) {
      $lbContext = $assocContexts['layout_builder'];
      $globalContext = $assocContexts['global'] ?? NULL;

      if (isset($contexts['layout_builder.entity']) && isset($element[$lbContext]['#options']['layout_builder.entity'])) {
        $element[$lbContext]['#type'] = 'value';
        $element[$lbContext]['#value'] = 'layout_builder.entity';

        if ($lbContext !== $globalContext && isset($element[$globalContext])) {
          try {
            $contextDef = $this->getContext($globalContext)->getContextDefinition();
            if (!$contextDef->isRequired()) {
              $element[$globalContext]['#value'] = '';
              $element[$globalContext]['#access'] = FALSE;
            }
          }
          catch (ContextException $e) {
            // Should theoretically not happen, $element[$globalContext] exists.
            $element[$globalContext]['#value'] = '';
            $element[$globalContext]['#access'] = FALSE;
          }
        }
      }
      else {
        // Non-layout_builder context, clear this value so it doesn't interfere
        // with the global context accidentally.
        if ($lbContext !== $globalContext) {
          $element[$lbContext]['#value'] = '';
          $element[$lbContext]['#access'] = FALSE;
        }

        // Ensure that the global context is required to ensure that the block
        // will be created with an association.
        if ($globalContext && !empty($element[$globalContext]['#options'])) {
          $element[$globalContext]['#required'] = TRUE;

          if (count($element[$globalContext]['#options']) === 1) {
            $element[$globalContext]['#value'] = array_key_first($element[$globalContext]['#options']);
            $element[$globalContext]['#access'] = FALSE;
          }
        }
      }
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function blockAccess(AccountInterface $account): AccessResultInterface {
    // Ensure a valid association is available and user has view access.
    if ($assoc = $this->getAssociation()) {
      return $assoc->access('view', $account, TRUE);
    }

    // No association value available, block is not available.
    return AccessResult::forbidden();
  }

}
