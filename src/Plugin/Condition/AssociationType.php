<?php

namespace Drupal\association\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\Context\EntityContextDefinition;
use Drupal\association\AssociationNegotiatorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Entity Association Type' condition.
 *
 * @Condition(
 *   id = "association_type",
 *   label = @Translation("Entity Association types"),
 *   context_definitions = {
 *     "association" = @ContextDefinition("entity:association", required = FALSE, label = @Translation("Association"))
 *   }
 * )
 */
class AssociationType extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The association negotiator.
   *
   * @var \Drupal\association\AssociationNegotiatorInterface
   */
  protected AssociationNegotiatorInterface $assocNegotiator;

  /**
   * The entity type bundle information.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected EntityTypeBundleInfoInterface $entityBundleInfo;

  /**
   * Creates a new AssociationType condition plugin instance.
   *
   * @param array $configuration
   *   The plugin configuration, which should contain which association types
   *   are configured for this condition.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle information.
   * @param \Drupal\association\AssociationNegotiatorInterface $association_negotiator
   *   The association negotiator.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeBundleInfoInterface $entity_type_bundle_info, AssociationNegotiatorInterface $association_negotiator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    // It is possible to determine an active association from another entity,
    // and for the ability to support conditions with pathauto, we need to
    // dynamically alter the contexts this plugin uses. This allows the entity
    // context to be injected by the initializing code.
    //
    // @todo better way to define and accept a entity context.
    if (!empty($configuration['entity_type_id'])) {
      $entityTypeId = $configuration['entity_type_id'];
      $contextDef = EntityContextDefinition::fromEntityTypeId($entityTypeId)
        ->setRequired(TRUE);

      $this->pluginDefinition['context_definitions'][$entityTypeId] = $contextDef;
      $this->pluginDefinition['context'][$entityTypeId] = $contextDef;
    }

    $this->assocNegotiator = $association_negotiator;
    $this->entityBundleInfo = $entity_type_bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.bundle.info'),
      $container->get('association.negotiator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'entity_type_id' => NULL,
      'bundles' => [],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $options = [];

    // Build a list of group type labels.
    $bundles = $this->entityBundleInfo->getBundleInfo('association');
    foreach ($bundles as $bundleId => $bundle) {
      $options[$bundleId] = $bundle['label'];
    }

    $form['mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Applies to'),
      '#options' => [
        0 => $this->t('All non-association contexts and association contexts of selected bundles'),
        1 => $this->t('Only association contexts (non-association contexts evaluate to FALSE)'),
      ],
      '#default_value' => $this->configuration['mode'] ?? 0,
    ];

    $form['bundles'] = [
      '#title' => $this->t('Association types'),
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => $this->configuration['bundles'],
    ];

    // The context mapping is required although an association isn't required.
    // The context_definition['association'] needs to not be required, but
    // the configuration needs to behave as if a context is required.
    if (!empty($form['context_mapping']['association'])) {
      $contextMapping = &$form['context_mapping']['association'];
      $contextMapping['#required'] = TRUE;
      unset($contextMapping['#empty_value']);

      if (count($contextMapping['#options']) === 1) {
        reset($contextMapping['#options']);
        $contextMapping['#default_value'] = key($contextMapping['#options']);
        $contextMapping['#access'] = FALSE;
      }
    }

    $form['negate']['#weight'] = 50;
    $form['context_mapping']['#weight'] = 60;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['bundles'] = array_filter($form_state->getValue('bundles'));

    // Unset this configuration so block visibility tests will consider this
    // condition empty if no bundles are selected. This saves us loading and
    // evaluating this condition on blocks or other services that ignore empty
    // condition configurations - but still will work correctly even if
    // initialized anyway "google_tag" module for example evaluates all
    // conditions even when configurations are empty.
    if (empty($form_state->getValue('mode'))) {
      unset($this->configuration['mode']);
    }
    else {
      $this->configuration['mode'] = 1;
    }

    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function summary(): \Stringable|string {
    $bundles = $this->configuration['bundles'];

    if ($bundles) {
      $tParams = [];
      $tParams['@bundles'] = implode(', ', $bundles);

      if (empty($this->configuration['mode'])) {
        return $this->t('Any non-associated content and only associated content from types: @bundles.', $tParams);
      }

      return $this->t('The only associated content from types: @bundles.', $tParams);
    }
    elseif (!empty($this->configuration['mode'])) {
      return $this->t('Any associated content');
    }

    return $this->t('Not restricted');
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(): bool {
    $config = $this->getConfiguration();

    if (!empty($config['entity_type_id'])) {
      $entity = $this->getContextValue($config['entity_type_id']);
      $assoc = $entity ? $this->assocNegotiator->byEntity($entity) : NULL;
    }
    else {
      $assoc = $this->getContextValue('association');
    }

    if ($assoc) {
      $bundles = $config['bundles'];

      // If empty, then any bundle is valid and just having an association is
      // enough, otherwise check for a valid association bundle.
      return empty($bundles) || !empty($bundles[$assoc->bundle()]);
    }

    return empty($config['mode']);
  }

}
