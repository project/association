<?php

namespace Drupal\association\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\association\EntityAdapterManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Create associated entity local tasks for quick access to owning association.
 */
class AssociatedEntityLocalTaskDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The ID of the plugin the deriver is implementing.
   *
   * @var string
   */
  protected string $basePluginId;

  /**
   * The route provider.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected RouteProviderInterface $routeProvider;

  /**
   * Plugin manager for entity adapters.
   *
   * @var \Drupal\association\EntityAdapterManagerInterface
   */
  protected EntityAdapterManagerInterface $entityAdapterManager;

  /**
   * Create a new local task menu deriver for associated entities.
   *
   * @param string $base_plugin_id
   *   The plugin ID of the deriver definition.
   * @param \Drupal\Core\Routing\RouteProviderInterface $route_provider
   *   The route provider.
   * @param \Drupal\association\EntityAdapterManagerInterface $entity_adapter_manager
   *   Plugin manager for entity adapters.
   */
  public function __construct(string $base_plugin_id, RouteProviderInterface $route_provider, EntityAdapterManagerInterface $entity_adapter_manager) {
    $this->basePluginId = $base_plugin_id;
    $this->routeProvider = $route_provider;
    $this->entityAdapterManager = $entity_adapter_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id): static {
    return new static(
      $base_plugin_id,
      $container->get('router.route_provider'),
      $container->get('strategy.manager.association.entity_adapter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    $base = $base_plugin_definition;
    unset($base['deriver']);

    // Find create potential routes for all associable types, and test if
    // these routes were actually created by the association route subscriber.
    //
    // @see \Drupal\association\Routing\AssociationRouteSubscriber.
    foreach ($this->entityAdapterManager->getEntityTypes() as $entityTypeId) {
      $routes[$entityTypeId] = "association.linked_entity.{$entityTypeId}.manage";
    }

    if (!empty($routes)) {
      $found = $this->routeProvider->getRoutesByNames($routes);

      // For each of the existing routes, create a local task linking to the
      // canonical link entity link.
      foreach (array_intersect($routes, array_keys($found)) as $entityTypeId => $routeName) {
        $baseRoute = "entity.{$entityTypeId}.canonical";

        $this->derivatives[$baseRoute] = [
          'title' => 'Association',
          'route_name' => $routeName,
          'base_route' => $baseRoute,
        ] + $base;
      }
    }

    return $this->derivatives;
  }

}
