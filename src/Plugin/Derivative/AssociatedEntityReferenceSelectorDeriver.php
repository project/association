<?php

namespace Drupal\association\Plugin\Derivative;

use Drupal\association\EntityAdapterManagerInterface;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Deriver to generate reference selection plugins for associated entities.
 *
 * The entity reference selection manager allows the use of dynamic selection
 * settings based on the derivative name, and assumes the entity type identifier
 * is in the derived plugin name "associated_entity:<entity_type>" and some
 * plugin specific configurations don't work correctly if the naming does not
 * fit this convention.
 *
 * @see \Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceField::fieldSettingsForm()
 */
class AssociatedEntityReferenceSelectorDeriver extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * Create a new instance of the AssociatedEntityReferenceSelectorDeriver.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\association\EntityAdapterManagerInterface $adapterManager
   *   The association entity adapter manager.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected EntityAdapterManagerInterface $adapterManager,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id): static {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('strategy.manager.association.entity_adapter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $types = $this->adapterManager->getEntityTypes();
    $types[] = 'association_page';

    foreach ($types as $entityTypeId) {
      $entityType = $this->entityTypeManager->getDefinition($entityTypeId, FALSE);

      if ($entityType && $entityType->getKey('label')) {
        $this->derivatives[$entityTypeId] = $base_plugin_definition;
        $this->derivatives[$entityTypeId]['entity_types'] = [$entityTypeId];
        $this->derivatives[$entityTypeId]['base_plugin_label'] = (string) $base_plugin_definition['label'];
        $this->derivatives[$entityTypeId]['label'] = $this->t('@entity_type selection', ['@entity_type' => $entityType->getLabel()]);
      }
    }

    return $this->derivatives;
  }

}
