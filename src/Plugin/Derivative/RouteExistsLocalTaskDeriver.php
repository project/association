<?php

namespace Drupal\association\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Conditionally create menu local task based on if the route name exists.
 *
 * This allows the conditional creation of local task items if the route they
 * depend on exists. Useful for situations a local task is only available when
 * optional modules provides a path.
 */
class RouteExistsLocalTaskDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The ID of the plugin the deriver is implementing.
   *
   * @var string
   */
  protected string $basePluginId;

  /**
   * The route provider.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected RouteProviderInterface $routeProvider;

  /**
   * Create a new link object deriver.
   *
   * @param string $base_plugin_id
   *   The plugin ID of the deriver definition.
   * @param \Drupal\Core\Routing\RouteProviderInterface $route_provider
   *   The route provider.
   */
  public function __construct(string $base_plugin_id, RouteProviderInterface $route_provider) {
    $this->basePluginId = $base_plugin_id;
    $this->routeProvider = $route_provider;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id): static {
    return new static(
      $base_plugin_id,
      $container->get('router.route_provider')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    $definition = $base_plugin_definition;
    $routeName = $definition['route_name'];
    unset($definition['deriver']);

    // Only add the local task if the required routes exist.
    if ($this->routeProvider->getRoutesByNames([$routeName])) {
      $this->derivatives[$routeName] = $definition;
    }

    return $this->derivatives;
  }

}
