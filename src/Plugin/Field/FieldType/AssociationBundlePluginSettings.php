<?php

namespace Drupal\association\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\MapDataDefinition;

/**
 * Provides configuration storage for an association bundle plugin.
 *
 * This provides storage for plugin configurations. These settings can be
 * different per association entity, but the plugin type is initialized and
 * available from the association bundle.
 *
 * @FieldType(
 *   id = "association_plugin_settings",
 *   label = @Translation("Association plugin settings"),
 *   module = "association",
 *   description = @Translation("Association settings for an association bundle plugin."),
 *   default_widget = "association_plugin_settings_widget",
 *   cardinality = 1,
 *   no_ui = TRUE,
 * )
 */
class AssociationBundlePluginSettings extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName(): ?string {
    return 'settings';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    $properties = [];

    $properties['plugin'] = DataDefinition::create('string')
      ->setLabel(t('Plugin ID'))
      ->setRequired(TRUE)
      ->setDescription(t('The identifier of the plugin these configurations were stored for.'));

    $properties['settings'] = MapDataDefinition::create('map')
      ->setLabel(t('Landing page settings'))
      ->setDescription(t('The association landing page handler plugin settings.'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    return [
      'columns' => [
        'plugin' => [
          'type' => 'varchar_ascii',
          'length' => 128,
          'not null' => TRUE,
          'description' => 'The plugin identifier.',
        ],
        'settings' => [
          'type' => 'blob',
          'serialize' => TRUE,
          'description' => 'Serialized plugin settings.',
        ],
      ],
    ];
  }

}
