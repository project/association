<?php

namespace Drupal\association\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\association\Entity\AssociationInterface;
use Drupal\association\Plugin\PluginFieldSettingsInterface;

/**
 * Widget for setting the entity plugin settings fields.
 *
 * @FieldWidget(
 *   id = "association_plugin_settings_widget",
 *   label = @Translation("Plugin settings"),
 *   multiple_values = true,
 *   field_types = {
 *     "association_plugin_settings",
 *   }
 * )
 *
 * @see \Drupal\association\Plugin\Validation\Constraint\PluginFieldSettingsContraint
 */
class AssociationBundlePluginSettingsWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function extractFormValues(FieldItemListInterface $items, array $form, FormStateInterface $form_state): void {
    $field_name = $this->fieldDefinition->getName();

    // Extract the values from $form_state->getValues().
    $path = array_merge($form['#parents'], [$field_name]);
    $key_exists = NULL;
    $values = $form_state->getValues();
    $values = NestedArray::getValue($values, $path, $key_exists);

    if ($key_exists) {
      /** @var \Drupal\association\Entity\AssociationInterface $entity */
      $entity = $items->getEntity();
      $pluginType = $this->getTargetPluginType();
      $plugin = $entity instanceof AssociationInterface ? $entity->getPlugin($pluginType) : NULL;

      // Give the landing page plugin process the submitted form values.
      if ($plugin instanceof PluginFieldSettingsInterface) {
        $values = $plugin->processValues($entity, $values, $form, $form_state);
      }

      // Let the widget massage the submitted values.
      $values = $this->massageFormValues($values, $form, $form_state);

      // Assign the values and remove the empty ones.
      $items->setValue($values);
      $items->filterEmptyItems();

      // Put delta mapping in $form_state, so that flagErrors() can use it.
      $field_state = static::getWidgetState($form['#parents'], $field_name, $form_state);
      foreach ($items as $delta => $item) {
        $field_state['original_deltas'][$delta] = $delta;
      }
      static::setWidgetState($form['#parents'], $field_name, $form_state, $field_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function form(FieldItemListInterface $items, array &$form, FormStateInterface $form_state, $get_delta = NULL): array {
    $elements = parent::form($items, $form, $form_state, $get_delta);

    $entity = $items->getEntity();
    $pluginType = $this->getTargetPluginType();
    $plugin = $entity instanceof AssociationInterface ? $entity->getPlugin($pluginType) : NULL;

    // If the landing page plugin implements the field settings interface then
    // the widget can provide the form elements for the settings.
    if (!$plugin instanceof PluginFieldSettingsInterface) {
      $elements['widget']['#access'] = FALSE;
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    /** @var \Drupal\association\Entity\AssociationInterface $entity */
    $entity = $items->getEntity();
    $pluginType = $this->getTargetPluginType();
    $plugin = $entity instanceof AssociationInterface ? $entity->getPlugin($pluginType) : NULL;

    // If the landing page plugin implements the field settings interface then
    // the widget can provide the form elements for the settings.
    if ($plugin instanceof PluginFieldSettingsInterface) {
      $settings = [];
      if ($values = $items->get($delta)) {
        $settings = $values->settings ?? [];
      }

      $element[$delta]['settings'] = $plugin->buildSettingsForm($entity, $settings, $form_state);
    }
    else {
      $element[$delta]['settings'] = [
        '#type' => 'value',
        '#value' => [],
      ];
    }

    // Ensure that the plugin field is being populated with the current plugin
    // which is creating the plugin settings elements.
    $element[$delta]['plugin'] = [
      '#type' => 'value',
      '#value' => $plugin->getPluginId(),
    ];

    return $element;
  }

  /**
   * Get the association plugin type this widget is providing settings for.
   *
   * @return string|null
   *   Get the name of the association plugin type to configure.
   */
  protected function getTargetPluginType(): ?string {
    return $this->fieldDefinition
      ->getFieldStorageDefinition()
      ->getSetting('type');
  }

}
