<?php

namespace Drupal\association\Plugin;

use Drupal\Core\Plugin\PluginBase;
use Drupal\association\Entity\AssociationInterface;

/**
 * Base plugin class for landing page handler plugins.
 *
 * @see \Drupal\association\Plugin\LandingPagePluginInterface
 */
abstract class LandingPagePluginBase extends PluginBase implements LandingPagePluginInterface {

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function onCreate(AssociationInterface $association): void {
  }

  /**
   * {@inheritdoc}
   */
  public function onUpdate(AssociationInterface $association): void {
  }

  /**
   * {@inheritdoc}
   */
  public function onPreDelete(array $associations): void {
  }

  /**
   * {@inheritdoc}
   */
  public function onPostDelete(array $association): void {
  }

}
