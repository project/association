<?php

namespace Drupal\association\Plugin;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\association\Entity\AssociationInterface;

/**
 * Base plugin interface for landing page handler plugins.
 *
 * Landing page handlers help to configure, and resolve the landing pages for
 * an association entity. The landing page handler is configured and set for
 * the association type, but each association entity can have its own per
 * entity settings.
 */
interface LandingPagePluginInterface extends PluginInspectionInterface, ConfigurableInterface {

  /**
   * Get the URL to use for the landing page of the association.
   *
   * @param \Drupal\association\Entity\AssociationInterface $association
   *   The association to get the landing page URL for..
   * @param \Drupal\Core\Entity\EntityInterface|null $page
   *   The page entity to use to build the URL.
   *
   * @return \Drupal\Core\Url
   *   The URL to the landing page to use for the association. The URL can be
   *   a "<nolink>" URL.
   */
  public function getPageUrl(AssociationInterface $association, ?EntityInterface $page): Url;

  /**
   * Get the entity which is used as the association's landing page.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   Get the entity that should be used as the association's landing page. If
   *   the association doesn't have a page, this method returns NULL.
   */
  public function getPage(AssociationInterface $association): ?EntityInterface;

  /**
   * Create event handler for associations which have been created and saved.
   *
   * @param \Drupal\association\Entity\AssociationInterface $association
   *   The association which has been created and saved.
   */
  public function onCreate(AssociationInterface $association): void;

  /**
   * Update event handler for associations which have been updated.
   *
   * @param \Drupal\association\Entity\AssociationInterface $association
   *   The association which has been updated.
   */
  public function onUpdate(AssociationInterface $association): void;

  /**
   * Pre-delete event handler for associations about to be deleted.
   *
   * The pre-delete event is called by the entity storage handler before the
   * association entities are deleted. This allow the landing page handler to
   * make adjustments to the related pages before associations are deleted.
   *
   * @param \Drupal\association\Entity\AssociationInterface[] $associations
   *   Association entities which are being deleted.
   */
  public function onPreDelete(array $associations): void;

  /**
   * Post delete event handler for associations which have been deleted.
   *
   * @param \Drupal\association\Entity\AssociationInterface[] $associations
   *   The association entities which have been deleted.
   */
  public function onPostDelete(array $associations): void;

}
