<?php

namespace Drupal\association\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\association\Annotation\AssociationLandingPage as AnnotationAssociationLandingPage;
use Drupal\association\Attribute\AssociationLandingPage;

/**
 * The default plugin manager for association landing page plugins.
 *
 * @see \Drupal\association\Plugin\LandingPagePluginInterface
 */
class LandingPagePluginManager extends DefaultPluginManager implements LandingPagePluginManagerInterface {

  /**
   * Create a new instance of the plugin manager for association landing pages.
   *
   * @param \Traversable $namespaces
   *   Namespaces available for Drupal to crawl for plugin discovery.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend to cache definitions and maps.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Association/LandingPage',
      $namespaces,
      $module_handler,
      LandingPagePluginInterface::class,
      AssociationLandingPage::class,
      AnnotationAssociationLandingPage::class
    );

    $this->alterInfo('association_landing_page');
    $this->setCacheBackend($cache_backend, 'association.landing_page.handlers');
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackPluginId($plugin_id, array $configuration = []): string {
    return 'none';
  }

}
