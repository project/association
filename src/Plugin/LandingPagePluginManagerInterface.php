<?php

namespace Drupal\association\Plugin;

use Drupal\Component\Plugin\FallbackPluginManagerInterface;
use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Interface for service classes that manage association landing page plugins.
 */
interface LandingPagePluginManagerInterface extends PluginManagerInterface, FallbackPluginManagerInterface {
}
