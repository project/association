<?php

namespace Drupal\association\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\association\Entity\AssociationInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Interface for landing page plugins that have per entity settings.
 *
 * Association plugins which have per entity settings should implement this
 * interface. The settings are stored as a field for each entity.
 */
interface PluginFieldSettingsInterface extends PluginInspectionInterface {

  /**
   * Build per entity settings form elements for association plugin settings.
   *
   * @param \Drupal\association\Entity\AssociationInterface $association
   *   The association entity instance the plugin settings are for.
   * @param array $settings
   *   The current settings to use as defaults.
   * @param \Drupal\Core\Form\FormStateInterface|\Drupal\Core\Form\SubformStateInterface $form_state
   *   The plugin settings form state or subform state. The form state
   *   should be scoped to the plugin settings.
   *
   * @return array
   *   FAPI elements to use for configuring the association entity's landing
   *   page settings.
   */
  public function buildSettingsForm(AssociationInterface $association, array $settings, FormStateInterface $form_state): array;

  /**
   * Validate the association plugin settings form submission values.
   *
   * Because fields and entities are validated through constraints, this method
   * is expected to be called by a field constraint validator.
   *
   * @param \Drupal\association\Entity\AssociationInterface $association
   *   The association entity instance the landing page settings are for.
   * @param array $values
   *   Field submission values to validate.
   * @param \Symfony\Component\Validator\Constraint $constraint
   *   The field settings validator constraint definition.
   * @param \Symfony\Component\Validator\Context\ExecutionContextInterface $context
   *   The field settings validation context to log violations to.
   *
   * @see \Drupal\association\Plugin\Validation\Constraint\PluginFieldSettingsConstraintValidator
   */
  public function validateSettings(AssociationInterface $association, array $values, Constraint $constraint, ExecutionContextInterface $context): void;

  /**
   * Process the submitted form values into the format for settings storage.
   *
   * @param \Drupal\association\Entity\AssociationInterface $association
   *   The association entity instance the landing page settings are for.
   * @param array $values
   *   The submitted form values for these plugin settings.
   * @param array $form
   *   Reference to the FAPI elements for the plugin settings.
   * @param \Drupal\Core\Form\FormStateInterface|\Drupal\Core\Form\SubformStateInterface $form_state
   *   The landing page settings form state or subform state. The form state
   *   should be scoped to the plugin settings.
   *
   * @return array
   *   The submitted and processed association landing page settings for the
   *   target association entity.
   */
  public function processValues(AssociationInterface $association, array $values, array &$form, FormStateInterface $form_state): array;

}
