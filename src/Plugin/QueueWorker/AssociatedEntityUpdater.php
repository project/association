<?php

namespace Drupal\association\Plugin\QueueWorker;

use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueFactoryInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Utility\Error;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Worker for updating associated entity content after an association update.
 *
 * @QueueWorker(
 *   id = "association_linked_content_updater",
 *   title = @Translation("Update associated entity content."),
 *   cron = {
 *     "time" = 10,
 *   },
 * )
 */
class AssociatedEntityUpdater extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use LoggerChannelTrait;

  const BATCH_SIZE = 25;

  /**
   * Flag to indicate that updaters need the target entity to be saved.
   *
   * This allows multiple updaters to make changes to the target entity, and
   * only having a single Entity::save() call at the end.
   *
   * @var bool
   */
  protected bool $targetSave = FALSE;

  /**
   * The dependency injection class resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected ClassResolverInterface $classResolver;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The queue worker factory.
   *
   * Required still for D10 support when the interface wasn't available yet.
   *
   * @var \Drupal\Core\Queue\QueueFactory|\Drupal\Core\Queue\QueueFactoryInterface
   */
  protected QueueFactory|QueueFactoryInterface $queueFactory;

  /**
   * Creates a new instance of the AssociatedEntityUpdater worker queue.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin identifier.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   The dependency injection class resolver.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Queue\QueueFactory|\Drupal\Core\Queue\QueueFactoryInterface $queue_factory
   *   The queue worker factory.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ClassResolverInterface $class_resolver, EntityTypeManagerInterface $entity_type_manager, QueueFactory|QueueFactoryInterface $queue_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->classResolver = $class_resolver;
    $this->entityTypeManager = $entity_type_manager;
    $this->queueFactory = $queue_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('class_resolver'),
      $container->get('entity_type.manager'),
      $container->get('queue')
    );
  }

  /**
   * Process and filter the set of entity updaters to use when updating content.
   *
   * @param object $item
   *   The queue item data being worked on.
   *
   * @return \Drupal\association\EntityUpdater\EntityUpdaterInterface[]
   *   The list of entity updaters to use when updating associated content.
   */
  protected function processUpdaters($item): array {
    $this->targetSave = FALSE;

    $updaters = [];
    foreach ($item->updaters as $delta => $updaterClass) {
      try {
        $updater = $this->classResolver->getInstanceFromDefinition($updaterClass);
        $this->targetSave = $this->targetSave || $updater->requiresSave();
        $updaters[$delta] = $updater;
      }
      catch (\Exception $e) {
        // Unable to create the updater, skip using it.
      }
    }

    if ($this->targetSave) {
      foreach ($updaters as $key => $updater) {
        // Remove updates that are not necessary if an entity save will be
        // triggered when iterating content entities.
        if ($updater->skipIfSave()) {
          unset($updaters[$key]);
        }
      }
    }

    return $updaters;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($item): void {
    try {
      $updaters = $this->processUpdaters($item);

      if (!empty($updaters) || $this->targetSave) {
        $storage = $this->entityTypeManager->getStorage('association_link');
        $ids = $storage->getQuery()
          ->accessCheck(FALSE)
          ->condition('association', $item->association)
          ->condition('id', $item->current ?? 0, '>')
          ->sort('id', 'ASC')
          ->range(0, self::BATCH_SIZE)
          ->execute();

        if ($ids) {
          /** @var \Drupal\association\Entity\AssociationLink */
          foreach ($storage->loadMultiple($ids) as $link) {
            foreach ($updaters as $updater) {
              $updater->update($link->getTarget(), $link);
            }

            // Do any of the updaters need the target entity to save in order
            // to commit the updates. This is determined during the
            // static::processUpdaters() if it is needed or not based on
            // which updaters are being used.
            if ($this->targetSave) {
              $link->getTarget()->save();
            }

            // Update the progress of the worker, so if requeuing, it can
            // resume from this point.
            $item->current = $link->id();
          }

          // If there are more items to update, place another item on the queue
          // to address the remaining items. We can not requeue here, since
          // that only updates the expiration and will not keep progress of
          // what has and hasn't been updated.
          if (count($ids) >= self::BATCH_SIZE) {
            $this->queueFactory
              ->get($this->getPluginId())
              ->createItem($item);
          }
        }
      }
    }
    catch (\Exception $e) {
      Error::logException($this->getLogger('association_update'), $e);
    }
  }

}
