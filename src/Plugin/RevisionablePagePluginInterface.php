<?php

namespace Drupal\association\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Interface for landing page plugins that have a revisionable page entity.
 *
 * Allows the association_type to expose a new revision flag for landing pages
 * managed by this plugin.
 */
interface RevisionablePagePluginInterface extends PluginInspectionInterface {

  /**
   * Gets whether a new revision should be created by default for new pages.
   *
   * @return bool
   *   TRUE if a new revision should be created by default.
   *
   * @see \Drupal\Core\Entity\RevisionableEntityBundleInterface
   * @see \Drupal\association\Entity\AssociationType::shouldCreateNewRevision()
   */
  public function shouldCreateNewRevision(): bool;

}
