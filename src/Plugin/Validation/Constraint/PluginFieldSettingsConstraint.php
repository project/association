<?php

namespace Drupal\association\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Field constraint for validating association plugin settings fields.
 *
 * @Constraint(
 *   id = "AssociationPluginFieldSettings",
 *   label = @Translation("Bundle plugin field settings", context = "Validation"),
 * )
 */
class PluginFieldSettingsConstraint extends Constraint {

  /**
   * The base validation error message.
   *
   * @var string
   */
  public string $message = 'Settings for @type_label failed validation.';

  /**
   * The plugin type identifier to validate.
   *
   * @var string
   */
  public string $type;

  /**
   * {@inheritdoc}
   */
  public function validatedBy(): string {
    return PluginFieldSettingsConstraintValidator::class;
  }

}
