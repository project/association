<?php

namespace Drupal\association\Plugin\Validation\Constraint;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\association\Entity\AssociationInterface;
use Drupal\association\Plugin\PluginFieldSettingsInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Constraint to validate entity bundle plugin settings.
 *
 * Validates the plugin settings for an an association plugin field setting. It
 * ensures that the plugin has a chance to validate the configurations values
 * for the plugin instance and plugin type.
 */
class PluginFieldSettingsConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate(mixed $items, Constraint $constraint): void {
    if ($items instanceof FieldItemListInterface) {
      $entity = $items->getEntity();
      $fieldStorageDef = $items->getFieldDefinition()->getFieldStorageDefinition();
      $pluginType = $constraint->type ?? $fieldStorageDef->getSetting('type');

      if ($pluginType && $entity instanceof AssociationInterface) {
        $plugin = $entity->getPlugin($pluginType);

        if ($plugin instanceof PluginFieldSettingsInterface) {
          $plugin->validateSettings($entity, $items->settings ?? [], $constraint, $this->context);
        }
      }
    }
  }

}
