<?php

namespace Drupal\association\Plugin\search_api\processor;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\association\AssociationNegotiatorInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Search API processor to determine if associated entities should be indexed.
 *
 * @SearchApiProcessor(
 *   id = "association_entity_processor",
 *   label = @Translation("Associated content processor"),
 *   description = @Translation("Apply entity association status and settings to associated content indexing."),
 *   locked = true,
 *   hidden = true,
 *   stages = {
 *     "alter_items" = 0,
 *   },
 * )
 */
class AssociatedEntitiesProcessor extends ProcessorPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The association negotiator which finds the association of a content entity.
   *
   * @var \Drupal\association\AssociationNegotiatorInterface
   */
  protected AssociationNegotiatorInterface $assocNegotiator;

  /**
   * Creates a new AssociatedEntitiesProcessor class instance.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin machine name identifier.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\association\AssociationNegotiatorInterface $association_negotiator
   *   The association negotiator which finds the association of an entity.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AssociationNegotiatorInterface $association_negotiator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->assocNegotiator = $association_negotiator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('association.negotiator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function alterIndexedItems(array &$items): void {
    foreach ($items as $itemId => $item) {
      $object = $item->getOriginalObject()->getValue();

      if (!$object instanceof EntityInterface) {
        continue;
      }

      if ($association = $this->assocNegotiator->byEntity($object)) {
        $assocType = $association->getType();

        // Remove the item from search index if they are not from an
        // association type that allows its content to be searched, or if
        // the association is not active.
        if (!($assocType->isContentSearchable() && $association->isActive())) {
          unset($items[$itemId]);
        }
      }
    }
  }

}
