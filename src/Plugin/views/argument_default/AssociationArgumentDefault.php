<?php

namespace Drupal\association\Plugin\views\argument_default;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\association\AssociationNegotiatorInterface;
use Drupal\association\Entity\AssociationInterface;
use Drupal\Component\Plugin\Definition\PluginDefinitionInterface;
use Drupal\views\Plugin\views\argument_default\ArgumentDefaultPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Default argument plugin to extract a association entity from the route.
 *
 * @ViewsArgumentDefault(
 *   id = "assocation",
 *   title = @Translation("Association ID from the current route")
 * )
 */
class AssociationArgumentDefault extends ArgumentDefaultPluginBase implements CacheableDependencyInterface {

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * Association negotiator, for fetching the active association from the route.
   *
   * @var \Drupal\association\AssociationNegotiatorInterface
   */
  protected AssociationNegotiatorInterface $assocNegotiator;

  /**
   * Constructs a new AssociationArgumentDefault instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array|\Drupal\Component\Plugin\Definition\PluginDefinitionInterface $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   Route match object to use when searching for an association entity.
   * @param \Drupal\association\AssociationNegotiatorInterface $association_negotiator
   *   The association negotiator, which determines the active association from
   *   routes or from entities.
   */
  public function __construct(array $configuration, string $plugin_id, array|PluginDefinitionInterface $plugin_definition, RouteMatchInterface $route_match, AssociationNegotiatorInterface $association_negotiator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->routeMatch = $route_match;
    $this->assocNegotiator = $association_negotiator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('association.negotiator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getArgument() {
    $assoc = $this->assocNegotiator->byRoute($this->routeMatch);

    if ($assoc instanceof AssociationInterface) {
      return $assoc->id();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge(): int {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts(): array {
    return ['route.association'];
  }

}
