<?php

namespace Drupal\association\Routing;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Drupal\association\Controller\AssociationManagementController;
use Drupal\association\EntityAdapterManagerInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Alters Field UI and layout builder routes for associated entities.
 */
class AssociationRouteSubscriber extends RouteSubscriberBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity adapter manager.
   *
   * @var \Drupal\association\EntityAdapterManagerInterface
   */
  protected EntityAdapterManagerInterface $entityAdapterManager;

  /**
   * Constructs an AssociationRouteSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\association\EntityAdapterManagerInterface $entity_adapter_manager
   *   The entity adapter manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityAdapterManagerInterface $entity_adapter_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityAdapterManager = $entity_adapter_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    // Ensure that this happens alter event triggers after layout_builder (-110)
    // adds its routes.
    $events = [];
    $events[RoutingEvents::ALTER][] = ['onAlterRoutes', -200];

    return $events;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    // Add routes for each of the supported entity types to allow linked
    // entities to have a local tasks.
    foreach ($this->entityAdapterManager->getEntityTypes() as $entityTypeId) {
      $entityType = $this->entityTypeManager->getDefinition($entityTypeId);
      $entityRoute = $collection->get("entity.{$entityTypeId}.canonical");
      $linkTemplate = $entityType->getLinkTemplate('canonical');

      // Link doesn't have a canonical link template, or doesn't have a known
      // canonical entity route, we won't have what we need to generate an
      // route to the association management link.
      if (!($linkTemplate && $entityRoute)) {
        continue;
      }

      $route = new Route($linkTemplate . '/association');
      $route->addDefaults([
        '_title_callback' => AssociationManagementController::class . '::entityManageTitle',
        '_controller' => AssociationManagementController::class . '::entityManageContent',
        'association_entity_type' => $entityTypeId,
      ]);

      $requirements = [];
      $parameters = $entityRoute->getOption('parameters');

      foreach ($parameters as $name => $paramDef) {
        $requirements[$name] = $entityRoute->getRequirement($name);
      }
      $requirements = array_filter($requirements);
      $requirements['_association_linked_entity_manage_access'] = $entityTypeId;

      // Ensure that user has required permissions to manage the
      // associations's linked content entities.
      $route
        ->setRequirements($requirements)
        ->setOption('parameters', $parameters)
        ->setOption('_admin_route', TRUE);

      $collection->add("association.linked_entity.{$entityTypeId}.manage", $route);
    }
  }

}
