<?php

namespace Drupal\association\Utility;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\association\EntityAdapterManagerInterface;
use Drupal\pathauto\PathautoPatternInterface;

/**
 * Helper class to assist with pathauto integrations for linked content.
 */
class AssociationPathautoHelper {

  use StringTranslationTrait;

  /**
   * The entity type adapter plugin manager.
   *
   * @var \Drupal\association\EntityAdapterManagerInterface
   */
  protected EntityAdapterManagerInterface $entityAdapterManager;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity type bundle information manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected EntityTypeBundleInfoInterface $entityBundleInfo;

  /**
   * Create a new instance of the AssociationPathautoHelper class.
   *
   * @param \Drupal\association\EntityAdapterManagerInterface $entity_adapter_manager
   *   The entity type adapter plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle information manager.
   */
  public function __construct(EntityAdapterManagerInterface $entity_adapter_manager, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info) {
    $this->entityAdapterManager = $entity_adapter_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityBundleInfo = $entity_type_bundle_info;
  }

  /**
   * Alter the pathauto_pattern entity form, to add association conditions.
   *
   * @param \Drupal\pathauto\PathautoPatternInterface $pattern
   *   The pathauto pattern entity being updated.
   * @param array $form
   *   Reference to the form elements and structures of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form build, values and input state for the form.
   */
  public function alterForm(PathautoPatternInterface $pattern, array &$form, FormStateInterface $form_state): void {
    if (!$pattern->getType()) {
      return;
    }

    $entityTypeId = $pattern->getAliasType()->getDerivativeId();
    $entityType = $this->entityTypeManager->getDefinition($entityTypeId, FALSE);

    if ($entityType && $this->entityAdapterManager->isAssociableType($entityTypeId)) {
      $defaults = [
        'mode' => 'disabled',
        'bundles' => [],
      ];

      // Capture existing configuration options, and use them as the defaults.
      foreach ($pattern->getSelectionConditions() as $condition) {
        if ($condition->getPluginId() == 'association_type') {
          $configuration = $condition->getConfiguration();
          $defaults['bundles'] = $configuration['bundles'];
          $defaults['mode'] = empty($configuration['negate']) ? 'selected' : 'negated';
          break;
        }
      }

      $bundleNames = [];
      foreach ($this->entityBundleInfo->getBundleInfo('association') as $bundle => $info) {
        $bundleNames[$bundle] = $info['label'];
      }

      $form['#entity_builders'][] = [static::class, 'buildEntity'];

      // Add association configurations to the pathauto settings.
      $form['pattern_container']['association_condition'] = [
        '#type' => 'fieldset',
        '#tree' => TRUE,

        'mode' => [
          '#type' => 'radios',
          '#title' => $this->t('If @entity_label has association types', [
            '@entity_label' => $entityType->getLabel(),
          ]),
          '#options' => [
            'disabled' => $this->t('Any (accept content with or without an association)'),
            'selected' => $this->t('Only selected types'),
            'negated' => $this->t('Only types not selected'),
          ],
          '#default_value' => $defaults['mode'],
        ],

        'bundles' => [
          '#type' => 'checkboxes',
          '#title' => $this->t('Association types'),
          '#options' => $bundleNames,
          '#default_value' => $defaults['bundles'],
        ],
      ];
    }
  }

  /**
   * The entity builder callback for adding association condition to pathauto.
   *
   * @param string $entity_type_id
   *   The entity type ID of the entity being built. Should only be
   *   "pathauto_pattern" for this builder.
   * @param \Drupal\Core\Entity\EntityInterface $pattern
   *   The pathauto_pattern entity being updated.
   * @param array $form
   *   Reference to the form elements and structures of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form build, values and input state for the form.
   */
  public static function buildEntity(string $entity_type_id, EntityInterface $pattern, array &$form, FormStateInterface $form_state): void {
    if (!$pattern instanceof PathautoPatternInterface) {
      throw new \InvalidArgumentException('Pathauto helper expects to be updating entities of pathauto_pattern');
    }

    // Remove the existing association and apply configuration from submission.
    foreach ($pattern->getSelectionConditions() as $condition_id => $condition) {
      if ($condition->getPluginId() == 'association_type') {
        $pattern->removeSelectionCondition($condition_id);
      }
    }

    $entityTypeId = $pattern->getAliasType()->getDerivativeId();
    $values = $form_state->getValue('association_condition');

    switch ($values['mode']) {
      case 'negated':
        $negated = TRUE;

      case 'selected':
        $pattern->addSelectionCondition([
          'id' => 'association_type',
          'entity_type_id' => $entityTypeId,
          'mode' => 1,
          'bundles' => array_filter($values['bundles']),
          'negate' => $negated ?? FALSE,
          'context_mapping' => [
            $entityTypeId => $entityTypeId,
          ],
        ]);
    }
  }

}
