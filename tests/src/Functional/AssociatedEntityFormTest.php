<?php

namespace Drupal\Tests\association\Functional;

use Drupal\association\Entity\AssociationInterface;
use Drupal\association\Entity\AssociationTypeInterface;
use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\association\Traits\AssociationCreationTrait;

/**
 * Test the generation and submission of associated entity forms.
 *
 * Associated entity forms are for creating, editing or deleting entities which
 * are linked to an association.
 *
 * @group association
 */
class AssociatedEntityFormTest extends BrowserTestBase {

  use AssociationCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'toolshed',
    'association',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The association type with a basic entity list for testing.
   *
   * @var \Drupal\association\Entity\AssociationTypeInterface|null
   */
  protected ?AssociationTypeInterface $assocType;

  /**
   * The association entity to associate nodes to.
   *
   * @var \Drupal\association\Entity\AssociationInterface|null
   */
  protected ?AssociationInterface $association;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalCreateContentType(['type' => 'page']);
    $this->drupalCreateContentType(['type' => 'article']);
    $this->drupalCreateContentType(['type' => 'blog']);

    // Create an entity list association type which allows the creation
    // of the of each of the three newly generated content types.
    $this->assocType = $this->createAssociationType([
      'id' => 'test',
      'label' => 'Test type',
      'behavior' => [
        'id' => 'entity_list',
        'config' => [
          'entity_types' => [
            'node' => [
              'page' => 'page',
              'blog' => 'blog',
            ],
          ],
        ],
      ],
    ]);

    // Create an association to group the generate content to.
    $this->association = $this->createAssociation([
      'name' => 'Test association',
      'type' => $this->assocType->id(),
      'status' => TRUE,
    ]);

    // Create an admin user to submit the entity forms with.
    $webAdmin = $this->drupalCreateUser([
      'create page content',
      'edit own page content',
      'administer entity association configurations',
    ], NULL, TRUE);
    $this->drupalLogin($webAdmin);
  }

  /**
   * Test an entity create, that is not part of this association (should fail).
   */
  public function testCreateWrongTag(): void {
    $assoc = $this->association;
    $url = Url::fromRoute('entity.association_link.add_content', [
      'association' => $assoc->id(),
      'tag' => 'entity',
      'entity_type' => 'node',
      'bundle' => 'article',
    ]);

    // Visit create route for the node article - which should fail because
    // node articles are not defined in this entity list.
    $this->drupalGet($url->getInternalPath());
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test the associated content create form and route.
   */
  public function testCreateAssociated(): void {
    $assoc = $this->association;
    $url = Url::fromRoute('entity.association_link.add_content', [
      'association' => $assoc->id(),
      'tag' => 'entity',
      'entity_type' => 'node',
      'bundle' => 'blog',
    ]);

    // Visit the associated node create form.
    $this->drupalGet($url->getInternalPath());
    $this->assertSession()->statusCodeEquals(200);

    // Submit the create node form.
    $edit = [];
    $edit['title[0][value]'] = $this->randomMachineName(8);
    $edit['body[0][value]'] = $this->randomMachineName(16);
    $this->submitForm($edit, 'Save');

    // Check that the node exists in the database.
    $node = $this->drupalGetNodeByTitle($edit['title[0][value]']);

    // Ensure node is for the correct entity type, bundle that it was
    // successfully associated to the intented association.
    /** @var \Drupal\association\Entity\AssociationLink $assocLink */
    $assocLink = $node->get('associations')->entity;
    $this->assertNotEmpty($assocLink);
    $this->assertSame($assoc->id(), $assocLink->getAssociation()->id());
    $this->assertSame('node', $node->getEntityTypeId());
    $this->assertSame('blog', $node->bundle());

    // After submission of the create form, redirect back to the association
    // manage content page.
    $manageUrl = Url::fromRoute('entity.association.manage', [
      'association' => $assoc->id(),
    ]);
    $this->assertSession()->addressEquals($manageUrl->getInternalPath());
  }

}
