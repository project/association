<?php

namespace Drupal\Tests\association\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\association\Traits\AssociationCreationTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\node\NodeInterface;
use Drupal\node\NodeTypeInterface;
use Drupal\user\Entity\User;

/**
 * Base kernel test for creating and manipulating associated node content.
 *
 * Base kernel test class which installs modules, setups association schemas,
 * and node schemas.
 */
abstract class AssociatedEntityTestBase extends KernelTestBase {

  use AssociationCreationTrait;
  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'node',
    'association',
  ];

  /**
   * The node types to install during the test fixtures setup.
   *
   * @var array
   */
  protected static $nodeTypes = [
    'page' => [
      'type' => 'page',
      'name' => 'Page',
    ],
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('node', 'node_access');
    $this->installEntitySchema('date_format');
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('association');
    $this->installEntitySchema('association_link');
    $this->installConfig('system');

    // Create admin and anonymous user accounts.
    $this->setUpCurrentUser(['uid' => 1], [], TRUE);

    // Install all node types defined by this class.
    foreach (static::$nodeTypes as $nodeType) {
      $this->createNodeType($nodeType);
    }
  }

  /**
   * Creates a new node type for use with tests.
   *
   * Used the createNodeType() method instead of the
   * \Drupal\Tests\node\Traits\ContentTypeCreationTrait to avoid the overhead
   * of having a body field, which we don't generally need for our association
   * content tests.
   *
   * @param array $values
   *   Values to apply to the node type during creations.
   * @param bool $should_save
   *   Should the newly created node type entity be saved before being returned.
   *
   * @return \Drupal\node\NodeTypeInterface
   *   The created node type entity with the values and settings applied from
   *   the $values parameter.
   */
  protected function createNodeType(array $values = [], bool $should_save = TRUE): NodeTypeInterface {
    $storage = $this->container
      ->get('entity_type.manager')
      ->getStorage('node_type');

    // Create a random type name if one is not specified.
    if (empty($values['type'])) {
      do {
        // Ensure loop to ensure that the "type" name is not already in use.
        $values['type'] = $this->randomMachineName(8);
      } while ($storage->load($values['type']));
    }

    // Apply default node type settings.
    $values += [
      'name' => $values['type'],
      'new_revision' => FALSE,
      'display_submitted' => FALSE,
    ];

    $nodeType = $storage->create($values);
    if ($should_save) {
      $nodeType->save();
    }

    return $nodeType;
  }

  /**
   * Creates a new node to use with association tests.
   *
   * @param array $values
   *   Values to apply to the node during creations.
   * @param bool $should_save
   *   Should the newly created node entity be saved before being returned.
   *
   * @return \Drupal\node\NodeInterface
   *   The created node entity with the values from the $value parameter.
   */
  protected function createNode(array $values = [], bool $should_save = TRUE): NodeInterface {
    // Populate defaults array.
    $values += [
      'title' => $this->randomMachineName(8),
    ];

    if (!array_key_exists('uid', $values)) {
      $user = User::load(\Drupal::currentUser()->id());

      if ($user) {
        $values['uid'] = $user->id();
      }
      else {
        $user = $this->setUpCurrentUser();
        $values['uid'] = $user->id();
      }
    }

    $node = $this->container
      ->get('entity_type.manager')
      ->getStorage('node')
      ->create($values);

    if ($should_save) {
      $node->save();
    }

    return $node;
  }

}
