<?php

namespace Drupal\Tests\association\Kernel;

/**
 * Test the association type condition plugin.
 *
 * @group association
 */
class AssociationConditionTest extends AssociatedEntityTestBase {

  /**
   * An array of pre-built association instances for use with tests.
   *
   * @var \Drupal\association\Entity\AssociationInterface[]
   */
  protected ?array $associations;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $assocTypes = [
      'microsite' => 'Microsite',
      'species' => 'Species',
      'district' => 'District',
    ];

    foreach ($assocTypes as $type => $typeLabel) {
      $this->createAssociationType([
        'id' => $type,
        'label' => $typeLabel,
        'behavior' => [
          'id' => 'entity_list',
          'config' => [
            'entity_types' => [
              'node' => ['page' => 'page'],
            ],
          ],
        ],
      ]);

      $this->associations[$type] = $this->createAssociation([
        'type' => $type,
        'name' => $typeLabel . ' content',
      ]);
    }
  }

  /**
   * Test condition plugins with association contexts.
   *
   * This kernel test manually sets the association context explicitly and
   * ensures the Condition::evaluate() method is working correctly.
   *
   * @todo create functional test which ensures the context provider is working
   * with the "association_type" condition plugin.
   */
  public function testAssociationTypeMatch(): void {
    $config = [
      'mode' => 0,
      'bundles' => [
        'species' => 'species',
        'district' => 'district',
      ],
    ];
    $condition = \Drupal::service('plugin.manager.condition')
      ->createInstance('association_type', $config);

    // Species should be a matching type.
    $condition->setContextValue('association', $this->associations['species']);
    $this->assertTrue($condition->evaluate());

    // Non-matching type.
    $condition->setContextValue('association', $this->associations['microsite']);
    $this->assertFalse($condition->evaluate());

    // No association value - with "mode = 0" should return TRUE.
    $condition->setContextValue('association', NULL);
    $this->assertTrue($condition->evaluate());

    // No association value - with "mode = 1" should return FALSE.
    $config['mode'] = 1;
    $condition->setConfiguration($config);
    $this->assertFALSE($condition->evaluate());
  }

  /**
   * Test the condition plugin with associated entities.
   *
   * The method of providing a "entity_type_id" in the configuration is used
   * mostly with the pathauto condition to select a pathauto pattern.
   *
   * @see \Drupal\association\Utility\AssociationPathautoHelper
   */
  public function testAssociatedEntityMatch(): void {
    $config = [
      'entity_type_id' => 'node',
      'mode' => 0,
      'bundles' => [
        'microsite' => 'microsite',
        'district' => 'district',
      ],
    ];
    $condition = \Drupal::service('plugin.manager.condition')
      ->createInstance('association_type', $config);

    $values = ['type' => 'page'];

    /* TEST "MODE = 0": Matching association types, and non-associated match */

    // First test content not associated to any association at all.
    $noAssocNode = $this->createNode($values);
    $condition->setContextValue('node', $noAssocNode);
    $this->assertTrue($condition->evaluate());

    // Test associated to a "microsite" type.
    $micrositeNode = $this->createNode($values);
    $this->associations['microsite']->associateEntity('entity', $micrositeNode);
    $condition->setContextValue('node', $micrositeNode);
    $this->assertTrue($condition->evaluate());

    // Test associated to a "species" type.
    $speciesNode = $this->createNode($values);
    $this->associations['species']->associateEntity('entity', $speciesNode);
    $condition->setContextValue('node', $speciesNode);
    $this->assertFalse($condition->evaluate());

    /* TEST "MODE = 1": Only matching association types match */
    $config['mode'] = 1;
    $condition->setConfiguration($config);

    // Non-associations contexts should fail now.
    $condition->setContextValue('node', $noAssocNode);
    $this->assertFalse($condition->evaluate());

    // Ensure matching still passes.
    $condition->setContextValue('node', $micrositeNode);
    $this->assertTrue($condition->evaluate());

    // Ensure non-matches still fail.
    $condition->setContextValue('node', $speciesNode);
    $this->assertFalse($condition->evaluate());
  }

}
