<?php

namespace Drupal\Tests\association\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\association\Traits\AssociationCreationTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Tests for association page functionality.
 *
 * @group association
 */
class AssociationPageTest extends KernelTestBase {

  use AssociationCreationTrait;
  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'node',
    'association',
    'association_page',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setup();

    $this->installEntitySchema('user');
    $this->installEntitySchema('association_type');
    $this->installEntitySchema('association');
    $this->installEntitySchema('association_link');
    $this->installEntitySchema('association_page');

    // Create an admin user to ensure we get all the operations back.
    $this->setUpCurrentUser(['uid' => 1], [], TRUE);
  }

  /**
   * Test association types which have an complementary association page.
   *
   * Ensure that associations of types with pages will create pages when they
   * are saved. Ensure that pages are deleted when the related association is
   * deleted and has page management operations.
   */
  public function testTypeWithPage(): void {
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface */
    $entityTypeManager = $this->container->get('entity_type.manager');

    $assocType = $this->createAssociationType([
      'id' => 'has_page',
      'label' => 'Association with page',
      'landingPage' => [
        'id' => 'association_page',
        'config' => [],
      ],
    ]);
    $assoc = $this->createAssociation([
      'name' => 'Assoc with page',
      'type' => $assocType->id(),
    ]);

    // Check that a association page was automatically created.
    $page = $entityTypeManager
      ->getStorage('association_page')
      ->load($assoc->id());

    $this->assertNotEmpty($page);
    $this->assertEquals($page->toUrl()->toString(), $assoc->toUrl()->toString(), 'Canonical URL for association does not match the page URL.');

    // Ensure that association operations include the page operations when
    // and association with pages enabled is being used.
    $ops = $entityTypeManager
      ->getListBuilder('association')
      ->getOperations($assoc);

    $this->assertArrayHasKey('view-page', $ops);
    $this->assertArrayHasKey('edit-page', $ops);

    // Ensure page is removed when association is deleted.
    $assoc->delete();
    $page = $entityTypeManager
      ->getStorage('association_page')
      ->load($assoc->id());

    $this->assertEmpty($page);
  }

  /**
   * Test that association types without pages.
   *
   * Associations of types without pages should not create pages automatically
   * and should not have any page management related operations.
   */
  public function testTypeWithoutPage(): void {
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface */
    $entityTypeManager = $this->container->get('entity_type.manager');

    $assocType = $this->createAssociationType([
      'id' => 'pageless',
      'label' => 'Type without page',
    ]);
    $assoc = $this->createAssociation([
      'name' => 'Association without page',
      'type' => $assocType->id(),
    ]);

    // Ensure that no page instance was created for this association type.
    $page = $entityTypeManager
      ->getStorage('association_page')
      ->load($assoc->id());

    $this->assertEmpty($page);
    $this->assertEquals('<nolink>', $assoc->toUrl()->getRouteName());

    // Page operation should not be include for associations that are pageless.
    $ops = $entityTypeManager
      ->getListBuilder('association')
      ->getOperations($assoc);

    $this->assertArrayNotHasKey('view-page', $ops);
    $this->assertArrayNotHasKey('edit-page', $ops);
  }

}
