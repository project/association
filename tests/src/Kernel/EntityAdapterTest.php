<?php

namespace Drupal\Tests\association\Kernel;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormState;
use Drupal\association\Adapter\EntityAdapterInterface;
use Drupal\node\NodeInterface;
use Drupal\toolshed\Strategy\Exception\StrategyNotFoundException;
use Drupal\user\Entity\User;

/**
 * Tests the entity adapter functionality.
 *
 * Entity adapters are classes that define a common interface for entity
 * associations to interact with entities they need to associate with. This
 * test is designed to check that common functionality.
 *
 * @group association
 */
class EntityAdapterTest extends AssociatedEntityTestBase {

  /**
   * Gets the entity adapter which handles the entity type requested.
   *
   * @param string|EntityInterface $entity_type_id
   *   Either the machine name of the entity type to fetch the adapter for,
   *   or an instance of an entity to match the adapter to.
   *
   * @return \Drupal\association\Adapter\EntityAdapterInterface
   *   The entity adapter of the entity type requested.
   */
  protected function getEntityAdapter($entity_type_id): EntityAdapterInterface {
    /** @var \Drupal\association\EntityAdapterManagerInterface */
    $adapterManager = $this->container->get('strategy.manager.association.entity_adapter');

    return $entity_type_id instanceof EntityInterface
      ? $adapterManager->getAdapter($entity_type_id)
      : $adapterManager->getAdapterByEntityType($entity_type_id);
  }

  /**
   * Test that entity adapters can generate create new entities.
   */
  public function testAdapterEntityCreate(): void {
    $adapter = $this->getEntityAdapter('node');
    $node = $adapter->createEntity('page', [
      'title' => 'Associated node',
      'status' => NodeInterface::PUBLISHED,
    ]);

    $this->assertNotEmpty($node);
    $this->assertInstanceOf(NodeInterface::class, $node);
  }

  /**
   * Tests the entity adapter can create entity operation forms.
   *
   * Ensures that association adapter creates the correct entity form for
   * the node create operation. See the functional AssociationEntityFormTest to
   * ensure full associated entity creation functionality.
   *
   * @see \Drupal\Tests\association\Functional\AssociatedEntityFormTest
   */
  public function testAdapterEntityForms(): void {
    $nodeTypeDef = $this->getEntityTypeManager()->getDefinition('node');
    $adapter = $this->getEntityAdapter('node');
    $node = $adapter->createEntity('page', [
      'title' => 'Associated node',
      'status' => NodeInterface::PUBLISHED,
    ]);

    $createState = new FormState();
    $adapter->getEntityForm($node, 'create', $createState);

    /** @var \Drupal\Core\Entity\EntityFormInterface */
    $form = $createState->getFormObject();
    $expectedClass = $nodeTypeDef->getFormClass('create') ?? $nodeTypeDef->getFormClass('default');

    $this->assertEquals('node_form', $form->getBaseFormId());
    $this->assertInstanceOf($expectedClass, $form);
    $this->assertEquals($node->label(), $form->getEntity()->label());
    $this->assertEquals('page', $form->getEntity()->bundle());
  }

  /**
   * Test error when requesting an adapter that isn't defined.
   */
  public function testNonExistingAdapter(): void {
    $this->expectException(StrategyNotFoundException::class);
    $this->getEntityAdapter('user');
  }

  /**
   * Test entity adapter exception when wrong entity type provided.
   *
   * Entity adapters are designed to work with a single entity type, and should
   * throw an error if the wrong entity type is provided. This prevents
   * unintentional manipulation of the wrong entity.
   */
  public function testEntityTypeMismatch(): void {
    $adapter = $this->getEntityAdapter('node');
    $anonUser = User::load(0);
    $account = User::load(1);

    $this->expectException(\InvalidArgumentException::class);
    $adapter->checkAccess($anonUser, 'view', $account);
  }

}
