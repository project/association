<?php

namespace Drupal\Tests\association\Traits;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\association\Entity\AssociationInterface;
use Drupal\association\Entity\AssociationTypeInterface;

/**
 * Trait for easier creation of association and association type entities.
 */
trait AssociationCreationTrait {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected ?EntityTypeManagerInterface $entityTypeManager;

  /**
   * Get the entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager.
   */
  protected function getEntityTypeManager(): EntityTypeManagerInterface {
    if (!isset($this->entityTypeManager)) {
      $this->entityTypeManager = \Drupal::entityTypeManager();
    }

    return $this->entityTypeManager;
  }

  /**
   * Generates a random machine name string.
   *
   * @return string
   *   A valid machine name of the requested length.
   */
  abstract protected function randomMachineName(int $length): string;

  /**
   * Create a new entity association type from supplied field values.
   *
   * @param array $values
   *   Values to apply to the association_type entity.
   * @param bool $should_save
   *   Should the new entity be saved? Default is to save the entity.
   *
   * @return \Drupal\association\Entity\AssociationTypeInterface
   *   The association_type entity created from the values.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   Throw an EntityStorageException when an association type with the same
   *   ID already exists.
   */
  protected function createAssociationType(array $values = [], bool $should_save = TRUE): AssociationTypeInterface {
    $storage = $this
      ->getEntityTypeManager()
      ->getStorage('association_type');

    // Ensure that the association type has a valid machine name ID.
    if (empty($values['id'])) {
      do {
        // Make sure that the new machine name is not already in use.
        $values['id'] = $this->randomMachineName(8);
      } while ($storage->load($values['id']));
    }

    // Apply defaults for anything not explicitly needed for the test, but
    // may still be required for the association type instances.
    $values += [
      'label' => $values['id'],
      'searchable' => FALSE,
      'landingPage' => [
        'id' => 'none',
        'config' => [],
      ],
      'behavior' => [
        'id' => 'entity_list',
        'config' => [],
      ],
    ];

    /** @var \Drupal\association\Entity\AssociationTypeInterface $type */
    $type = $storage->create($values);
    if ($should_save) {
      $type->save();
    }

    return $type;
  }

  /**
   * Creates a new association entity.
   *
   * @param array $values
   *   Values for the association entity fields to be populated with.
   * @param bool $should_save
   *   Should the association entity be saved? By default the entity is saved.
   *
   * @return \Drupal\association\Entity\AssociationInterface
   *   An association entity created from the supplied values.
   */
  protected function createAssociation(array $values = [], bool $should_save = TRUE): AssociationInterface {
    $storage = $this
      ->getEntityTypeManager()
      ->getStorage('association');

    // Set a value for the default owner user ID.
    if (!isset($values['uid'])) {
      $account = \Drupal::currentUser();
      $values['uid'] = 0;

      if ($account) {
        $values['uid'] = $account->id();
      }
      elseif (method_exists($this, 'setUpCurrentUser')) {
        /** @var \Drupal\user\UserInterface */
        $account = $this->setUpCurrentUser();
        $values['uid'] = $account->id();
      }
    }

    /** @var \Drupal\association\Entity\AssociationInterface $assoc */
    $assoc = $storage->create($values);
    if ($should_save) {
      $assoc->save();
    }

    return $assoc;
  }

}
